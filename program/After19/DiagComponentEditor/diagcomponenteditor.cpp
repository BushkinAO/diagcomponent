//Constructor and slots here

#include "diagcomponenteditor.h"

DiagComponentEditor::DiagComponentEditor(QWidget *parent, Qt::WFlags flags)
	: QMainWindow(parent, flags)
{
	ui.setupUi(this);//set ui

	CreateNewDocument();

	//connects
	//left menu
	connect(ui.SelectModeButton,SIGNAL(clicked()),this,SLOT(SwitchSelectMode()));
	connect(ui.ComponentModeButton,SIGNAL(clicked()),this,SLOT(SwitchComponentAddMode()));
	connect(ui.CommentModeButton,SIGNAL(clicked()),this,SLOT(SwitchCommentAddMode()));
	connect(ui.ProvidedPortButton,SIGNAL(clicked()),this,SLOT(SwitchPrdPortAddMode()));
	connect(ui.RequiredPortButton,SIGNAL(clicked()),this,SLOT(SwitchReqPortAddMode()));
	connect(ui.LinkButton,SIGNAL(clicked()),this,SLOT(SwitchCreateLinkMode()));
	connect(ui.DelegateLinkButton,SIGNAL(clicked()),this,SLOT(SwitchCreateDelegateLinkMode()));

	//context menu
	connect(ui.WorkPlace,SIGNAL(customContextMenuRequested(QPoint)),this,SLOT(ShowContextMenu(QPoint)));

	//scale
	connect(ui.ScaleSpin,SIGNAL(valueChanged(int)),this,SLOT(ScaleScene(int)));

	/*//scrolls
	horisontScroll = new QScrollBar(ui.WorkPlace->horizontalScrollBar());
	horisontScroll->setRange(0,MainScene->width());
	horisontScroll->setSliderPosition(MainScene->width()/2);
	ui.WorkPlace->setHorizontalScrollBar(horisontScroll);

	verticalScroll = new QScrollBar(ui.WorkPlace->verticalScrollBar());
	verticalScroll->setRange(0,MainScene->height());
	verticalScroll->setSliderPosition(MainScene->height()/2);
	ui.WorkPlace->setVerticalScrollBar(verticalScroll);

	connect(horisontScroll,SIGNAL(sliderMoved(int)),this,SLOT(ScrollHor(int)));
	connect(verticalScroll,SIGNAL(sliderMoved(int)),this,SLOT(ScrollVer(int)));*/

	//main menu
	connect(ui.actionNew_file,SIGNAL(triggered(bool)),this,SLOT(CreateNewDocument()));
	connect(ui.actionSave_as,SIGNAL(triggered(bool)),this,SLOT(SaveJPG()));
	connect(ui.actionPrint,SIGNAL(triggered(bool)),this,SLOT(PrintResult()));
	connect(ui.actionSave,SIGNAL(triggered(bool)),this,SLOT(SaveDCE()));
	connect(ui.actionOpen_file,SIGNAL(triggered(bool)),this,SLOT(LoadDCE()));

	connect(ui.actionHelp_about_program,SIGNAL(triggered(bool)),this,SLOT(ShowHelpAP()));
	connect(ui.actionHelp_about_functions,SIGNAL(triggered(bool)),this,SLOT(ShowHelpAF()));

	//text editing buttons
	connect(ui.AgreeButton,SIGNAL(clicked()),this,SLOT(ConfirmEditing()));
	connect(ui.DisagreeButton,SIGNAL(clicked()),this,SLOT(CancelEditing()));
}

DiagComponentEditor::~DiagComponentEditor()
{

}

//slots

void DiagComponentEditor::SwitchSelectMode()
{
	MainState = mSELECT;

	ClearSelection();
	ColorModeButton(ui.SelectModeButton);

	//off
	TurnOffPlaceholder();
}

void DiagComponentEditor::SwitchComponentAddMode()
{
	MainState = mCOMPONENT;

	ClearSelection();
	ColorModeButton(ui.ComponentModeButton);

	//on
	ComponentEl newItemRaw = ComponentEl(GlobalID);
	GlobalID++;
	QGraphicsItem * tmp = newItemRaw.getThisElement();
	
	TurnOnPlaceholder(tmp, true);
}

void DiagComponentEditor::SwitchCommentAddMode()
{
	MainState = mCOMMENT;

	ClearSelection();
	ColorModeButton(ui.CommentModeButton);

	//on
	ElemComment newItemRaw = ElemComment(GlobalID);
	GlobalID++;
	QGraphicsItem * tmp = newItemRaw.getThisElement();

	TurnOnPlaceholder(tmp, true);
}

void DiagComponentEditor::SwitchReqPortAddMode()
{
	MainState = mRecPORT;

	ClearSelection();
	ColorModeButton(ui.RequiredPortButton);

	//on
	QGraphicsItem * tmp = new QGraphicsTextItem(QString::fromLocal8Bit("�������� �� ��������� ��� ���������� �����"),0,MainScene);
	tmp->setData(1,QVariant("Tooltip"));
	
	TurnOnPlaceholder(tmp,true);
}

void DiagComponentEditor::SwitchPrdPortAddMode()
{
	MainState = mPrdPORT;

	ClearSelection();
	ColorModeButton(ui.ProvidedPortButton);

	//on
	QGraphicsItem * tmp = new QGraphicsTextItem(QString::fromLocal8Bit("�������� �� ��������� ��� ���������� �����"),0,MainScene);
	tmp->setData(1,QVariant("Tooltip"));

	TurnOnPlaceholder(tmp,true);
}

void DiagComponentEditor::SwitchCreateLinkMode()
{
	MainState = mPortsLINK;

	ClearSelection();
	ColorModeButton(ui.LinkButton);

	QGraphicsItem * tmp = new QGraphicsTextItem(QString::fromLocal8Bit("�������� �� �������� ����(����)"),0,MainScene);
	tmp->setData(1,QVariant("Tooltip"));

	PrdPortItemForLink = 0;
	RecPortItemForLink = 0;

	//on
	TurnOnPlaceholder(tmp,true);
}

void DiagComponentEditor::SwitchCreateDelegateLinkMode()
{
	MainState = mDelegateLINK;

	ClearSelection();
	ColorModeButton(ui.DelegateLinkButton);

	QGraphicsItem * tmp = new QGraphicsTextItem(QString::fromLocal8Bit("�������� 2 ����� ��� �������� ����� delegate"),0,MainScene);
	tmp->setData(1,QVariant("Tooltip"));

	//on
	TurnOnPlaceholder(tmp,true);
}

void DiagComponentEditor::FixMouseClick(QGraphicsSceneMouseEvent *e)
{
	QPointF pos = e->scenePos();

	QGraphicsItem * pressed = MainScene->itemAt(pos);
	QGraphicsItem * clicked = 0;
	QGraphicsItem * selZon;

	if(pressed != 0)//find something
	{
		clicked = FindMainParent(pressed,"Item");//find whole item

		//remove all linked with placeholder
		QList<QGraphicsItem *> children = Placeholder->childItems();
		int size = children.size();

		for(int i=0; i<size; i++)
		{
			if(clicked == children[i])//it was placeholder, nothing to do here
			{
				clicked = 0;
				break;
			}
		}

		if(clicked != 0)//verify hit
		{
			selZon = GetSelectionItem(clicked);

			if(!selZon || !selZon->contains(selZon->mapFromScene(pos)))//cliked off selected zone
			{
				clicked = 0;
			}
		}
	}

	//work with states
	if(e->button() == Qt::LeftButton)
	{
		if(MainState == mCOMPONENT && Placeholder->brush() == QBrush(ColorForSelect))
		{
			ComponentEl newItemRaw = ComponentEl(GlobalID);
			GlobalID++;
			newItemRaw.ColorSelecting(QBrush(Qt::NoBrush), QPen(Qt::NoPen));
			QGraphicsItem * newItem = newItemRaw.getThisElement();
			newItem->setPos(pos);

			MainScene->addItem(newItem);
		}
		else if(MainState == mCOMMENT && Placeholder->brush() == QBrush(ColorForSelect))
		{
			ElemComment newItemRaw = ElemComment(GlobalID);
			GlobalID++;
			newItemRaw.ColorSelecting(QBrush(Qt::NoBrush), QPen(Qt::NoPen));
			QGraphicsItem * newItem = newItemRaw.getThisElement();
			newItem->setPos(pos);

			MainScene->addItem(newItem);
		}
		else if(MainState == mSELECT)
		{
			if(clicked == 0)//remove selection
			{
				ClearSelection();
			}

			//need to add this element to selected?
			if(clicked != 0 && clicked->data(3) == QVariant("NotSelected"))
			{
				SelectOneItem(clicked);
			}
			else if(SelectedItems.size())
			{
				LastAllowedPosition = e->scenePos();
			}
		}
		else if(MainState == mRecPORT || MainState == mPrdPORT)
		{
			//is clicked element can have ports?
			if(clicked != 0 && clicked->data(1) == QVariant("Component") && ItemForAddPorts != clicked)//1 stage
			{
				//set placeholder
				PortClass newItemRaw;

				if(MainState == mRecPORT)
				{
					newItemRaw = PortClass(true,GlobalID);
					GlobalID++;
				}
				else
				{
					newItemRaw = PortClass(false,GlobalID);
					GlobalID++;
				}

				//turn it on
				QGraphicsItem * tmp = newItemRaw.getThisElement();
				TurnOnPlaceholder(tmp, true);

				//mark selectedclicked item
				ItemForAddPorts = clicked;
			}
			else if(ItemForAddPorts != 0 && Placeholder->brush() == QBrush(ColorForSelect))//2 stage
			{
				QGraphicsItem * chPlaceholder = Placeholder->childItems().first();//here must be only one element in array - tagged mainblock
				PortClass newPort;

				if(MainState == mRecPORT)
				{
					newPort = PortClass(true,GlobalID);
					GlobalID++;
				}
				else//mPrdPORT
				{
					newPort = PortClass(false,GlobalID);
					GlobalID++;
				}

				//create new port object
				newPort.ColorSelecting(QBrush(Qt::NoBrush), QPen(Qt::NoPen));
				QGraphicsTextItem * textPort = newPort.getTextItem();
				QGraphicsItem * newItem = newPort.getThisElement();

				//form place gor additing port - because center of this object is not in (0,0)
				QPointF placeRaw = chPlaceholder->scenePos();
				QPointF placeGrilled;
				qreal sizeOffset = newItem->transformOriginPoint().y();
				
				if(Placeholder->rotation() == 90)
				{
					placeGrilled = QPointF(placeRaw.x()-sizeOffset, placeRaw.y()-sizeOffset);
					textPort->setPos(16,-5);
					textPort->rotate(-90);
				}
				else if(Placeholder->rotation() == 180)
				{
					placeGrilled = QPointF(placeRaw.x(), placeRaw.y()-sizeOffset*2);
					textPort->setPos(130,-3);
					textPort->rotate(-180);
				}
				else if(Placeholder->rotation() == -90)
				{
					placeGrilled = QPointF(placeRaw.x()+sizeOffset, placeRaw.y()-sizeOffset);
					textPort->setPos(36,14);
					textPort->rotate(90);
				}
				else
				{
					placeGrilled = placeRaw;
				}

				//add new port
				newItem->setRotation(Placeholder->rotation());
				newItem->setParentItem(ItemForAddPorts);
				
				placeGrilled = ItemForAddPorts->mapFromScene(placeGrilled);
				newItem->setPos(placeGrilled);
				
				MainScene->addItem(newItem);//add on scene
			}
		}
		else if(MainState == mPortsLINK)
		{
			if(RecPortItemForLink == 0 && clicked->data(1) == QVariant("InputPort"))//1 stage
			{
				RecPortItemForLink = clicked;
				SelectOneItem(clicked);

				QGraphicsItem * tmp = new QGraphicsTextItem(QString::fromLocal8Bit("�������� �� ������� ����(����)"),0,MainScene);
				tmp->setData(1,QVariant("Tooltip"));

				TurnOnPlaceholder(tmp,true);
			}
			else if(PrdPortItemForLink == 0 && RecPortItemForLink != 0 && clicked->data(1) == QVariant("OutputPort"))//2 stage
			{
				PrdPortItemForLink = clicked;
				SelectOneItem(clicked);

				if(RecPortItemForLink->parentItem() == PrdPortItemForLink->parentItem())//one parent for both ports is not allowed
				{
					QMessageBox::warning(this,QString::fromLocal8Bit("������"),QString::fromLocal8Bit("���������� ������� �����!\n��� ����� ����������� ������ � ���� �� ����������!"));
				}
				else
				{
					MakeNewFormOfConnection(PrdPortItemForLink,RecPortItemForLink,false);
				}

				SwitchCreateLinkMode();
			}
		}
		else if(MainState == mDelegateLINK)
		{
			if(clicked != 0 && SelectedItems.size()<2 && (SelectedItems.isEmpty() || !SelectedItems.isEmpty() && (clicked->data(1) == QVariant("InputPort") || clicked->data(1) == QVariant("OutputPort")) && clicked->data(1) == SelectedItems.first()->data(1)))
			{
				//can't use funtion
				SelectIt(clicked,true);
				clicked->setZValue(131);
				SelectedItems.append(clicked);
			}
			
			if(SelectedItems.size() == 2)
			{
				//MakeDelegateLine
				if(SelectedItems[0]->data(1) == QVariant("InputPort") && SelectedItems[1]->data(1) == QVariant("InputPort") || SelectedItems[0]->data(1) == QVariant("OutputPort") && SelectedItems[1]->data(1) == QVariant("OutputPort"))
				{
					if(SelectedItems[0]->parentItem() == SelectedItems[1]->parentItem()->parentItem())
					{
						MakeDelegateLine(SelectedItems[0],SelectedItems[1]);
						SwitchCreateDelegateLinkMode();
					}
					else if(SelectedItems[0]->parentItem()->parentItem() == SelectedItems[1]->parentItem())//reverse order of arguments
					{
						MakeDelegateLine(SelectedItems[1],SelectedItems[0]);
						SwitchCreateDelegateLinkMode();
					}
					else
					{
						QMessageBox::warning(this,QString::fromLocal8Bit("������"),QString::fromLocal8Bit("���������� ������� ����� delegate!\n����������, ������� ����������� ���������� �����, �� ����� ������������ ����� ����� �����!"));
						SwitchCreateDelegateLinkMode();
					}
				}
			}
		}
	}
	
	ui.WorkPlace->update();
}

void DiagComponentEditor::FixMouseMove(QGraphicsSceneMouseEvent *e)
{
	//moving selected element
	if(MainState == mSELECT && e->buttons() & Qt::LeftButton)
	{
		QGraphicsItem * clicked = MainScene->itemAt(e->scenePos());

		//finding under cursor item with tag SelectingRect - for drag it can be only comment or component as parent
		if(clicked != 0 && clicked->parentItem() != 0 && clicked->data(1) == QVariant("SelectingRect") && (clicked->parentItem()->data(1) == QVariant("Component") || clicked->parentItem()->data(1) == QVariant("Comment")))
		{
			//move all selected items
			int size = SelectedItems.size();
			for(int i=0; i<size; i++)
			{
				if(SelectedItems[i]->parentItem()->data(1) != QVariant("Component"))
				{
					SelectedItems[i]->setPos(SelectedItems[i]->scenePos() + (e->scenePos() - e->lastScenePos()));
				}
			}
		}
	}
	

	//placeholder
	//if we have something to move...
	if(Placeholder->isVisible())
	{
		QList<QGraphicsItem *> intersection;
		QGraphicsItem * chPlaceholder = Placeholder->childItems().first();//here must be only one element in array - tagged mainblock

		if(chPlaceholder->data(1) == QVariant("Tooltip"))
		{
			//remove rotation of placeholder
			Placeholder->setRotation(0);

			//moving placeholder
			Placeholder->setPos(e->scenePos() + QPointF(-10,-25));
			Placeholder->setBrush(ColorMain);
		}
		else
		{
			//move placeholder - depends on type of additing element
			//choose type item under placeholder
			if(chPlaceholder->data(1) == QVariant("Comment") || chPlaceholder->data(1) == QVariant("Component"))
			{
				//remove rotation of placeholder
				Placeholder->setRotation(0);

				//moving placeholder
				Placeholder->setPos(e->scenePos());
			}
			else if(chPlaceholder->data(1) == QVariant("InputPort") || chPlaceholder->data(1) == QVariant("OutputPort"))
			{
				//moving placeholder
				if(ItemForAddPorts != 0)
				{
					float X = e->scenePos().x();
					float Y = e->scenePos().y();
					QPointF newPos = e->scenePos();

					//here accuratly - port have rect with some size - 12px - and MUST NOT bulge
					int rectsize = 12;

					QPointF PosIFAP = ItemForAddPorts->parentItem()!=0?ItemForAddPorts->parentItem()->mapToScene(ItemForAddPorts->pos()):ItemForAddPorts->pos();//map to scene for ItemForAddPorts, if it's needed

					if(X < PosIFAP.x())//left
					{
						//verify positon - it must be into bounding rect of ItemForAddPorts
						if(newPos.y() >= PosIFAP.y()+rectsize && newPos.y() <= PosIFAP.y()+ItemForAddPorts->boundingRect().height())
						{
							Placeholder->setRotation(180);
							Placeholder->setPos(PosIFAP.x()+rectsize/2,newPos.y());
						}
					}
					else if(X >= PosIFAP.x() && X <= PosIFAP.x()+ItemForAddPorts->boundingRect().width())//center
					{
						if(Y <= PosIFAP.y() + ItemForAddPorts->boundingRect().height()/2)//up
						{
							//verify positon - it must be into bounding rect of ItemForAddPorts
							if(newPos.x() >= PosIFAP.x() && newPos.x() <= ItemForAddPorts->x()+ItemForAddPorts->boundingRect().width()-rectsize)
							{
								Placeholder->setRotation(-90);
								Placeholder->setPos(newPos.x(),PosIFAP.y()+rectsize/2);
							}
						}
						else//down
						{
							//verify positon - it must be into bounding rect of ItemForAddPorts
							if(newPos.x() >= PosIFAP.x()+rectsize && newPos.x() <= PosIFAP.x()+ItemForAddPorts->boundingRect().width())
							{
								Placeholder->setRotation(90);
								Placeholder->setPos(newPos.x(),PosIFAP.y()+ItemForAddPorts->boundingRect().height()-rectsize/2);
							}
						}
					}
					else//right
					{
						//verify positon - it must be into bounding rect of ItemForAddPorts
						if(newPos.y() >= PosIFAP.y() && newPos.y() <= PosIFAP.y()+ItemForAddPorts->boundingRect().height()-rectsize)
						{
							Placeholder->setRotation(0);
							Placeholder->setPos(PosIFAP.x()+ItemForAddPorts->boundingRect().width()-rectsize/2,newPos.y());
						}
					}
				}
			}
		
			Placeholder->setBrush(ColorForSelect);
		}
	}
}

void DiagComponentEditor::FixMouseRelease(QGraphicsSceneMouseEvent *e)
{
	if(MainState == mSELECT && e->button() == Qt::LeftButton)
	{
		//make connection line
		MakeConnectionLines();
	}

	if(!WasMODIFIED)
	{
		WasMODIFIED = true;
		this->setWindowTitle(QString("%1 %2").arg(this->windowTitle()).arg("*"));
	}
}


void DiagComponentEditor::ShowHelpAP()
{
	helpD = new QDialog(ui.centralWidget);
	helpD->setGeometry(300,100,600,400);
	helpD->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);

	QGraphicsView * viewGr = new QGraphicsView(helpD);
	viewGr->setGeometry(0,0,600,400);

	QGraphicsScene * sc = new QGraphicsScene();

	sc->addPixmap(QPixmap("Resources/help/AP_1.png"));

	viewGr->setScene(sc);

	helpD->exec();
}

void DiagComponentEditor::ShowHelpAF()
{
	helpD = new QDialog(ui.centralWidget);
	helpD->setGeometry(300,100,650,400);
	helpD->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);

	QGraphicsView * viewGr = new QGraphicsView(helpD);
	viewGr->setGeometry(0,0,650,400);

	QGraphicsScene * sc = new QGraphicsScene();

	QGraphicsItem * tmp1 = sc->addPixmap(QPixmap("Resources/help/AF_1.png"));
	QGraphicsItem * tmp2 = sc->addPixmap(QPixmap("Resources/help/AF_2.png"));
	tmp2->setY(tmp1->boundingRect().height());
	QGraphicsItem * tmp3 = sc->addPixmap(QPixmap("Resources/help/AF_3.png"));
	tmp3->setY(tmp1->boundingRect().height() + tmp2->boundingRect().height());

	viewGr->setScene(sc);

	helpD->exec();
}






//ContextMenu starts here
void DiagComponentEditor::ShowContextMenu(QPoint pos)
{
	//create
	QMenu menu;
	QMenu * submenu1;

	bool allComponents;

	QGraphicsItem * comm;
	QGraphicsItem * comp;

	QGraphicsItem * parentport, * childport;

	QList<QGraphicsItem *> comments;
	
	//clicked on selected item
	if(MainState == mSELECT && SelectedItems.size())
	{
		//4
		if(SelectedItems.size() == 1 && (SelectedItems.first()->data(1) == QVariant("Component") || SelectedItems.first()->data(1) == QVariant("Comment") || SelectedItems.first()->data(1) == QVariant("InputPort") || SelectedItems.first()->data(1) == QVariant("OutputPort")) && TextThatEditing == 0)
		{
			QAction *action4 = new QAction(QString::fromLocal8Bit("�������� �����"), ui.WorkPlace);
			menu.addAction(action4);
		}

		//5
		QAction *action5 = new QAction(QString::fromLocal8Bit("������� ����������"), ui.WorkPlace);
		menu.addAction(action5);

		//6
		if(SelectedItems.size() >= 2)
		{
			allComponents = true;

			for(QList<QGraphicsItem *>::iterator it = SelectedItems.begin(); it != SelectedItems.end() && allComponents; it++)
			{
				if((*it)->data(1) != QVariant("Component"))
				{
					allComponents = false;
				}
			}
		
			if(allComponents)//all selected elements are components
			{
				QAction *action6 = new QAction(QString::fromLocal8Bit("���������� ��������"), ui.WorkPlace);
				menu.addAction(action6);
			}
			
		}

		//7
		allComponents = true;

		for(QList<QGraphicsItem *>::iterator it = SelectedItems.begin(); it != SelectedItems.end() && allComponents; it++)
		{
			if((*it)->parentItem() == 0 || (*it)->data(1) != QVariant("Component"))
			{
				allComponents = false;
			}
		}
		
		if(allComponents)//all selected elements are components
		{
			QAction *action7 = new QAction(QString::fromLocal8Bit("�������� ����� � ���������"), ui.WorkPlace);
			menu.addAction(action7);
		}

		//8
		if(SelectedItems.size() == 2)
		{
			if(SelectedItems[0]->data(1) == QVariant("Comment") && SelectedItems[1]->data(1) == QVariant("Component"))
			{
				comm = SelectedItems[0];
				comp = SelectedItems[1];

				QAction *action8 = new QAction(QString::fromLocal8Bit("��������� �����������"), ui.WorkPlace);
				menu.addAction(action8);
			}
			else if(SelectedItems[0]->data(1) == QVariant("Component") && SelectedItems[1]->data(1) == QVariant("Comment"))
			{
				comm = SelectedItems[1];
				comp = SelectedItems[0];

				QAction *action8 = new QAction(QString::fromLocal8Bit("��������� �����������"), ui.WorkPlace);
				menu.addAction(action8);
			}
		}

		//9
		if(SelectedItems.size())
		{
			for(QList<QGraphicsItem *>::iterator it = SelectedItems.begin(); it != SelectedItems.end(); it++)
			{
				if((*it)->data(1) == QVariant("Comment"))
				{
					comments.append(*it);
				}
			}
		
			if(comments.size())//we have comments
			{
				QAction *action9 = new QAction(QString::fromLocal8Bit("�������� �����������"), ui.WorkPlace);
				menu.addAction(action9);
			}
			
		}

		//10
		if(SelectedItems.size() == 1 && (SelectedItems[0]->data(1) == QVariant("InputPort") || SelectedItems[0]->data(1) == QVariant("OutputPort")))
		{	
			submenu1 = new QMenu(QString::fromLocal8Bit("������� ����� delegate"), ui.WorkPlace);
			
			bool notEmpty = false;
			
			if(SelectedItems[0]->data(8) != QVariant(0))
			{
				QAction * action10 = new QAction(QString::fromLocal8Bit("�� ����� ������(delegate)"), ui.WorkPlace);
				submenu1->addAction(action10);

				notEmpty = true;
			}

			if(SelectedItems[0]->data(9) != QVariant(0))
			{
				QAction * action11 = new QAction(QString::fromLocal8Bit("� ���������(delegate)"), ui.WorkPlace);
				submenu1->addAction(action11);

				notEmpty = true;
			}

			if(notEmpty)
			{
				menu.addMenu(submenu1);
			}
		}


		//call
		QAction* selectedItem = menu.exec(ui.WorkPlace->mapToGlobal(pos));
		if (selectedItem)
		{
			if(selectedItem->text() == QString::fromLocal8Bit("������� ����������"))
			{
				DeleteSelectedItems();
			}
			else if(selectedItem->text() == QString::fromLocal8Bit("�������� �����"))
			{
				MainState = mEditTEXT;

				//switch off buttons
				TurnXButtons(false);

				ChangeText(SelectedItems.first());
			}
			else if(selectedItem->text() == QString::fromLocal8Bit("���������� ��������"))
			{
				SetParentForComponents(SelectedItems);
			}
			else if(selectedItem->text() == QString::fromLocal8Bit("�������� ����� � ���������"))
			{
				RemoveParentsForSelected(SelectedItems);
			}
			else if(selectedItem->text() == QString::fromLocal8Bit("��������� �����������"))
			{
				LinkCommentWithComponent(comm,comp);
			}
			else if(selectedItem->text() == QString::fromLocal8Bit("�������� �����������"))
			{
				RemoveCommCompLink(comments);
			}
			else if(selectedItem->text() == QString::fromLocal8Bit("� ���������(delegate)"))
			{
				RemoveDelegateLine(SelectedItems[0],false);//item is child
			}
			else if(selectedItem->text() == QString::fromLocal8Bit("�� ����� ������(delegate)"))
			{
				RemoveDelegateLine(SelectedItems[0],true);//item is parent
			}
		}
	}
}

void DiagComponentEditor::ConfirmEditing()
{
	//confirm
	if(LineEditText->isVisible())//for component
	{
		TextThatEditing->setPlainText(LineEditText->text());

		//verify text length
		CalculateTextLength(TextThatEditing);
	}
	else if(TextEditText->isVisible())//for comment
	{
		TextThatEditing->setPlainText(TextEditText->toPlainText());
	}
	else if(LineEditPort->isVisible())//for port
	{
		TextThatEditing->setPlainText(LineEditPort->text());
	}

	//hide
	ui.AgreeButton->setVisible(false);
	ui.DisagreeButton->setVisible(false);
	TextThatEditing->setVisible(true);
	TextEditText->setVisible(false);
	LineEditText->setVisible(false);
	LineEditPort->setVisible(false);

	TextThatEditing = 0;
	TurnXButtons(true);
	MainState = mSELECT;
}

void DiagComponentEditor::CancelEditing()
{
	//hide
	ui.AgreeButton->setVisible(false);
	ui.DisagreeButton->setVisible(false);
	TextThatEditing->setVisible(true);
	TextEditText->setVisible(false);
	LineEditText->setVisible(false);
	LineEditPort->setVisible(false);

	TextThatEditing = 0;
	TurnXButtons(true);
	MainState = mSELECT;
}

void DiagComponentEditor::VerifyMaxLength()
{
	//int countLines = TextEditText->document()->blockCount();
	int maxNormalCount = 3;
	int maxLineSize = 9;

	if(TextEditText->isVisible())
	{
		if(TextEditText->toPlainText().size() > maxNormalCount*maxLineSize)
		{
			TextEditText->textCursor().deletePreviousChar();
		}
	}
}

void DiagComponentEditor::ScaleScene(int val)
{
	QRectF scRect = ui.WorkPlace->sceneRect();

	float f = ui.WorkPlace->width()*((float)val/100);
	float g = scRect.width();

	scRect.setRect(scRect.x(),scRect.y(),ui.WorkPlace->width()/((float)val/100),ui.WorkPlace->height()/((float)val/100));
	
	ui.WorkPlace->fitInView(scRect);
	ui.WorkPlace->setProperty("ViewRect",QVariant::fromValue<QPolygonF>(ui.WorkPlace->mapToScene(scRect.toRect())));
}

/*void DiagComponentEditor::ScrollHor(int newval)
{
	QRectF rectt = ui.WorkPlace->sceneRect();

	float part = (float)newval/horisontScroll->maximum();
	float maxOffset = horisontScroll->maximum()-rectt.width();
}

void DiagComponentEditor::ScrollVer(int newval)
{
}*/





//up menu
void DiagComponentEditor::CreateNewDocument()
{
	MainScene = new WorkScene();//construct main scene

	//clear
	SelectedItems.clear();

	ItemForAddPorts = 0;

	LoadingCons = NotLoadedYetConnections();

	//Colors
	ColorForSelect = QColor(130,200,240);
	ColorError = QColor(210,87,89);
	ColorMain = QColor(255,255,255);

	ui.WorkPlace->setContextMenuPolicy(Qt::CustomContextMenu);//off default context menu

	//set placeholder settings
	Placeholder = new QGraphicsRectItem(0,0,0,0,0,MainScene);
	Placeholder->setBrush(QBrush(ColorForSelect));
	Placeholder->setPen(Qt::NoPen);
	Placeholder->setOpacity(0.33);
	Placeholder->setVisible(false);
	Placeholder->setZValue(1000);

	SwitchSelectMode();//set mode to SELECT

	//for editiong text component
	LineEditText = new QLineEdit(ui.centralWidget);
	LineEditText->setVisible(false);

	TextEditText = new QTextEdit(ui.centralWidget);
	TextEditText->setVisible(false);
	TextEditText->setWordWrapMode(QTextOption::WordWrap);

	LineEditPort = new QLineEdit(ui.centralWidget);
	LineEditPort->setVisible(false);

	ui.AgreeButton->setVisible(false);
	ui.DisagreeButton->setVisible(false);

	TextThatEditing = NULL;

	GlobalID = 0;

	ui.WorkPlace->setScene(MainScene);
	ui.WorkPlace->setSceneRect(0,0,ui.WorkPlace->width(),ui.WorkPlace->height());

	//set standart scale
	ScaleScene(100);
	ui.ScaleSpin->setValue(100);

	WasMODIFIED = false;
	this->setWindowTitle("");

	//workscene resignal to this
	connect(MainScene,SIGNAL(mousePressed(QGraphicsSceneMouseEvent *)),this,SLOT(FixMouseClick(QGraphicsSceneMouseEvent *)));
	connect(MainScene,SIGNAL(mouseMove(QGraphicsSceneMouseEvent *)),this,SLOT(FixMouseMove(QGraphicsSceneMouseEvent *)));
	connect(MainScene,SIGNAL(mouseReleased(QGraphicsSceneMouseEvent *)),this,SLOT(FixMouseRelease(QGraphicsSceneMouseEvent *)));

	//when text is editing
	connect(TextEditText,SIGNAL(textChanged()),this,SLOT(VerifyMaxLength()));
}