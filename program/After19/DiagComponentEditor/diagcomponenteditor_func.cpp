//methods(not slots) here

#include "diagcomponenteditor.h"

//status Item - to find full component by item
//status Diagram - find main parent of this item in all diagram
//status All - find maximal parent, without conditions
QGraphicsItem * DiagComponentEditor::FindMainParent(QGraphicsItem *item, QString status)
{
	QGraphicsItem * currentParent = item->parentItem();

	if(status == QString("Item"))
	{
		if(item->data(2) == QVariant("Main"))
		{
			return item;
		}
		else if(currentParent != 0)
		{
			return FindMainParent(currentParent,status);
		}
		else
		{
			return 0;
		}
	}
	else if(status == QString("Diagram"))
	{
		if(item->data(2) == QVariant("Main") && currentParent->data(2) != QVariant("Main"))
		{
			return item;
		}
		else if(currentParent != 0)
		{
			return FindMainParent(currentParent,status);
		}
		else
		{
			return 0;
		}
	}
	else if(status == QString("All"))
	{
		if(currentParent == 0)
		{
			return item;
		}
		else
		{
			return FindMainParent(currentParent,status);
		}
	}
	else
	{
		return 0;
	}
}

void DiagComponentEditor::TurnOnPlaceholder(QGraphicsItem * item, bool onoff)
{
	//clear placeholder
	TurnOffPlaceholder();

	//show placeholder
	Placeholder->setVisible(onoff);

	if(item != 0)
	{
		item->setParentItem(Placeholder);

		//form placeholder size
		QGraphicsItem * selecting = GetSelectionItem(item);
		if(selecting != 0)
		{
			Placeholder->setRect(selecting->boundingRect());
		}
		else
		{
			Placeholder->setRect(item->boundingRect());
		}
	}

	ui.WorkPlace->setMouseTracking(true);
}

void DiagComponentEditor::TurnOffPlaceholder()
{
	//hide placeholder
	Placeholder->setVisible(false);
	ui.WorkPlace->setMouseTracking(false);

	//clear after ports
	ItemForAddPorts = NULL;//clear item to work with - it will be again finded when click will been
	Placeholder->setRotation(0);

	//clear placeholder
	QList<QGraphicsItem *> tmp = Placeholder->childItems();
	if(!tmp.isEmpty())
	{
		MainScene->removeItem(tmp.first());
	}
}

// return objects, itersects with items, if it's components or comments without parents
/*QList<QGraphicsItem *> DiagComponentEditor::ItersectSomething(QGraphicsItem *item)
{
	QList<QGraphicsItem *> result;
	QGraphicsItem * itemForCollide = GetSelectionItem(FindMainParent(item,"Item"));

	if(itemForCollide == 0)//no selection zone - it can be placeholder
	{
		itemForCollide = item;
	}

	QList<QGraphicsItem *> PlshlCollidedWith  = itemForCollide->collidingItems();
	QList<QGraphicsItem *> DetectedItems;
	QList<QGraphicsItem *>::iterator i;
	QGraphicsItem * tmp, * parr;

	PlshlCollidedWith.append(item);//for detecting children too

	for(i=PlshlCollidedWith.begin(); i<PlshlCollidedWith.end(); i++)
	{
		tmp = FindMainParent(*i,"Item");

		if(tmp != 0 && DetectedItems.indexOf(tmp) == -1 && tmp != item)//founded parent already exist in array AND not itself (*i) OR this (*i) global parent and have Item in children
		{
			parr = tmp;

			do
			{
				parr = parr->parentItem();

				if()!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			}
			while(parr != 0);

			DetectedItems.append(tmp);
		}
	}

	return DetectedItems;
}*/

void DiagComponentEditor::SelectIt(QGraphicsItem * item, bool onoff)
{
	QGraphicsPolygonItem * selecting = (QGraphicsPolygonItem *)GetSelectionItem(item);
	
	if(selecting != 0)//item have selecting child
	{
		if(onoff)
		{
			selecting->setBrush(QBrush(ColorForSelect));
		}
		else
		{
			selecting->setBrush(Qt::NoBrush);
		}

		selecting->setPen(Qt::NoPen);
		selecting->setOpacity(0.35);
		item->setData(3, QVariant((onoff)?"Selected":"NotSelected"));
	}
}

QGraphicsItem * DiagComponentEditor::GetSelectionItem(QGraphicsItem *item)
{
	if(item != 0 && item->data(2) == QVariant("Main"))
	{
		QList<QGraphicsItem *> children = item->childItems();
		children.append(item);//item itself can be that we want

		for(int i=0; i<children.size(); i++)
		{
			if(children[i]->data(1) == QVariant("SelectingRect"))
			{
				return children[i];//selection item found and returned
			}
		}
	}

	return 0;//nothing found - strange, but everything can be
}

int DiagComponentEditor::countTaggedElements(QList<QGraphicsItem*> list, QVariant tag)
{
	int count = 0;

	for(QList<QGraphicsItem *>::iterator i = list.begin(); i != list.end(); i++)
	{
		if((*i)->data(1) == tag)
		{
			count++;
		}
	}

	return count;
}

void DiagComponentEditor::SelectOneItem(QGraphicsItem *clicked)
{
	if(SelectedItems.isEmpty() || !SelectedItems.isEmpty() && clicked->parentItem() == SelectedItems.first()->parentItem())//something already selected, and new have the same parent?
	{
		//mark selected
		SelectIt(clicked,true);
		clicked->setZValue(20);
		SelectedItems.append(clicked);
	}
}




//context menu
void DiagComponentEditor::DeleteSelectedItems()
{
	QList<QGraphicsItem *>::iterator i;

	int state = QMessageBox::information(this,QString::fromLocal8Bit("����� �������?"),QString::fromLocal8Bit("�� ������ ����� ������ ������� ���������� %1 �� ���������?").arg(QString::number(SelectedItems.size())),QString::fromLocal8Bit("��"),QString::fromLocal8Bit("���"));

	if(state == 0)
	{
		for(i = SelectedItems.begin(); i != SelectedItems.end() && SelectedItems.size(); i++)
		{
			if((*i)->data(1) == QVariant("Comment"))
			{
				QList<QGraphicsItem *> lst;
				lst.append(*i);

				RemoveCommCompLink(lst);

				MainScene->removeItem(*i);
			}
			else if((*i)->data(1) == QVariant("ConLine"))
			{
				LinkPoints connec = (*i)->data(4).value<LinkPoints>();

				//input
				QList<LinkPoints> listt = connec.input->data(4).value<QList<LinkPoints>>();
				listt.removeOne(connec);
				connec.input->setData(4,QVariant::fromValue<QList<LinkPoints>>(listt));

				//output
				listt = connec.output->data(4).value<QList<LinkPoints>>();
				listt.removeOne(connec);
				connec.output->setData(4,QVariant::fromValue<QList<LinkPoints>>(listt));

				//restore port end
				if(listt.isEmpty())//no connection left in output port
				{
					QList<QGraphicsItem * > childs = connec.output->childItems();
					int size = childs.size();

					for(int j=0; j<size; j++)
					{
						if(childs[j]->data(1) == QVariant("OutConnector"))
						{
							childs[j]->setVisible(true);
						}
					}
				}

				//remove connection
				MainScene->removeItem(*i);
			}
			else if((*i)->data(1) == QVariant("InputPort") || (*i)->data(1) == QVariant("OutputPort"))
			{
				RemovePort(*i);
			}
			else if((*i)->data(1) == QVariant("Component"))
			{
				RemoveItem(*i);
			}
		}

		SelectedItems.clear();
	}
}

void DiagComponentEditor::ChangeText(QGraphicsItem * item)
{
	if(TextThatEditing)
	{
		TextThatEditing->setVisible(false);//hide
	}

	//find text
	QList<QGraphicsItem *> children = item->childItems();
	QGraphicsTextItem * textItem = 0;

	QPointF pos;

	for(QList<QGraphicsItem *>::iterator i=children.begin(); i!=children.end(); i++)
	{
		if((*i)->data(1) == QVariant("TextContent"))
		{
			textItem = (QGraphicsTextItem *)(*i);
			break;
		}
	}

	//create edit form
	//start editing
	ui.AgreeButton->setVisible(true);
	ui.DisagreeButton->setVisible(true);
	textItem->setVisible(false);//hide

	TextThatEditing = textItem;//save for confirm/cancel

	if(textItem)
	{
		if(item->data(1) == QVariant("Component"))
		{
			pos = textItem->pos();
			pos = item->mapToScene(pos.toPoint());
			pos = ui.WorkPlace->mapFromScene(pos.toPoint()) + ui.WorkPlace->pos()+QPointF(0,5);
			
			LineEditText->setVisible(true);
			LineEditText->setGeometry(pos.x(),pos.y(),item->boundingRect().width()-20,20);
			LineEditText->setMaxLength(12*item->boundingRect().width()/150);
			LineEditText->setText(textItem->toPlainText());
		}
		else if(item->data(1) == QVariant("Comment"))//comment
		{
			pos = textItem->pos();
			pos = item->mapToScene(pos.toPoint());
			pos = ui.WorkPlace->mapFromScene(pos.toPoint()) + ui.WorkPlace->pos()+QPointF(0,5);
			
			TextEditText->setVisible(true);
			TextEditText->setGeometry(pos.x(),pos.y(),item->boundingRect().width()-20,item->boundingRect().height()-20);
			//TextEditText->setTextWidth(12*item->boundingRect().width()/150);
			TextEditText->setText(textItem->toPlainText());
		}
		else if(item->data(1) == QVariant("InputPort") || item->data(1) == QVariant("OutputPort"))
		{
			pos = textItem->pos();
			pos = item->mapToScene(pos.toPoint());
			pos = ui.WorkPlace->mapFromScene(pos.toPoint()) + ui.WorkPlace->pos();
			
			LineEditPort->setVisible(true);
			LineEditPort->setGeometry(pos.x(),pos.y()-10,100,20);
			LineEditPort->setMaxLength(8);
			LineEditPort->setText(textItem->toPlainText());
		}
	}
}

void DiagComponentEditor::CalculateTextLength(QGraphicsItem * textitem)
{
	QGraphicsTextItem * textit = (QGraphicsTextItem *)textitem;
	QFontMetrics fontmet(textit->font());
	int textlength = fontmet.width(textit->toPlainText());
	int newsize = textit->parentItem()->boundingRect().width()-20;

	textit->setTextWidth(newsize);

	if(textlength > newsize)
	{
		QMessageBox::warning(this,QString::fromLocal8Bit("��������������!"),QString::fromLocal8Bit("����� ������ �������� �� ��������� �������� ���� ����. ����� ������ ����� �������!"));

		QString txt = textit->toPlainText();
		int sizel = txt.size();
		int newstrsize;
		bool end = true;

		for(int l = 0; l < sizel && end; l++)
		{
			newstrsize = txt.size()-1;
			txt.remove(newstrsize,1);

			int fff = fontmet.width(txt);

			if(fontmet.width(txt) <= newsize-5)//fit in, then ok, 5 is offset to garant fit
			{
				textit->setPlainText(txt);
				end = false;
			}
		}
	}
}

void DiagComponentEditor::RemoveParentsForSelected(QList<QGraphicsItem*> items)
{
	int size = items.size();

	for(int i=0; i<size; i++)
	{
		RemoveParentForComponent(items[i]);
	}
}



void DiagComponentEditor::RemovePort(QGraphicsItem * port)
{
	QList<LinkPoints> connec = port->data(4).value<QList<LinkPoints>>();

	int size = connec.size();

	for(int i=0; i<size; i++)
	{
		//input
		QList<LinkPoints> listt = connec[i].input->data(4).value<QList<LinkPoints>>();
		listt.removeOne(connec[i]);
		connec[i].input->setData(4,QVariant::fromValue<QList<LinkPoints>>(listt));

		//output
		listt = connec[i].output->data(4).value<QList<LinkPoints>>();
		listt.removeOne(connec[i]);
		connec[i].output->setData(4,QVariant::fromValue<QList<LinkPoints>>(listt));

		//restore port end
		if(listt.isEmpty())//no connection left in output port
		{
			QList<QGraphicsItem * > childs = connec[i].output->childItems();
			int size = childs.size();

			for(int j=0; j<size; j++)
			{
				if(childs[j]->data(1) == QVariant("OutConnector"))
				{
					childs[j]->setVisible(true);
				}
			}
		}

		MainScene->removeItem(connec[i].connectionLine);
	}

	//remove delegate lines
	if(port->data(8) != QVariant(0))
	{
		RemoveDelegateLine(port,true);
	}

	if(port->data(9) != QVariant(0))
	{
		RemoveDelegateLine(port,false);
	}

	//remove itself
	MainScene->removeItem(port);
}

void DiagComponentEditor::RemoveItem(QGraphicsItem * item)
{
	QList<LinkPoints> connec;

	QList<QGraphicsItem *> childrenn = item->childItems();
	int sizej = childrenn.size();

	for(int j=0; j<sizej; j++)
	{
		if(childrenn[j]->data(1) == QVariant("InputPort") || childrenn[j]->data(1) == QVariant("OutputPort"))
		{
			RemovePort(childrenn[j]);
		}
		/*else if(childrenn[j]->data(1) == QVariant("Component"))//not needed
		{
			RemoveItem(childrenn[j]);
		}*/
	}

	//remove link with comment
	QList<QGraphicsItem *> lp;

	if(item->data(7) != QVariant(0))
	{
		lp.append(item->data(7).value<QGraphicsItem *>()->data(7).value<LinkPoints>().input);
		RemoveCommCompLink(lp);
	}

	//remove itself
	QGraphicsItem * parr = item->parentItem();
	MainScene->removeItem(item);

	ResizeParentAfterExitingElement(parr);//recalculate parent
}

void DiagComponentEditor::ClearSelection()
{
	int size = SelectedItems.size();

	for(int i=0; i<size; i++)//free linked item from selection
	{
		SelectIt(SelectedItems[i],false);
		SelectedItems[i]->setZValue(1);
		SelectedItems[i]->setData(3,QVariant("NotSelected"));
	}
					
	//clear list
	SelectedItems.clear();
}

void DiagComponentEditor::ColorModeButton(QPushButton * button)
{
	QString style = QString("background-color: rgb(151, 198, 255);");

	ui.RequiredPortButton->setStyleSheet("");
	ui.ProvidedPortButton->setStyleSheet("");
	ui.SelectModeButton->setStyleSheet("");
	ui.LinkButton->setStyleSheet("");
	ui.ComponentModeButton->setStyleSheet("");
	ui.CommentModeButton->setStyleSheet("");
	ui.DelegateLinkButton->setStyleSheet("");

	button->setStyleSheet(style);
}

void DiagComponentEditor::TurnXButtons(bool state)
{
	ui.SelectModeButton->setEnabled(state);
	ui.ComponentModeButton->setEnabled(state);
	ui.CommentModeButton->setEnabled(state);
	ui.ProvidedPortButton->setEnabled(state);
	ui.RequiredPortButton->setEnabled(state);
	ui.LinkButton->setEnabled(state);
	ui.DelegateLinkButton->setEnabled(state);
}

void DiagComponentEditor::MakeNewFormOfConnection(QGraphicsItem *PrdPort, QGraphicsItem *RecPort, bool WasSelected)
{
	if(PrdPort == 0 || RecPort == 0)
	{
		QMessageBox::warning(this,QString::fromLocal8Bit("������"),QString::fromLocal8Bit("���������� ������� ����� ����� �������!\n�� ������� ��� ������� ���� �� ����������� ������!\n\n�������� ��������� �������� ��� ������� ����������� ����� ������ �������� � ������� ����������."));
		
		QList<QGraphicsItem * > childs = PrdPort->childItems();
		int size = childs.size();

		for(int j=0; j<size; j++)
		{
			if(childs[j]->data(1) == QVariant("OutConnector"))
			{
				childs[j]->setVisible(true);
			}
		}
		
		return;
	}

	if(PrdPort->parentItem()->parentItem() != RecPort->parentItem()->parentItem())
	{
		QMessageBox::warning(this,QString::fromLocal8Bit("������"),QString::fromLocal8Bit("���������� ������� ����� ����� �������!\n����������, ��������� ���������� �������, ����� ������ ���������!"));
		
		QList<QGraphicsItem * > childs = PrdPort->childItems();
		int size = childs.size();
		
		for(int j=0; j<size; j++)
		{
			if(childs[j]->data(1) == QVariant("OutConnector"))
			{
				childs[j]->setVisible(true);
			}
		}

		return;
	}

	QVector<QPointF> points;

	//find point to connect
	QGraphicsPolygonItem * RecPolyg = (QGraphicsPolygonItem *)RecPort;
	QGraphicsPolygonItem * PrdPolyg = (QGraphicsPolygonItem *)PrdPort;

	QList<QGraphicsItem *> childrenn = RecPort->childItems();
	childrenn.append(PrdPort->childItems());

	//hide main connector
	QPointF tmmp;
	int size = childrenn.size();

	for(int i=0; i<size; i++)
	{
		if(childrenn[i]->data(1) == QVariant("OutConnector"))
		{
			QGraphicsPolygonItem * pol = (QGraphicsPolygonItem *)childrenn[i];
			
			if(childrenn[i]->parentItem() == RecPort)
			{
				tmmp = pol->polygon().last();
			}
			else
			{
				tmmp = pol->polygon().first();
			}

			points.append(childrenn[i]->parentItem()->mapToScene(tmmp));
		}
	}
	
	QPointF RecPoint = RecPolyg->parentItem()->mapToScene(QPointF(RecPolyg->x(),RecPolyg->y())) + RecPolyg->polygon().last();
	QPointF PrdPoint = PrdPolyg->parentItem()->mapToScene(QPointF(PrdPolyg->x(),PrdPolyg->y())) + PrdPolyg->polygon().last();

	//make line
	QVector<QPointF> line;
	line.append(points[0]);
	line.append(QPointF(points[0].x(),points[1].y()));
	line.append(points[1]);
	line.append(QPointF(points[0].x(),points[1].y()));//because polygon MUST be closed and do it automaticly

	//draw line
	QGraphicsItem * resPolyg = MainScene->addPolygon(QPolygonF(line),QPen(QColor(0,0,0)),QBrush(Qt::NoBrush));
	resPolyg->setZValue(700);

	//save data about connection
	LinkPoints connectionn = LinkPoints();
	connectionn.input = RecPort;
	connectionn.output = PrdPort;
	connectionn.connectionLine = resPolyg;

	//verify existing connection of this element
	QList<LinkPoints> consPrd = PrdPort->data(4).value<QList<LinkPoints>>();
	QList<LinkPoints> consRec = RecPort->data(4).value<QList<LinkPoints>>();
	
	if(consPrd.indexOf(connectionn) != -1 || consRec.indexOf(connectionn) != -1)
	{
		QMessageBox::warning(this,QString::fromLocal8Bit("������"),QString::fromLocal8Bit("���������� ������� �����!\n���������� ����� ��� �������!"));
		MainScene->removeItem(resPolyg);
		return;
	}
	else
	{
		consPrd.append(connectionn);
		consRec.append(connectionn);
	}

	//move ports ends
	//let it be only produced ports...
	childrenn = PrdPort->childItems();

	//hide main connector
	QGraphicsItem * conctr;
	size = childrenn.size();

	for(int i=0; i<size; i++)
	{
		if(childrenn[i]->data(1) == QVariant("OutConnector"))
		{
			childrenn[i]->setVisible(false);
			conctr = childrenn[i];
		}
	}

	//create
	QGraphicsEllipseItem * Connector;
	QPointF connPoint = points[0];//points[0] - because its end of required port

	if(RecPort->rotation() == 0)
	{
		Connector = new QGraphicsEllipseItem(connPoint.x()+1,connPoint.y()-5,10,10);
	}
	else if(RecPort->rotation() == 90)
	{
		Connector = new QGraphicsEllipseItem(connPoint.x()-5,connPoint.y()+1,10,10);
	}
	else if(RecPort->rotation() == 180 || RecPort->rotation() == -180)
	{
		Connector = new QGraphicsEllipseItem(connPoint.x()-10,connPoint.y()-5,10,10);
	}
	else if(RecPort->rotation() == 270 || RecPort->rotation() == -90)
	{
		Connector = new QGraphicsEllipseItem(connPoint.x()-5,connPoint.y()-10,10,10);
	}
	else
	{
		QMessageBox::warning(this,QString::fromLocal8Bit("������"),QString::fromLocal8Bit("���������� ������� �����!\n������ ��� �������� ������ �����!"));
		conctr->setVisible(true);
		MainScene->removeItem(resPolyg);
		return;
	}

	//form connector
	Connector->setBrush(QBrush(QColor(255,255,255)));
	Connector->setParentItem(resPolyg);
	Connector->setZValue(720);

	//set data
	Connector->setData(1,QVariant("ConLine"));
	resPolyg->setData(1,QVariant("ConLine"));
	resPolyg->setData(2,QVariant("Main"));//is main part of block
	resPolyg->setData(3,QVariant("NotSelected"));

	//save pointer in ports
	PrdPort->setData(4,QVariant::fromValue<QList<LinkPoints>>(consPrd));
	RecPort->setData(4,QVariant::fromValue<QList<LinkPoints>>(consRec));
	resPolyg->setData(4,QVariant::fromValue<LinkPoints>(connectionn));

	//make selection for connectline
	line.remove(line.size()-1);//remove duplicate point
	size = line.size();
	QPointF cur, las, nex, tmp;
	QVector<QPointF> selecZone;
	
	//1 stage
	for(int i=0; i<size; i++)
	{
		if(i != 0 && i != size-1)//not first ond not last
		{
			tmp = NextPointOfSelectionConnection(0,line[i],line[i-1],line[i+1]);
		}
		else if(i == 0)//first
		{
			tmp = NextPointOfSelectionConnection(1,line[i],QPointF(),line[i+1]);
		}
		else//last
		{
			tmp = NextPointOfSelectionConnection(2,line[i],line[i-1],QPointF());
		}

		//add to vector
		if(tmp != QPointF(-30000,-30000))
		{
			selecZone.append(tmp);
		}
		else
		{
			return;
		}
	}

	//2 stage - return
	for(int i=size-1; i>=0; i--)
	{
		if(i != 0 && i != size-1)//not first ond not last
		{
			tmp = NextPointOfSelectionConnection(0,line[i],line[i+1],line[i-1]);
		}
		else if(i == size-1)//first
		{
			tmp = NextPointOfSelectionConnection(1,line[i],QPointF(),line[i-1]);
		}
		else//last
		{
			tmp = NextPointOfSelectionConnection(2,line[i],line[i+1],QPointF());
		}

		//add to vector
		if(tmp != QPointF(-30000,-30000))
		{
			selecZone.append(tmp);
		}
		else
		{
			return;
		}
	}

	QGraphicsItem * selectionZone = MainScene->addPolygon(selecZone,QPen(Qt::NoPen),QBrush(Qt::NoBrush));
	selectionZone->setParentItem(resPolyg);
	selectionZone->setZValue(800);
	selectionZone->setData(1,QVariant("SelectingRect"));

	//return selection
	if(WasSelected)
	{
		SelectOneItem(resPolyg);
	}
}

void DiagComponentEditor::LinkCommentWithComponent(QGraphicsItem * comment, QGraphicsItem * component)
{
	if(comment == 0 || component == 0 || comment->data(1) != QVariant("Comment") || component->data(1) != QVariant("Component"))
	{
		QMessageBox::warning(this,QString::fromLocal8Bit("������"),QString::fromLocal8Bit("���������� ��������� �����������!\n���� �� ��������� ����������, ���� ���������, ���� ���."));
		return;
	}

	if(component->data(7) != QVariant(0) || comment->data(7) != QVariant(0))
	{
		QMessageBox::warning(this,QString::fromLocal8Bit("������"),QString::fromLocal8Bit("���������� ��������� �����������!\n���� ��������� ��� ����� ����������� �����������. ����������� ������ ����� ���, ��� �������� �����!"));
		return;
	}

	QVector<QPointF> vec;
	vec.append(comment->scenePos() + QPointF(10,10));
	vec.append(component->scenePos() + QPointF(10,10));

	QGraphicsItem * line = MainScene->addLine(QLineF(vec[0],vec[1]),QPen(QBrush(QColor(0,0,0)),Qt::DashLine));

	//set data
	LinkPoints linkp;
	linkp.input = comment;
	linkp.output = component;
	linkp.connectionLine = line;

	//setting z-values
	line->setZValue(component->zValue()+1);
	comment->setZValue(component->zValue()+2);

	line->setData(1,QVariant("CommCompLink"));
	line->setData(7,QVariant::fromValue<LinkPoints>(linkp));

	comment->setData(7,QVariant::fromValue<QGraphicsItem *>(line));
	component->setData(7,QVariant::fromValue<QGraphicsItem *>(line));
}

void DiagComponentEditor::MakeDelegateLine(QGraphicsItem *ParentPort, QGraphicsItem *ChildPort)
{
	if(ParentPort == 0 || ChildPort == 0)
	{
		QMessageBox::warning(this,QString::fromLocal8Bit("������"),QString::fromLocal8Bit("���������� ������� ����� ����� �������!\n�� ������� ��� ������� ���� �� ����������� ������!\n\n�������� ��������� ����� ������� ����� �������� ����� ��������-�������, ���� � ��� ���� ��������� �����"));
		return;
	}

	if(ParentPort->parentItem() != ChildPort->parentItem()->parentItem())
	{
		QMessageBox::warning(this,QString::fromLocal8Bit("������"),QString::fromLocal8Bit("���������� ������� ����� ����� �������!\n����� ��� ����� �������� ��� ������ � ������ ���������� � ������ ������� ����� ����������!"));
		return;
	}

	if(ChildPort->data(9) != QVariant(0))
	{
		QMessageBox::warning(this,QString::fromLocal8Bit("������"),QString::fromLocal8Bit("���������� ������� ����� ����� �������!\n���� ��������� ���������� ��� ����� ����� ���� delegate(� ������� ������)! ������ ������� ��� �����."));
		return;
	}

	if(!((ParentPort->data(1) == QVariant("InputPort") || ParentPort->data(1) == QVariant("OutputPort")) && ParentPort->data(1) == ChildPort->data(1)))
	{
		QMessageBox::warning(this,QString::fromLocal8Bit("������"),QString::fromLocal8Bit("���������� ������� ����� ����� �������!\n��� �������� ������ ���� �������, ������, ����������� ����!"));
		return;
	}

	//for save data about connection
	QVector<QPointF> points;
	LinkPoints connectionn = LinkPoints();

	if(ParentPort->data(1) == QVariant("OutputPort"))//from parent to child arrow orientation
	{
		points.append(ParentPort->parentItem()->mapToScene(ParentPort->pos()));//first point

		QGraphicsPolygonItem * ChildPolyg = (QGraphicsPolygonItem *)ChildPort;
		points.append(ChildPolyg->mapToScene(ChildPolyg->polygon().last()+QPointF(5,0)));
	}
	else if(ParentPort->data(1) == QVariant("InputPort"))//from child to parent arrow orientation
	{
		QGraphicsPolygonItem * ChildPolyg = (QGraphicsPolygonItem *)ChildPort;
		points.append(ChildPolyg->mapToScene(ChildPolyg->polygon().last()+QPointF(5,0)));//first point

		points.append(ParentPort->parentItem()->mapToScene(ParentPort->pos()));
	}

	//make line
	//calculate coords for arrow sides
	QPointF SideLeft, SideRight;
	float angl = QLineF(points[1],points[0]).angle();
	float myangl = 30;//in degrees
	int size = 11;//in pixels

	SideLeft = QPointF(points[1].x()+size*cos((angl-myangl)/180*3.1415),points[1].y()-size*sin((angl-myangl)/180*3.1415));
	SideRight = QPointF(points[1].x()+size*cos((angl+myangl)/180*3.1415),points[1].y()-size*sin((angl+myangl)/180*3.1415));

	//fill vector
	QVector<QPointF> line;
	line.append(points[0]);
	line.append(points[1]);
	line.append(SideLeft);
	line.append(points[1]);
	line.append(SideRight);
	line.append(points[1]);//because polygon MUST be closed and do it automaticly

	//draw line
	QGraphicsItem * resPolyg = MainScene->addPolygon(QPolygonF(line),QPen(QColor(0,0,0)),QBrush(Qt::NoBrush));
	resPolyg->setZValue(721);

	//form data
	connectionn.input = ParentPort;
	connectionn.output = ChildPort;
	connectionn.connectionLine = resPolyg;

	//verify existing connection of this element
	QList<LinkPoints> consParent = ParentPort->data(8).value<QList<LinkPoints>>();
	LinkPoints consChild = ChildPort->data(9).value<LinkPoints>();
	
	if(consParent.indexOf(connectionn) != -1 || consChild == connectionn)
	{
		QMessageBox::warning(this,QString::fromLocal8Bit("������"),QString::fromLocal8Bit("���������� ������� �����!\n���������� ����� ��� �������!"));
		MainScene->removeItem(resPolyg);
		return;
	}
	else
	{
		consParent.append(connectionn);
		consChild = connectionn;
	}

	//set data
	resPolyg->setData(1,QVariant("ConLineDelegate"));

	//save pointer in ports
	ParentPort->setData(8,QVariant::fromValue<QList<LinkPoints>>(consParent));
	ChildPort->setData(9,QVariant::fromValue<LinkPoints>(consChild));
}

void DiagComponentEditor::RemoveDelegateLine(QGraphicsItem * port, bool parentLine)
{
	if(parentLine)//remove from parent
	{
		QList<LinkPoints> lst = port->data(8).value<QList<LinkPoints>>();
		int size = lst.size();
		
		for(int i=0; i<size; i++)
		{
			MainScene->removeItem(lst[i].connectionLine);//remove line

			//clear data
			lst[i].output->setData(9,QVariant(0));
		}

		port->setData(8,QVariant(0));
	}
	else//from child
	{
		LinkPoints lstCh = port->data(9).value<LinkPoints>();

		QList<LinkPoints> lstPr = lstCh.input->data(8).value<QList<LinkPoints>>();
		lstPr.removeOne(lstCh);
		lstCh.input->setData(8,QVariant::fromValue<QList<LinkPoints>>(lstPr));

		MainScene->removeItem(lstCh.connectionLine);//remove line

		port->setData(9,QVariant(0));
	}
}

void DiagComponentEditor::RemoveCommCompLink(QList<QGraphicsItem *> comments)
{
	LinkPoints lp;
	int size = comments.size();

	for(int i=0; i<size; i++)
	{
		QGraphicsItem * f = comments[i]->data(7).value<QGraphicsItem *>();
		lp = comments[i]->data(7).value<QGraphicsItem *>()->data(7).value<LinkPoints>();

		lp.input->setData(7,QVariant(0));
		lp.output->setData(7,QVariant(0));

		MainScene->removeItem(lp.connectionLine);
	}
}

QPointF DiagComponentEditor::NextPointOfSelectionConnection(int state, QPointF cur, QPointF las, QPointF nex)
{
	/*
	state:
	0 - not last and first
	1 - first
	2 - last
	*/

	QPointF tmp;
	int sizeSelec = 7;

	//don't touch it!)))
	//realy, don't touch
	if(state == 0)
	{
		if(las.x() > cur.x() && nex.y() > cur.y())//right-down
		{
			tmp = QPointF(cur.x()-sizeSelec,cur.y()-sizeSelec);
		}
		else if(nex.x() < cur.x() && las.y() > cur.y())//down-left
		{
			tmp = QPointF(cur.x()+sizeSelec,cur.y()-sizeSelec);
		}
		else if(las.x() < cur.x() && nex.y() < cur.y())//left-up
		{
			tmp = QPointF(cur.x()+sizeSelec,cur.y()+sizeSelec);
		}
		else if(nex.x() > cur.x() && las.y() < cur.y())//up-right
		{
			tmp = QPointF(cur.x()-sizeSelec,cur.y()+sizeSelec);
		}
		else if(nex.x() > cur.x() && las.y() > cur.y())//down-right
		{
			tmp = QPointF(cur.x()+sizeSelec,cur.y()+sizeSelec);
		}
		else if(las.x() > cur.x() && nex.y() < cur.y())//right-up
		{
			tmp = QPointF(cur.x()+sizeSelec,cur.y()-sizeSelec);
		}
		else if(nex.x() < cur.x() && las.y() < cur.y())//up-left
		{
			tmp = QPointF(cur.x()-sizeSelec,cur.y()-sizeSelec);
		}
		else if(las.x() < cur.x() && nex.y() > cur.y())//left-down
		{
			tmp = QPointF(cur.x()-sizeSelec,cur.y()+sizeSelec);
		}
	}
	else if(state == 1)
	{
		if(nex.y() < cur.y())//down
		{
			tmp = QPointF(cur.x()+sizeSelec,cur.y());
		}
		else if(nex.x() > cur.x())//left
		{
			tmp = QPointF(cur.x(),cur.y()+sizeSelec);
		}
		else if(nex.y() > cur.y())//up
		{
			tmp = QPointF(cur.x()-sizeSelec,cur.y());
		}
		else if(nex.x() < cur.x())//right
		{
			tmp = QPointF(cur.x(),cur.y()-sizeSelec);
		}
	}
	else if(state == 2)
	{
		if(las.y() < cur.y())//down
		{
			tmp = QPointF(cur.x()-sizeSelec,cur.y());
		}
		else if(las.x() > cur.x())//left
		{
			tmp = QPointF(cur.x(),cur.y()-sizeSelec);
		}
		else if(las.y() > cur.y())//up
		{
			tmp = QPointF(cur.x()+sizeSelec,cur.y());
		}
		else if(las.x() < cur.x())//right
		{
			tmp = QPointF(cur.x(),cur.y()+sizeSelec);
		}
	}
	else
	{
		QMessageBox::warning(this,QString::fromLocal8Bit("������"),QString::fromLocal8Bit("���������� ������� �����!\n������ ��� ������������ ������� ��������� ������� �����!"));
		return QPointF(-30000,-30000);
	}

	return tmp;
}

QGraphicsItem * DiagComponentEditor::ResizeElement(QGraphicsItem * item, int width, int height)
{
	if((item->data(1) != QVariant("Component") && item->data(1) != QVariant("Comment")) || width < 150 || height < 75)
	{
		return item;
	}

	double oldwidthCoef = (float)width / item->boundingRect().toRect().width();
	double oldheightCoef = (float)height / item->boundingRect().toRect().height();

	QGraphicsRectItem * rectitem = (QGraphicsRectItem *) item;
	QPointF poss = QPoint(item->x(),item->y());
	rectitem->setRect(0,0,width,height);

	//change port's positions
	if(item->data(1) == QVariant("Component"))
	{
		QList<QGraphicsItem *> childrenn = item->childItems();
		int size = childrenn.size();

		for(int i=0; i<size; i++)
		{
			if(childrenn[i]->data(1) == QVariant("InputPort") || childrenn[i]->data(1) == QVariant("OutputPort"))//this is POOOOOOOORT!
			{
				childrenn[i]->setPos(childrenn[i]->x()*oldwidthCoef, childrenn[i]->y()*oldheightCoef);

				if(childrenn[i]->rotation() == 0)
				{
					childrenn[i]->setX(width-6);
				}
				else if(childrenn[i]->rotation() == 90)
				{
					childrenn[i]->setY(height-12);
				}
				else if(childrenn[i]->rotation() == 180)
				{
					childrenn[i]->setX(6);
				}

				//recalculate connections
				QList<LinkPoints> listlink = childrenn[i]->data(4).value<QList<LinkPoints>>();
				QList<LinkPoints> listconc;

				for(QList<LinkPoints>::iterator k = listlink.begin(); k != listlink.end(); k++)
				{
					MainScene->removeItem((*k).connectionLine);//remove old

					//input
					listconc = (*k).input->data(4).value<QList<LinkPoints>>();//get list
					listconc.removeOne(*k);//remove old connection
					(*k).input->setData(4,QVariant::fromValue<QList<LinkPoints>>(listconc));//send list back

					//output
					listconc = (*k).output->data(4).value<QList<LinkPoints>>();//get list
					listconc.removeOne(*k);//remove old connection
					(*k).output->setData(4,QVariant::fromValue<QList<LinkPoints>>(listconc));
					
					
				}
			}
			else if(childrenn[i]->data(1) == QVariant("SelectingRect"))
			{
				QGraphicsRectItem * rectt = (QGraphicsRectItem *)childrenn[i];
				rectt->setRect(0,0,width,height);
				rectt->setZValue(-1);
			}
			else if(childrenn[i]->data(1) == QVariant("IconComponent"))
			{
				childrenn[i]->setX(width-30);
			}
		}
	}
	
	return rectitem;
}

//parenting
void DiagComponentEditor::SetParentForComponents(QList<QGraphicsItem*> items)
{
	if(items.size() < 2)
	{
		QMessageBox::warning(this,QString::fromLocal8Bit("������!"),QString::fromLocal8Bit("���������� ��������� ��������!\n��� �������� �������� ����� ������ ���� �������� 2 � ����� ����������."));
		return;
	}

	QGraphicsItem * FutureParent = items.last();
	items.removeLast();//other
	
	int topMargin = 50;
	int outsideMargin = 40;

	int widthChs, heightChs;
	int newwidth, newheight;
	int oldwidth, oldheight;
	int leftpos;

	ChildsItems CI;
	QList<ChildsItems> CIlist;

	int maxInLine = 3;

	for(QList<QGraphicsItem *>::iterator it = items.begin(); it != items.end(); it++)
	{
		if(FindMainParent(*it,"Diagram")->data(6).value<int>() >= 2)//3 levels-include
		{
			QMessageBox::warning(this,QString::fromLocal8Bit("������!"),QString::fromLocal8Bit("���������� ��������� ��������!\n������������ ������� ����������� ����� 3 � ��������� ���� ������������� �������� ��� ������ ���!"));
			break;
		}

		//increment nesting
		FutureParent->setData(6,QVariant((*it)->data(6).value<int>()+1));

		//add children
		if((*it)->data(1) == QVariant("Component"))
		{
			if(FutureParent->data(5) == QVariant(0))//not filled yet
			{
				//choose new size
				oldwidth = FutureParent->boundingRect().toRect().width();
				newwidth = (*it)->boundingRect().toRect().width();

				oldheight = FutureParent->boundingRect().toRect().height();
				newheight = (*it)->boundingRect().toRect().height();

				widthChs = oldwidth>=newwidth?oldwidth:newwidth;
				heightChs = oldheight>=newheight?oldheight:newheight;

				//set new size
				ResizeElement(FutureParent,widthChs+2*outsideMargin*1.5,heightChs+topMargin+2*outsideMargin*1.5);
				(*it)->setParentItem(FutureParent);
				(*it)->setPos(outsideMargin*1.5, topMargin+outsideMargin*1.5);

				//add data about children
				CI = ChildsItems();
				CI.line.append(*it);
				CIlist.append(CI);
				FutureParent->setData(5,QVariant::fromValue<QList<ChildsItems>>(CIlist));
			}
			else
			{
				CIlist = FutureParent->data(5).value<QList<ChildsItems>>();

				int NewWidth = 0;

				//find line to add
				if(CIlist.size() > maxInLine || CIlist.size() == maxInLine && CIlist[maxInLine-1].line.size() >= maxInLine)
				{
					QMessageBox::warning(this,QString::fromLocal8Bit("������!"),QString::fromLocal8Bit("���������� ��������� ��������!\n���������� ������������ ���������� ����� ��� ���������� ���� ��������!"));
					break;
				}
				else
				{
					if(CIlist[CIlist.size()-1].line.size() < maxInLine)//add to line
					{
						//set new size for parent
						int lastborder = CIlist[CIlist.size()-1].line.last()->x() + CIlist[CIlist.size()-1].line.last()->boundingRect().toRect().width() + outsideMargin*1.5;//last element in line's right border + margin
						newwidth = lastborder + (*it)->boundingRect().toRect().width() + 2*outsideMargin*1.5;
						
						//maybe new item have bigger height than existing
						oldheight = FutureParent->boundingRect().toRect().height();
						newheight = (*it)->boundingRect().toRect().height() + CIlist[CIlist.size()-1].line.first()->y() + outsideMargin*1.5;

						newheight = oldheight>newheight?oldheight:newheight;
						
						ResizeElement(FutureParent,newwidth,newheight);
						(*it)->setParentItem(FutureParent);

						//set position for child
						leftpos = CIlist[CIlist.size()-1].line.last()->x() + CIlist[CIlist.size()-1].line.last()->boundingRect().toRect().width() + 2*outsideMargin*1.5;//last element in line's right border + margin
						(*it)->setPos(leftpos, CIlist[CIlist.size()-1].line.first()->y());

						CIlist[CIlist.size()-1].line.append(*it);
						FutureParent->setData(5,QVariant::fromValue<QList<ChildsItems>>(CIlist));
					}
					else//make new line
					{
						//set new size for parent						
						newheight = (*it)->boundingRect().toRect().height() + FutureParent->boundingRect().toRect().height() + 2*outsideMargin*1.5;
						oldheight = FutureParent->boundingRect().toRect().height() + outsideMargin*1.5;//for save only

						//maybe new item have bigger width than existing
						oldwidth = FutureParent->boundingRect().toRect().width();
						newwidth = (*it)->boundingRect().toRect().width() + 2*outsideMargin*1.5;

						newwidth = oldwidth>newwidth?oldwidth:newwidth;

						ResizeElement(FutureParent,newwidth,newheight);
						(*it)->setParentItem(FutureParent);

						//set position for child
						leftpos = CIlist[CIlist.size()-1].line.last()->x() + CIlist[CIlist.size()-1].line.last()->boundingRect().toRect().width() + outsideMargin*1.5;//last element in line's right border + margin
						(*it)->setPos(outsideMargin*1.5, oldheight);

						CI = ChildsItems();
						CI.line.append(*it);
						CIlist.append(CI);
						FutureParent->setData(5,QVariant::fromValue<QList<ChildsItems>>(CIlist));
					}
				}
			}
		}
	}

	FutureParent->setZValue(12);

	if(FutureParent->parentItem() != 0)//resize parent
	{
		ResizeParentAfterExitingElement(FutureParent->parentItem());
	}

	//recalculate text width
	QList<QGraphicsItem *> childrenn = FutureParent->childItems();

	for(QList<QGraphicsItem *>::iterator it = childrenn.begin(); it != childrenn.end(); it++)
	{
		if((*it)->data(1) == QVariant("TextContent"))//verify text length
		{
			CalculateTextLength(*it);
		}
	}

	MakeConnectionLines();

	ClearSelection();
}

void DiagComponentEditor::RemoveParentForComponent(QGraphicsItem * item)
{
	QGraphicsRectItem * parentt = (QGraphicsRectItem *)item->parentItem();

	//change data
	QList<ChildsItems> CIlist = parentt->data(6).value<QList<ChildsItems>>();
	bool founded = false;
	
	for(QList<ChildsItems>::iterator it1 = CIlist.begin(); it1 != CIlist.end(); it1++)
	{
		if(!founded)//let's find it
		{
			if((*it1).line.indexOf(item) != -1)//found
			{
				founded = true;
				(*it1).line.removeOne(item);
			}
		}
		else//join tail
		{
			if((*it1).line.size() != 0)
			{
				(*(it1-1)).line.append((*it1).line.first());//add to upper line
				(*it1).line.removeFirst();//remove from current
			}
		}
	}

	parentt->setData(6,QVariant::fromValue<QList<ChildsItems>>(CIlist));

	//clear parent
	item->setParentItem(0);
	item->setZValue(487);//bigger than 400, i think...

	//resize parent
	ResizeParentAfterExitingElement(parentt);

	MakeConnectionLines();//redraw connections
}

void DiagComponentEditor::ResizeParentAfterExitingElement(QGraphicsItem * parentt)
{
	if(parentt == 0)
	{
		qWarning()<<"Can't use 0 as parent! But it's NOT error sometimes...";
		return;
	}

	QList<QGraphicsItem *> childrenn = parentt->childItems();
	QList<QGraphicsItem *> components;
	int size = childrenn.size();

	for(int i=0; i<size; i++)
	{
		if(childrenn[i]->data(1) == QVariant("Component"))
		{
			childrenn[i]->setParentItem(0);
			components.append(childrenn[i]);
		}
	}

	//normalize
	QGraphicsItem * example = ComponentEl(GlobalID).getThisElement();
	GlobalID++;
	QGraphicsRectItem * rectPar = (QGraphicsRectItem *)parentt;
	rectPar->setData(5,QVariant(0));
	
	ResizeElement(rectPar,example->boundingRect().toRect().width(),example->boundingRect().toRect().height());

	MainScene->removeItem(example);

	//set childs back
	components.append(rectPar);//add parent in the end - so it will be parent again

	if(components.size() >= 2)//have childs
	{
		SetParentForComponents(components);
	}
	else if(rectPar->parentItem() != 0)//have parent
	{
		ResizeParentAfterExitingElement(rectPar->parentItem());
	}
}

void DiagComponentEditor::MakeConnectionLines()
{
	QList<QGraphicsItem *> items;
	QMap<QGraphicsItem *, LinkPoints> portsPairs; //key - linkline, value - struct with connection info
	QList<LinkPoints> linkstruct;
	
	items = MainScene->items();
	int sizej = items.size();
		
	for(int j=0; j<sizej; j++)
	{
		if(items[j]->data(1) == QVariant("InputPort") || items[j]->data(1) == QVariant("OutputPort"))
		{
			if(items[j]->data(4) != 0)//connect lines
			{
				linkstruct = items[j]->data(4).value<QList<LinkPoints>>();//get info about old connection

				int sizek = linkstruct.size();

				for(int k=0; k<sizek; k++)//for each connection
				{
					//try to get already found port from map
					if(!portsPairs.contains(linkstruct[k].connectionLine))
					{
						portsPairs.insert(linkstruct[k].connectionLine,linkstruct[k]);
					}
				}
			}

			if(items[j]->data(8) != 0)//as parent delegate
			{
				QList<LinkPoints> lp = items[j]->data(8).value<QList<LinkPoints>>();
				int size = lp.size();

				RemoveDelegateLine(items[j],true);//remove old
				
				//create new
				for(int i=0; i<size; i++)
				{
					MakeDelegateLine(lp[i].input, lp[i].output);
				}
			}

			if(items[j]->data(9) != 0)//as child delegate
			{
				LinkPoints lp = items[j]->data(9).value<LinkPoints>();

				//remove old
				RemoveDelegateLine(items[j],false);

				//create new
				MakeDelegateLine(lp.input, lp.output);
			}
		}
		else if(items[j]->data(1) == QVariant("CommCompLink"))
		{
			LinkPoints lp = items[j]->data(7).value<LinkPoints>();

			//remove old
			QList<QGraphicsItem *> lst;
			lst.append(lp.input);
			RemoveCommCompLink(lst);

			LinkCommentWithComponent(lp.input, lp.output);//recalculate
		}
	}

	//make new linklines
	QMap<QGraphicsItem *, LinkPoints>::iterator iter;
	QList<LinkPoints> listconc;

	for(iter = portsPairs.begin(); iter != portsPairs.end(); iter++)
	{
		//remove old
		//input
		listconc = iter.value().input->data(4).value<QList<LinkPoints>>();//get list
		listconc.removeOne(iter.value());//remove old connection
		iter.value().input->setData(4,QVariant::fromValue<QList<LinkPoints>>(listconc));//send list back

		//output
		listconc = iter.value().output->data(4).value<QList<LinkPoints>>();//get list
		listconc.removeOne(iter.value());//remove old connection
		iter.value().output->setData(4,QVariant::fromValue<QList<LinkPoints>>(listconc));

		//save select state
		bool WasSelected = iter.key()->data(3) == QVariant("Selected");

		MainScene->removeItem(iter.key());

		//make new
		MakeNewFormOfConnection(iter.value().output,iter.value().input,WasSelected);
	}
}

bool DiagComponentEditor::MakeLinesAfterLoad(QList<PairItems> pairs, int typeOfPairs)
{
	QList<QGraphicsItem *> items = MainScene->items();
	int sizei = pairs.size();
	int sizej = items.size();
	bool founded = false;

	for(int i=0; i<sizei; i++)
	{
		for(int j=0; j<sizej; j++)
		{
			if(items[j]->data(0) == QVariant(pairs[i].Id))
			{
				if(typeOfPairs == 0)
				{
					LinkCommentWithComponent(items[j], pairs[i].item);
				}
				else if(typeOfPairs == 1)
				{
					MakeDelegateLine(pairs[i].item, items[j]);
				}
				else if(typeOfPairs == 2)
				{
					MakeNewFormOfConnection(items[j],pairs[i].item,false);
				}
				
				founded = true;
			}
		}

		if(!founded)
		{
			QMessageBox::warning(this,QString::fromLocal8Bit("������!"),QString::fromLocal8Bit("�������� ��������� ���������! ������� ��������� �������!"));
			return false;
		}
	}

	return true;
}

void DiagComponentEditor::closeEvent(QCloseEvent * ev)
{
	if(WasMODIFIED && !QMessageBox::question(this,QString::fromLocal8Bit("������������� ��������"),QString::fromLocal8Bit("������������� ���� ��� �������. ������ ��������� ���������?"),QMessageBox::Ok,QMessageBox::Cancel))
	{
		SaveDCE();
	}

	ev->accept();
}