#ifndef COMMENTCLASS_H
#define COMMENTCLASS_H

#include <QGraphicsItem>
#include <QGraphicsRectItem>
#include <QGraphicsTextItem>
#include <QString>

#include <QFont>
#include <QBrush>
#include <QColor>
#include <QPen>
#include <QVariant>

class ElemComment
{
public:
	ElemComment();
	ElemComment(unsigned int  ID);

	//functions
	QGraphicsItem * getThisElement();
	void ColorSelecting(QBrush brush, QPen pen);

private:
	//variables
	QGraphicsPolygonItem * MainBlock;
	QGraphicsTextItem * CompName;
	QGraphicsRectItem * SelectRect;
};

#endif