/****************************************************************************
** Meta object code from reading C++ file 'diagcomponenteditor.h'
**
** Created: Mon 23. Dec 02:04:29 2013
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../diagcomponenteditor.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'diagcomponenteditor.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_DiagComponentEditor[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
      22,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      21,   20,   20,   20, 0x08,
      40,   20,   20,   20, 0x08,
      65,   20,   20,   20, 0x08,
      88,   20,   20,   20, 0x08,
     111,   20,   20,   20, 0x08,
     134,   20,   20,   20, 0x08,
     157,   20,   20,   20, 0x08,
     190,  188,   20,   20, 0x08,
     231,  188,   20,   20, 0x08,
     271,  188,   20,   20, 0x08,
     318,  314,   20,   20, 0x08,
     342,   20,   20,   20, 0x08,
     359,   20,   20,   20, 0x08,
     375,   20,   20,   20, 0x08,
     397,  393,   20,   20, 0x08,
     413,   20,   20,   20, 0x08,
     426,   20,   20,   20, 0x08,
     439,   20,   20,   20, 0x08,
     459,   20,   20,   20, 0x08,
     469,   20,   20,   20, 0x08,
     483,   20,   20,   20, 0x08,
     493,   20,   20,   20, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_DiagComponentEditor[] = {
    "DiagComponentEditor\0\0SwitchSelectMode()\0"
    "SwitchComponentAddMode()\0"
    "SwitchCommentAddMode()\0SwitchReqPortAddMode()\0"
    "SwitchPrdPortAddMode()\0SwitchCreateLinkMode()\0"
    "SwitchCreateDelegateLinkMode()\0e\0"
    "FixMouseClick(QGraphicsSceneMouseEvent*)\0"
    "FixMouseMove(QGraphicsSceneMouseEvent*)\0"
    "FixMouseRelease(QGraphicsSceneMouseEvent*)\0"
    "pos\0ShowContextMenu(QPoint)\0"
    "ConfirmEditing()\0CancelEditing()\0"
    "VerifyMaxLength()\0val\0ScaleScene(int)\0"
    "ShowHelpAP()\0ShowHelpAF()\0CreateNewDocument()\0"
    "SaveJPG()\0PrintResult()\0SaveDCE()\0"
    "LoadDCE()\0"
};

const QMetaObject DiagComponentEditor::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_DiagComponentEditor,
      qt_meta_data_DiagComponentEditor, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &DiagComponentEditor::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *DiagComponentEditor::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *DiagComponentEditor::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_DiagComponentEditor))
        return static_cast<void*>(const_cast< DiagComponentEditor*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int DiagComponentEditor::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: SwitchSelectMode(); break;
        case 1: SwitchComponentAddMode(); break;
        case 2: SwitchCommentAddMode(); break;
        case 3: SwitchReqPortAddMode(); break;
        case 4: SwitchPrdPortAddMode(); break;
        case 5: SwitchCreateLinkMode(); break;
        case 6: SwitchCreateDelegateLinkMode(); break;
        case 7: FixMouseClick((*reinterpret_cast< QGraphicsSceneMouseEvent*(*)>(_a[1]))); break;
        case 8: FixMouseMove((*reinterpret_cast< QGraphicsSceneMouseEvent*(*)>(_a[1]))); break;
        case 9: FixMouseRelease((*reinterpret_cast< QGraphicsSceneMouseEvent*(*)>(_a[1]))); break;
        case 10: ShowContextMenu((*reinterpret_cast< QPoint(*)>(_a[1]))); break;
        case 11: ConfirmEditing(); break;
        case 12: CancelEditing(); break;
        case 13: VerifyMaxLength(); break;
        case 14: ScaleScene((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 15: ShowHelpAP(); break;
        case 16: ShowHelpAF(); break;
        case 17: CreateNewDocument(); break;
        case 18: SaveJPG(); break;
        case 19: PrintResult(); break;
        case 20: SaveDCE(); break;
        case 21: LoadDCE(); break;
        default: ;
        }
        _id -= 22;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
