#include "portsclass.h"

PortClass::PortClass(bool isInput, uint ID)
{
	//create
	QVector<QPointF> vec;
	vec.append(QPointF(12,6));
	vec.append(QPointF(12,0));
	vec.append(QPointF(0,0));
	vec.append(QPointF(0,12));
	vec.append(QPointF(12,12));
	vec.append(QPointF(12,6));
	vec.append(QPointF(20,6));
	MainBlock = new QGraphicsPolygonItem(vec);
	
	vec.clear();
	vec.append(QPointF(20,6));
	vec.append(QPointF(28,6));
	OutPartLine = new QGraphicsPolygonItem(vec);
	OutPartLine->setData(1,QVariant("OutConnector"));

	//if input connector - form half-circle polygon
	if(isInput)
	{
		//half-circle
		vec.append(QPointF(28,6));
		vec.append(QPointF(28,9));
		vec.append(QPointF(29,11));
		vec.append(QPointF(30,12));
		vec.append(QPointF(32,13));
		vec.append(QPointF(35,13));

		vec.append(QPointF(32,13));
		vec.append(QPointF(30,12));
		vec.append(QPointF(29,11));
		vec.append(QPointF(28,9));
		vec.append(QPointF(28,6));
		vec.append(QPointF(28,3));
		vec.append(QPointF(29,1));
		vec.append(QPointF(30,0));
		vec.append(QPointF(32,-1));
		vec.append(QPointF(35,-1));

		vec.append(QPointF(32,-1));
		vec.append(QPointF(30,0));
		vec.append(QPointF(29,1));
		vec.append(QPointF(28,3));
		vec.append(QPointF(28,6));
		Connector = new QGraphicsPolygonItem(vec);	
	}
	else
	{
		Connector = new QGraphicsEllipseItem(28,1,10,10);
	}

	//form text
	CompName = new QGraphicsTextItem();
	CompName->setPlainText(QString::fromLocal8Bit(""));
	CompName->setPos(12,10);
	CompName->setTextWidth(130);
	CompName->setFont(QFont("Arial"));
	CompName->setData(1,QVariant("TextContent"));

	//form all object
	CompName->setParentItem(MainBlock);

	//color
	QBrush brush = QBrush(QColor(255,255,255));
	MainBlock->setBrush(brush);

	//form connector
	Connector->setZValue(50);
	Connector->setParentItem(OutPartLine);

	//form selection
	SelectRect = new QGraphicsRectItem(QRect(-3,-3,42,20),MainBlock);

	//form all object
	OutPartLine->setParentItem(MainBlock);
	SelectRect->setParentItem(MainBlock);

	//set properties for selcting
	SelectRect->setZValue(800);
	SelectRect->setBrush(Qt::NoBrush);
	SelectRect->setPen(Qt::NoPen);
	SelectRect->setData(1,QVariant("SelectingRect"));

	//change center of rotation to more comfortable for me
	MainBlock->setTransformOriginPoint(0,6);

	//set data
	MainBlock->setData(0,QVariant(ID));//id
	MainBlock->setData(1,QVariant(isInput?"InputPort":"OutputPort"));//type
	MainBlock->setData(2,QVariant("Main"));//is main part of block
	MainBlock->setData(3,QVariant("NotSelected"));//state
	MainBlock->setData(4,QVariant(0));//link line
	MainBlock->setData(8,QVariant(0));//delegate line parent
	MainBlock->setData(9,QVariant(0));//delegate line child
}

QGraphicsItem * PortClass::getThisElement()
{
	return MainBlock;
}

void PortClass::ColorSelecting(QBrush brush, QPen pen)
{
	SelectRect->setBrush(brush);
	SelectRect->setPen(pen);
}

QGraphicsTextItem * PortClass::getTextItem()
{
	return CompName;
}