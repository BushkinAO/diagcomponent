//work with files here

#include "diagcomponenteditor.h"

//slots
void DiagComponentEditor::SaveJPG()
{
	QString filename = QFileDialog::getSaveFileName(this,QString::fromLocal8Bit("������� � �����������"),QDir::currentPath(),QString("Images (*.jpg)"));
	
	QList<QGraphicsItem *> wasSelected;
	bool wasPlaceholder = Placeholder->isVisible();

	//hide all system components
	QList<QGraphicsItem *> items = ui.WorkPlace->items();

	for(QList<QGraphicsItem *>::iterator i = items.begin(); i != items.end(); i++)
	{
		if((*i)->data(1) == QVariant("SelectingRect"))
		{
			QGraphicsRectItem * rect = (QGraphicsRectItem *)*i;

			if(rect->brush() == QBrush(ColorForSelect))//was selected
			{
				wasSelected.append(*i);//save it state
				rect->setBrush(Qt::NoBrush);
			}
		}
	}

	Placeholder->setVisible(false);
	
	//save
	filename = filename.replace(QString("/"),QString("\\"));
	if(!filename.size() || !SaveAsImage(filename))
	{
		QMessageBox::warning(this,QString::fromLocal8Bit("������"),QString::fromLocal8Bit("�� ������� ��������� ��������� ��� �����������! ���������� ������� ������ ���� ��� ����������� %1").arg(filename));
	}

	//and show again
	for(QList<QGraphicsItem *>::iterator i = wasSelected.begin(); i != wasSelected.end(); i++)
	{
		QGraphicsRectItem * rect = (QGraphicsRectItem *)*i;
		rect->setBrush(QBrush(ColorForSelect));
	}

	Placeholder->setVisible(wasPlaceholder);
}

void DiagComponentEditor::PrintResult()
{
	//hide system data
	QList<QGraphicsItem *> wasSelected;
	bool wasPlaceholder = Placeholder->isVisible();

	//hide all system components
	QList<QGraphicsItem *> items = ui.WorkPlace->items();

	for(QList<QGraphicsItem *>::iterator i = items.begin(); i != items.end(); i++)
	{
		if((*i)->data(1) == QVariant("SelectingRect"))
		{
			QGraphicsRectItem * rect = (QGraphicsRectItem *)*i;

			if(rect->brush() == QBrush(ColorForSelect))//was selected
			{
				wasSelected.append(*i);//save it state
				rect->setBrush(Qt::NoBrush);
			}
		}
	}

	Placeholder->setVisible(false);


	//print
	QPrinter printer(QPrinter::HighResolution);
	QPixmap pixMap = QPixmap::grabWidget(ui.WorkPlace);
	
	QPrintDialog dialog(&printer, ui.WorkPlace);
	dialog.setOption(QAbstractPrintDialog::PrintToFile,false);
	dialog.setOption(QAbstractPrintDialog::PrintSelection,false);
	dialog.setOption(QAbstractPrintDialog::PrintPageRange,false);
	dialog.setOption(QAbstractPrintDialog::PrintShowPageSize);
	dialog.setOption(QAbstractPrintDialog::PrintCurrentPage,false);

	//form image
	QImage imageBackground(MainScene->width(), MainScene->height(), QImage::Format_RGB32);
	imageBackground.fill(QColor(Qt::white).rgb());

	QPainter painter(&imageBackground);
	MainScene->render(&painter);

	if(dialog.exec() == QDialog::Accepted)
	{
		pixMap = QPixmap::fromImage(imageBackground);
		pixMap = pixMap.scaled(printer.pageRect().width(), printer.pageRect().height(), Qt::KeepAspectRatio);

		QPainter painter1;
		painter1.begin(&printer);
		painter1.drawPixmap (0, 0, pixMap);
		painter1.end();
	}


	//restore system data
	for(QList<QGraphicsItem *>::iterator i = wasSelected.begin(); i != wasSelected.end(); i++)
	{
		QGraphicsRectItem * rect = (QGraphicsRectItem *)*i;
		rect->setBrush(QBrush(ColorForSelect));
	}

	Placeholder->setVisible(wasPlaceholder);
}

void DiagComponentEditor::SaveDCE()
{
	//get filename
	QString filename = QFileDialog::getSaveFileName(this,QString::fromLocal8Bit("����������"),QDir::currentPath(),QString("DiagramComponentEditor files (*.dce)"));

	//verify result
	if(!filename.isEmpty())
	{
		if(!SaveDocument(filename))
		{
			QMessageBox::warning(this,QString::fromLocal8Bit("������!"),QString::fromLocal8Bit("�� ���������� ��������� ���������!"));
		}
		else
		{
			QMessageBox::information(this,QString::fromLocal8Bit("�����"),QString::fromLocal8Bit("��������� �� ������:\n%1").arg(filename));
		}
	}
}

void DiagComponentEditor::LoadDCE()
{
	//get filename
	QString filename = QFileDialog::getOpenFileName(this,QString::fromLocal8Bit("����������"),QDir::currentPath(),QString("DiagramComponentEditor files (*.dce)"));

	//verify result
	if(!filename.isEmpty())
	{
		if(!LoadDocument(filename))
		{
			QMessageBox::warning(this,QString::fromLocal8Bit("������!"),QString::fromLocal8Bit("�� ���������� ��������� ���������!"));

			CreateNewDocument();
		}
		else
		{
			QMessageBox::information(this,QString::fromLocal8Bit("�����"),QString::fromLocal8Bit("���������!"));
		}
	}
}





//functions
bool DiagComponentEditor::SaveAsImage(QString fileName)
{
	QImage imageBackground(MainScene->width(), MainScene->height(), QImage::Format_RGB32);
	imageBackground.fill(QColor(Qt::white).rgb());

	QPainter painter(&imageBackground);
	MainScene->render(&painter);

	return imageBackground.save(fileName);
	//QPixmap pixMap = QPixmap::grabWidget(ui.WorkPlace);
	//return pixMap.save(fileName);
}

bool DiagComponentEditor::SaveDocument(QString filename)
{
	MainState = mSAVE;

	QList<QGraphicsItem *> allItems = MainScene->items();
	int size = allItems.size();

	QFile file(filename);
	file.open(QIODevice::WriteOnly);

	QXmlStreamWriter xmlWriter(&file);
	xmlWriter.setAutoFormatting(true);
	xmlWriter.writeStartDocument();

	xmlWriter.writeStartElement("Start");
	xmlWriter.writeTextElement("GlobalID",QString::number(GlobalID));
	xmlWriter.writeTextElement("Scale",QString::number(ui.ScaleSpin->value()));

	for(int i=0; i<size; i++)
	{
		if(allItems[i]->parentItem() == 0)
		{
			if(allItems[i]->data(1) == QVariant("Component"))
			{
				if(!WriteComponent(&xmlWriter,allItems[i]))
				{
					file.close();
					return false;
				}
			}
			else if(allItems[i]->data(1) == QVariant("Comment"))
			{
				if(!WriteComment(&xmlWriter,allItems[i]))
				{
					file.close();
					return false;
				}
			}
		}
	}

	xmlWriter.writeEndElement();
	xmlWriter.writeEndDocument();
	file.close();

	MainState = mSELECT;

	this->setWindowTitle(filename);

	return true;
}

bool DiagComponentEditor::LoadDocument(QString filename)
{
	if(WasMODIFIED && !QMessageBox::question(this,QString::fromLocal8Bit("������������� ��������"),QString::fromLocal8Bit("������������� ���� ��� �������. ������ ��������� ���������?"),QMessageBox::Ok,QMessageBox::Cancel))
	{
		SaveDCE();
	}

	CreateNewDocument();

	MainState = mLOAD;

	QFile file(filename);
	
	if (!file.open(QFile::ReadOnly | QFile::Text))
	{
		QMessageBox::warning(this,QString::fromLocal8Bit("������!"),QString::fromLocal8Bit("�� ������� ��������� ����!"));
		return false;
	}

	QXmlStreamReader Rxml(&file);
	Rxml.readNextStartElement();

	while(!Rxml.atEnd())
	{
		if(Rxml.isStartElement())
		{
			if(Rxml.name() == "Start")
			{
				Rxml.readNext();
				continue;
			}
			else if(Rxml.name() == "GlobalID")
			{
				Rxml.readNext();
				GlobalID = Rxml.text().toString().toUInt();
			}
			else if(Rxml.name() == "Scale")
			{
				Rxml.readNext();
				ui.ScaleSpin->setValue(Rxml.text().toString().toInt());
			}
			else if(Rxml.isStartElement())
			{
				if(Rxml.name() == "Comment")
				{
					if(!LoadComment(&Rxml))
					{
						QMessageBox::warning(this,QString::fromLocal8Bit("������!"),QString::fromLocal8Bit("�� ���� �������� �����������!"));
						return false;
					}
				}
				else if(Rxml.name() == "Component")
				{
					if(!LoadComponent(&Rxml))
					{
						QMessageBox::warning(this,QString::fromLocal8Bit("������!"),QString::fromLocal8Bit("�� ���� �������� ���������!"));
						return false;
					}
				}
				else if(Rxml.name() == "InputPort" || Rxml.name() == "OutputPort")
				{
					QMessageBox::warning(this,QString::fromLocal8Bit("������!"),QString::fromLocal8Bit("�� ��������� ����!"));
					return false;
				}
			}
			else if(!Rxml.isStartElement() && !Rxml.isEndElement() && !Rxml.isStartDocument() && !Rxml.isEndDocument())
			{
				Rxml.raiseError(QString::fromLocal8Bit("�������������� �������"));
			}
		}
		
		Rxml.readNext();
	}

	file.close();

	if (Rxml.hasError())
	{
		qWarning()<<Rxml.errorString()<<" "<<Rxml.error();
		QMessageBox::warning(this,QString::fromLocal8Bit("������!"),QString::fromLocal8Bit("�� ������� ���������� ����! ��������, ���� ���������. ������������ ������:\n\n%1").arg(Rxml.errorString()));
		return false;
	}
	else if (file.error() != QFile::NoError)
	{
		QMessageBox::warning(this,QString::fromLocal8Bit("������!"),QString::fromLocal8Bit("�� ������� ��������� ����!"));
		return false;
	}

	//MAKE ALL LINKS HERE
	//Linked Comments
	bool f1 = MakeLinesAfterLoad(LoadingCons.LinkedComments,0);

	//Declared Ports
	bool f2 = MakeLinesAfterLoad(LoadingCons.DeclaredPorts,1);

	//Linked Ports
	bool f3 = MakeLinesAfterLoad(LoadingCons.ConnectedPorts,2);

	MainState = mSELECT;
	ui.WorkPlace->setSceneRect(-ui.WorkPlace->width()/2,-ui.WorkPlace->height()/2,ui.WorkPlace->width(),ui.WorkPlace->height());

	this->setWindowTitle(filename);

	return f1 && f2 && f3;
}



//write components

bool DiagComponentEditor::WriteComment(QXmlStreamWriter *fileForWrite, QGraphicsItem *item)
{
	//get text
	QString text = QString();

	QList<QGraphicsItem *> chlds = item->childItems();
	int size = chlds.size();
	bool found = false;

	for(int i=0; i<size; i++)
	{
		if(chlds[i]->data(1) == QVariant("TextContent"))
		{
			QGraphicsTextItem * tmp = (QGraphicsTextItem *)chlds[i];
			text = tmp->toPlainText();
			found = true;
			break;
		}
	}

	if(!found)
	{
		QMessageBox::warning(this,QString::fromLocal8Bit("������!"),QString::fromLocal8Bit("��������� ����������� �������(%1). ���������� � ����� ���� ����������").arg(QString::fromLocal8Bit("�����������")));
		return false;
	}

	fileForWrite->writeStartElement("Comment");
	fileForWrite->writeTextElement("ID",item->data(0).toString());
	fileForWrite->writeTextElement("Text",text);
	fileForWrite->writeTextElement("X",QString::number(item->x()));
	fileForWrite->writeTextElement("Y",QString::number(item->y()));
	fileForWrite->writeEndElement();

	return true;
}

bool DiagComponentEditor::WriteComponent(QXmlStreamWriter * fileForWrite, QGraphicsItem * item)
{
	//get text
	QString text = QString();

	QList<QGraphicsItem *> chlds = item->childItems();
	int size = chlds.size();
	bool found = false;

	for(int i=0; i<size; i++)
	{
		if(chlds[i]->data(1) == QVariant("TextContent"))
		{
			QGraphicsTextItem * tmp = (QGraphicsTextItem *)chlds[i];
			text = tmp->toPlainText();
			found = true;
			break;
		}
	}

	if(!found)
	{
		QMessageBox::warning(this,QString::fromLocal8Bit("������!"),QString::fromLocal8Bit("��������� ����������� �������(%1). ���������� � ����� ���� ����������").arg(QString::fromLocal8Bit("���������")));
		return false;
	}

	fileForWrite->writeStartElement("Component");
	fileForWrite->writeTextElement("ID",item->data(0).toString());
	fileForWrite->writeTextElement("Text",text);
	fileForWrite->writeTextElement("X",QString::number(item->x()));
	fileForWrite->writeTextElement("Y",QString::number(item->y()));

	//get linked comment - data 7
	if(item->data(7) != QVariant(0))
	{
		fileForWrite->writeTextElement("ConnectedComment",item->data(7).value<QGraphicsItem *>()->data(7).value<LinkPoints>().input->data(0).toString());
	}

	//get children components - data 5
	if(item->data(5) != QVariant(0))
	{
		fileForWrite->writeStartElement("Children_components");

		QList<ChildsItems> CIlist = item->data(5).value<QList<ChildsItems>>();
		QList<QGraphicsItem *> CurList;

		int sizek;
		size = CIlist.size();

		for(int i=0; i<size; i++)
		{
			CurList = CIlist[i].line;

			sizek = CurList.size();
			for(int k=0; k<sizek; k++)
			{
				if(!WriteComponent(fileForWrite,CurList[k]))
				{
					QMessageBox::warning(this,QString::fromLocal8Bit("������!"),QString::fromLocal8Bit("��������� ����������� �������(%1). ���������� �� ����� ���� ����������").arg(QString::fromLocal8Bit("���������")));
					return false;
				}
			}
		}
		
		fileForWrite->writeEndElement();
	}

	//get other children - ports
	chlds = item->childItems();
	size = chlds.size();

	qDebug()<<"\n\nNew";
	for(int i=0; i<size; i++)
	{
		qDebug()<<chlds[i]->data(1);
		if(chlds[i]->data(1) == QVariant("InputPort") || chlds[i]->data(1) == QVariant("OutputPort"))
		{
			if(!WritePort(fileForWrite,chlds[i]))
			{
				QMessageBox::warning(this,QString::fromLocal8Bit("������!"),QString::fromLocal8Bit("��������� ����������� �������(%1). ���������� �� ����� ���� ����������").arg(QString::fromLocal8Bit("����")));
				return false;
			}
		}
	}
	qDebug()<<"End\n\n\n";


	fileForWrite->writeEndElement();

	return true;
}

bool DiagComponentEditor::WritePort(QXmlStreamWriter *fileForWrite, QGraphicsItem *item)
{
	//get text
	QString text = QString();

	QList<QGraphicsItem *> chlds = item->childItems();
	int size = chlds.size();

	for(int i=0; i<size; i++)
	{
		if(chlds[i]->data(1) == QVariant("TextContent"))
		{
			QGraphicsTextItem * tmp = (QGraphicsTextItem *)chlds[i];
			text = tmp->toPlainText();
			break;
		}
	}

	QString PortType = item->data(1).toString();

	fileForWrite->writeStartElement(PortType);
	fileForWrite->writeTextElement("ID",item->data(0).toString());
	fileForWrite->writeTextElement("Text",text);
	fileForWrite->writeTextElement("X",QString::number(item->x()));
	fileForWrite->writeTextElement("Y",QString::number(item->y()));
	fileForWrite->writeTextElement("Rotation",QString::number(item->rotation()));

	//get connected ports - data 4
	if(item->data(4) != QVariant(0) && PortType == QString("InputPort"))
	{
		fileForWrite->writeStartElement("Connected_with_this");

		QList<LinkPoints> CIlist = item->data(4).value<QList<LinkPoints>>();
		QGraphicsItem * NotThisItem;

		int sizek;
		int size = CIlist.size();

		for(int i=0; i<size; i++)
		{
			NotThisItem = CIlist[i].output;
				
			fileForWrite->writeStartElement("Conected_Port");
			fileForWrite->writeTextElement("ID",NotThisItem->data(0).toString());
			fileForWrite->writeEndElement();
		}

		fileForWrite->writeEndElement();
	}

	//get connected with delegate line
	qDebug()<<item->data(8).toString();
	if(item->data(8) != QVariant(0))//have delegated children
	{
		fileForWrite->writeStartElement("Delegated_children");

		QList<LinkPoints> lp = item->data(8).value<QList<LinkPoints>>();
		int sizep = lp.size();

		for(int p = 0; p<sizep; p++)
		{
			fileForWrite->writeStartElement("Child_Port");
			fileForWrite->writeTextElement("ID",lp[p].output->data(0).toString());
			fileForWrite->writeEndElement();
		}

		fileForWrite->writeEndElement();
	}

	fileForWrite->writeEndElement();

	return true;
}


//read components

bool DiagComponentEditor::LoadComment(QXmlStreamReader *fileForRead)
{
	ElemComment comment;
	QGraphicsItem * result;

	int Counter = 0;

	fileForRead->readNext();

	while(!(fileForRead->name() == "Comment" && fileForRead->isEndElement()))
	{
		qDebug()<<fileForRead->name();
		if(fileForRead->isStartElement())
		{
			if(Counter == 0 && fileForRead->name() == "ID")
			{
				fileForRead->readNext();

				comment = ElemComment(fileForRead->text().toString().toUInt());
				comment.ColorSelecting(QBrush(Qt::NoBrush), QPen(Qt::NoPen));
				result = comment.getThisElement();

				MainScene->addItem(result);
			}
			else if(Counter == 1 && fileForRead->name() == "Text")
			{
				fileForRead->readNext();

				QList<QGraphicsItem *> chlds = result->childItems();
				int size = chlds.size();

				QString txtHere = fileForRead->text().toString();

				if(txtHere.size() > 27)
				{
					QMessageBox::warning(this,QString::fromLocal8Bit("������!"),QString::fromLocal8Bit("��������� ���������� ����� ������ ����������. ���� ���� �������������� � ������ - �� ���� ���� ����������."));
					txtHere = txtHere.remove(8,txtHere.size()-8);
				}

				for(int i=0; i<size; i++)
				{
					if(chlds[i]->data(1) == QVariant("TextContent"))
					{
						QGraphicsTextItem * tmp = (QGraphicsTextItem *)chlds[i];
						tmp->setPlainText(txtHere);
						break;
					}
				}
			}
			else if(Counter == 2 && fileForRead->name() == "X")
			{
				fileForRead->readNext();

				result->setX(fileForRead->text().toString().toFloat());
			}
			else if(Counter == 3 && fileForRead->name() == "Y")
			{
				fileForRead->readNext();

				result->setY(fileForRead->text().toString().toFloat());

				//remapping
				QPointF poss = result->scenePos();
				result->setPos(ui.WorkPlace->mapFromScene(poss));
			}
			else
			{
				QMessageBox::warning(this,QString::fromLocal8Bit("������!"),QString::fromLocal8Bit("�������������� �������"));
				return false;
			}

			Counter++;
		}

		fileForRead->readNext();
	}

	return true;
}

int DiagComponentEditor::LoadComponent(QXmlStreamReader *fileForRead)
{
	ComponentEl component;
	QGraphicsItem * result;
	unsigned int idForRet = 0;
	unsigned int answer = 0;
	QGraphicsTextItem * textItem = 0;

	QList<unsigned int> listports;
	QList<unsigned int> chldsComps;

	int Counter = 0;

	fileForRead->readNext();

	while(!(fileForRead->name() == "Component" && fileForRead->isEndElement()))
	{
		qDebug()<<fileForRead->name()<<"   "<<fileForRead->text();
		if(fileForRead->isStartElement())
		{
			if(Counter == 0 && fileForRead->name() == "ID")
			{
				fileForRead->readNext();

				idForRet = fileForRead->text().toString().toUInt();

				component = ComponentEl(idForRet);
				component.ColorSelecting(QBrush(Qt::NoBrush), QPen(Qt::NoPen));
				result = component.getThisElement();
				result->setPos(0,0);

				MainScene->addItem(result);
			}
			else if(Counter == 1 && fileForRead->name() == "Text")
			{
				fileForRead->readNext();

				QList<QGraphicsItem *> chlds = result->childItems();
				int size = chlds.size();

				for(int i=0; i<size; i++)
				{
					if(chlds[i]->data(1) == QVariant("TextContent"))
					{
						textItem = (QGraphicsTextItem *)chlds[i];
						textItem->setPlainText(fileForRead->text().toString());
						break;
					}
				}
			}
			else if(Counter == 2 && fileForRead->name() == "X")
			{
				fileForRead->readNext();

				result->setX(fileForRead->text().toString().toFloat());
			}
			else if(Counter == 3 && fileForRead->name() == "Y")
			{
				fileForRead->readNext();

				result->setY(fileForRead->text().toString().toFloat());

				//remapping
				QPointF poss = result->scenePos();
				result->setPos(ui.WorkPlace->mapFromScene(poss));
			}
			else if(Counter >= 4 && fileForRead->name() == "ConnectedComment")
			{
				fileForRead->readNext();

				unsigned int id = fileForRead->text().toString().toUInt();

				PairItems pairIts;
				pairIts.item = result;
				pairIts.Id = id;

				LoadingCons.LinkedComments.append(pairIts);
			}
			else if(Counter >= 4 && fileForRead->name() == "Children_components")
			{
				fileForRead->readNext();
				fileForRead->readNext();
				
				qDebug()<<fileForRead->name();
				if(fileForRead->name() == "Component")
				{
					answer = LoadComponent(fileForRead);
					
					if(!answer)
					{
						QMessageBox::warning(this,QString::fromLocal8Bit("������!"),QString::fromLocal8Bit("�� ���� �������� �������� ���������!"));
						return 0;
					}

					chldsComps.append(answer);
				}
				else
				{
					QMessageBox::warning(this,QString::fromLocal8Bit("������!"),QString::fromLocal8Bit("�� ���� �������� �������� ���������!"));
					return 0;
				}
			}
			else if(Counter >= 4 && fileForRead->name() == "OutputPort" || fileForRead->name() == "InputPort")
			{
				answer = LoadPort(fileForRead);

				if(!answer)
				{
					QMessageBox::warning(this,QString::fromLocal8Bit("������!"),QString::fromLocal8Bit("�� ���� �������� ����!"));
					return 0;
				}

				listports.append(answer);
			}
			else
			{
				QMessageBox::warning(this,QString::fromLocal8Bit("������!"),QString::fromLocal8Bit("�������������� �������"));
				return 0;
			}

			Counter++;
		}

		fileForRead->readNext();
	}

	if(fileForRead->hasError())
	{
		qWarning()<<fileForRead->error()<<"   "<<fileForRead->errorString();
	}

	QList<QGraphicsItem *> itemss = MainScene->items();
	int sizew = itemss.size();
	int sizew2 = listports.size();
	
	for(int w=0; w<sizew; w++)
	{
		for(int w2=0; w2<sizew2; w2++)
		{
			qDebug()<<itemss[w]->data(0);
			if(itemss[w]->data(0) == QVariant(listports[w2]))
			{
				itemss[w]->setParentItem(result);
			}
		}
	}

	//set parent if needed
	if(!fileForRead->hasError() && chldsComps.size())
	{
		QList<QGraphicsItem *> chItems;
		sizew = itemss.size();
		sizew2 = chldsComps.size();
	
		for(int w=0; w<sizew; w++)
		{
			for(int w2=0; w2<sizew2; w2++)
			{
				qDebug()<<itemss[w]->data(1);
				if(itemss[w]->data(0) == QVariant(chldsComps[w2]) && itemss[w]->data(1) == QVariant("Component"))
				{
					chItems.append(itemss[w]);
				}
			}
		}

		//save port's positions
		QList<QGraphicsItem *> chldsIts = result->childItems();
		int sizek = chldsIts.size();
		QList<PortAndItsCoords> ports;
		PortAndItsCoords tmp;

		//save ports positions
		for(int k=0; k<sizek; k++)
		{
			if(chldsIts[k]->data(1) == QVariant("InputPort") || chldsIts[k]->data(1) == QVariant("OutputPort"))
			{
				tmp.port = chldsIts[k];
				tmp.coords = chldsIts[k]->pos();

				ports.append(tmp);
			}
		}

		chItems.append(result);
		SetParentForComponents(chItems);

		//sestore ports positions after resizing their parent
		sizek = ports.size();
		for(int k=0; k<sizek; k++)
		{
			ports[k].port->setPos(ports[k].coords);
		}
	}
	else if(textItem != 0)
	{
		CalculateTextLength(textItem);
	}
	else
	{
		QMessageBox::warning(this,QString::fromLocal8Bit("������!"),QString::fromLocal8Bit("������������ ���������!"));
		return 0;
	}

	return idForRet;
}

int DiagComponentEditor::LoadPort(QXmlStreamReader *fileForRead)
{
	PortClass port;
	QGraphicsItem * result;
	unsigned int idForRet = 0;
	unsigned int answer = 0;
	bool PortType = true;

	QGraphicsTextItem * textItem;

	int Counter = 0;

	QList<QGraphicsItem *> chldsComps;

	while(!((fileForRead->name() == "InputPort" || fileForRead->name() == "OutputPort") && fileForRead->isEndElement()))
	{
		if(fileForRead->isStartElement())
		{
			if(Counter == 0 && fileForRead->name() == "InputPort")
			{
				PortType = true;
			}
			else if(Counter == 0 && fileForRead->name() == "OutputPort")
			{
				PortType = false;
			}
			else if(Counter == 1 && fileForRead->name() == "ID")
			{
				fileForRead->readNext();

				idForRet = fileForRead->text().toString().toUInt();

				port = PortClass(PortType,idForRet);
				port.ColorSelecting(QBrush(Qt::NoBrush), QPen(Qt::NoPen));
				result = port.getThisElement();

				MainScene->addItem(result);
			}
			else if(Counter == 2 && fileForRead->name() == "Text")
			{
				fileForRead->readNext();

				QList<QGraphicsItem *> chlds = result->childItems();
				int size = chlds.size();

				QString txtHere = fileForRead->text().toString();

				if(txtHere.size() > 8)
				{
					QMessageBox::warning(this,QString::fromLocal8Bit("��������������!"),QString::fromLocal8Bit("��������� ���������� ����� ������ ����������. ���� ���� �������������� � ������ - �� ���� ���� ����������."));
					txtHere = txtHere.remove(8,txtHere.size()-8);
				}

				for(int i=0; i<size; i++)
				{
					if(chlds[i]->data(1) == QVariant("TextContent"))
					{
						textItem = (QGraphicsTextItem *)chlds[i];
						textItem->setPlainText(txtHere);
						break;
					}
				}
			}
			else if(Counter == 3 && fileForRead->name() == "X")
			{
				fileForRead->readNext();

				result->setX(fileForRead->text().toString().toFloat());
			}
			else if(Counter == 4 && fileForRead->name() == "Y")
			{
				fileForRead->readNext();

				result->setY(fileForRead->text().toString().toFloat());
			}
			else if(Counter == 5 && fileForRead->name() == "Rotation")
			{
				fileForRead->readNext();

				float angleTh = fileForRead->text().toString().toInt();
				
				result->setRotation(angleTh);

				//rotate sub-text
				if(angleTh == 90)
				{
					textItem->setPos(16,-5);
					textItem->rotate(-90);
				}
				else if(angleTh == 180)
				{
					textItem->setPos(130,-3);
					textItem->rotate(-180);
				}
				else if(angleTh == -90)
				{
					textItem->setPos(36,14);
					textItem->rotate(90);
				}//else is no needed
			}
			else if(Counter >= 6 && fileForRead->name() == "Connected_with_this")
			{
				fileForRead->readNext();

				qDebug()<<fileForRead->name();
				do
				{
					fileForRead->readNext();

					qDebug()<<fileForRead->name();
					if(fileForRead->isStartElement())
					{
						if(fileForRead->name() == "Conected_Port")
						{
							continue;
						}
						else if(fileForRead->name() == "ID")
						{
							fileForRead->readNext();

							unsigned int id = fileForRead->text().toString().toUInt();

							PairItems pairIts;
							pairIts.item = result;
							pairIts.Id = id;

							LoadingCons.ConnectedPorts.append(pairIts);
						}
						else
						{
							QMessageBox::warning(this,QString::fromLocal8Bit("������!"),QString::fromLocal8Bit("�� ���� ��������� ��������� ����!"));
							return 0;
						}
					}
				}
				while(!(fileForRead->name() == "Connected_with_this" && fileForRead->isEndElement()));
			}
			else if(Counter >= 6 && fileForRead->name() == "Delegated_children")
			{
				fileForRead->readNext();

				qDebug()<<fileForRead->name();
				do
				{
					fileForRead->readNext();

					qDebug()<<fileForRead->name();
					if(fileForRead->isStartElement())
					{
						if(fileForRead->name() == "Child_Port")
						{
							continue;
						}
						else if(fileForRead->name() == "ID")
						{
							fileForRead->readNext();

							unsigned int id = fileForRead->text().toString().toUInt();

							PairItems pairIts;
							pairIts.item = result;
							pairIts.Id = id;

							LoadingCons.DeclaredPorts.append(pairIts);
						}
						else
						{
							QMessageBox::warning(this,QString::fromLocal8Bit("������!"),QString::fromLocal8Bit("�� ���� ��������� ��������� delegate ����!"));
							return 0;
						}
					}
				}
				while(!(fileForRead->name() == "Delegated_children" && fileForRead->isEndElement()));
			}
			else
			{
				QMessageBox::warning(this,QString::fromLocal8Bit("������!"),QString::fromLocal8Bit("�������������� �������"));
				return 0;
			}

			Counter++;
		}

		fileForRead->readNext();
	}

	return idForRet;
}