#ifndef WORKSCENE_H
#define WORKSCENE_H

#include <QGraphicsScene>
#include <QPointF>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsSceneDragDropEvent>

class WorkScene : public QGraphicsScene
{
	Q_OBJECT

signals:
	void mousePressed(QGraphicsSceneMouseEvent * e);
	void mouseMove(QGraphicsSceneMouseEvent * e);
	void mouseReleased(QGraphicsSceneMouseEvent * e);

protected:
    virtual void mousePressEvent(QGraphicsSceneMouseEvent *e);
	virtual void mouseMoveEvent(QGraphicsSceneMouseEvent *e);
	virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent *e);
};

#endif