/********************************************************************************
** Form generated from reading UI file 'diagcomponenteditor.ui'
**
** Created: Tue 24. Sep 10:05:05 2013
**      by: Qt User Interface Compiler version 4.7.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIAGCOMPONENTEDITOR_H
#define UI_DIAGCOMPONENTEDITOR_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGraphicsView>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QStatusBar>
#include <QtGui/QToolBar>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionNew_file;
    QAction *actionOpen_file;
    QAction *actionSave;
    QAction *actionSave_as;
    QAction *actionExit;
    QAction *actionTracing;
    QAction *actionKegl;
    QAction *actionCopy;
    QAction *actionPaste;
    QAction *actionDelete;
    QAction *actionColor_of_line;
    QAction *actionColor_of_area;
    QAction *actionHelp_about_program;
    QAction *actionHelp_about_functions;
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout;
    QPushButton *AddItemButton;
    QPushButton *AddConnectionButton;
    QSpacerItem *verticalSpacer;
    QGraphicsView *WorkPlace;
    QMenuBar *menuBar;
    QMenu *menuUMLComponentMaker;
    QMenu *menuEdit;
    QMenu *menuFont;
    QMenu *menuColor;
    QMenu *menuService;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(850, 560);
        actionNew_file = new QAction(MainWindow);
        actionNew_file->setObjectName(QString::fromUtf8("actionNew_file"));
        actionOpen_file = new QAction(MainWindow);
        actionOpen_file->setObjectName(QString::fromUtf8("actionOpen_file"));
        actionSave = new QAction(MainWindow);
        actionSave->setObjectName(QString::fromUtf8("actionSave"));
        actionSave_as = new QAction(MainWindow);
        actionSave_as->setObjectName(QString::fromUtf8("actionSave_as"));
        actionExit = new QAction(MainWindow);
        actionExit->setObjectName(QString::fromUtf8("actionExit"));
        actionTracing = new QAction(MainWindow);
        actionTracing->setObjectName(QString::fromUtf8("actionTracing"));
        actionKegl = new QAction(MainWindow);
        actionKegl->setObjectName(QString::fromUtf8("actionKegl"));
        actionCopy = new QAction(MainWindow);
        actionCopy->setObjectName(QString::fromUtf8("actionCopy"));
        actionPaste = new QAction(MainWindow);
        actionPaste->setObjectName(QString::fromUtf8("actionPaste"));
        actionDelete = new QAction(MainWindow);
        actionDelete->setObjectName(QString::fromUtf8("actionDelete"));
        actionColor_of_line = new QAction(MainWindow);
        actionColor_of_line->setObjectName(QString::fromUtf8("actionColor_of_line"));
        actionColor_of_area = new QAction(MainWindow);
        actionColor_of_area->setObjectName(QString::fromUtf8("actionColor_of_area"));
        actionHelp_about_program = new QAction(MainWindow);
        actionHelp_about_program->setObjectName(QString::fromUtf8("actionHelp_about_program"));
        actionHelp_about_functions = new QAction(MainWindow);
        actionHelp_about_functions->setObjectName(QString::fromUtf8("actionHelp_about_functions"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        horizontalLayout = new QHBoxLayout(centralWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        AddItemButton = new QPushButton(centralWidget);
        AddItemButton->setObjectName(QString::fromUtf8("AddItemButton"));

        verticalLayout->addWidget(AddItemButton);

        AddConnectionButton = new QPushButton(centralWidget);
        AddConnectionButton->setObjectName(QString::fromUtf8("AddConnectionButton"));

        verticalLayout->addWidget(AddConnectionButton);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        horizontalLayout->addLayout(verticalLayout);

        WorkPlace = new QGraphicsView(centralWidget);
        WorkPlace->setObjectName(QString::fromUtf8("WorkPlace"));

        horizontalLayout->addWidget(WorkPlace);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 850, 21));
        menuUMLComponentMaker = new QMenu(menuBar);
        menuUMLComponentMaker->setObjectName(QString::fromUtf8("menuUMLComponentMaker"));
        menuEdit = new QMenu(menuBar);
        menuEdit->setObjectName(QString::fromUtf8("menuEdit"));
        menuFont = new QMenu(menuEdit);
        menuFont->setObjectName(QString::fromUtf8("menuFont"));
        menuColor = new QMenu(menuEdit);
        menuColor->setObjectName(QString::fromUtf8("menuColor"));
        menuService = new QMenu(menuBar);
        menuService->setObjectName(QString::fromUtf8("menuService"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuUMLComponentMaker->menuAction());
        menuBar->addAction(menuEdit->menuAction());
        menuBar->addAction(menuService->menuAction());
        menuUMLComponentMaker->addAction(actionNew_file);
        menuUMLComponentMaker->addAction(actionOpen_file);
        menuUMLComponentMaker->addAction(actionSave);
        menuUMLComponentMaker->addAction(actionSave_as);
        menuUMLComponentMaker->addAction(actionExit);
        menuEdit->addAction(menuFont->menuAction());
        menuEdit->addAction(menuColor->menuAction());
        menuEdit->addAction(actionCopy);
        menuEdit->addAction(actionPaste);
        menuEdit->addAction(actionDelete);
        menuFont->addAction(actionTracing);
        menuFont->addAction(actionKegl);
        menuColor->addAction(actionColor_of_line);
        menuColor->addAction(actionColor_of_area);
        menuService->addAction(actionHelp_about_program);
        menuService->addAction(actionHelp_about_functions);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "UMLComponentMaker", 0, QApplication::UnicodeUTF8));
        actionNew_file->setText(QApplication::translate("MainWindow", "New file", 0, QApplication::UnicodeUTF8));
        actionOpen_file->setText(QApplication::translate("MainWindow", "Open file", 0, QApplication::UnicodeUTF8));
        actionSave->setText(QApplication::translate("MainWindow", "Save", 0, QApplication::UnicodeUTF8));
        actionSave_as->setText(QApplication::translate("MainWindow", "Save as...", 0, QApplication::UnicodeUTF8));
        actionExit->setText(QApplication::translate("MainWindow", "Exit", 0, QApplication::UnicodeUTF8));
        actionTracing->setText(QApplication::translate("MainWindow", "Tracing", 0, QApplication::UnicodeUTF8));
        actionKegl->setText(QApplication::translate("MainWindow", "Kegl", 0, QApplication::UnicodeUTF8));
        actionCopy->setText(QApplication::translate("MainWindow", "Copy", 0, QApplication::UnicodeUTF8));
        actionPaste->setText(QApplication::translate("MainWindow", "Paste", 0, QApplication::UnicodeUTF8));
        actionDelete->setText(QApplication::translate("MainWindow", "Delete", 0, QApplication::UnicodeUTF8));
        actionColor_of_line->setText(QApplication::translate("MainWindow", "Color of line", 0, QApplication::UnicodeUTF8));
        actionColor_of_area->setText(QApplication::translate("MainWindow", "Color of area", 0, QApplication::UnicodeUTF8));
        actionHelp_about_program->setText(QApplication::translate("MainWindow", "Help about programm", 0, QApplication::UnicodeUTF8));
        actionHelp_about_functions->setText(QApplication::translate("MainWindow", "Help about functions", 0, QApplication::UnicodeUTF8));
        AddItemButton->setText(QApplication::translate("MainWindow", "Add Item", 0, QApplication::UnicodeUTF8));
        AddConnectionButton->setText(QApplication::translate("MainWindow", "Add Connection", 0, QApplication::UnicodeUTF8));
        menuUMLComponentMaker->setTitle(QApplication::translate("MainWindow", "File", 0, QApplication::UnicodeUTF8));
        menuEdit->setTitle(QApplication::translate("MainWindow", "Edit", 0, QApplication::UnicodeUTF8));
        menuFont->setTitle(QApplication::translate("MainWindow", "Font", 0, QApplication::UnicodeUTF8));
        menuColor->setTitle(QApplication::translate("MainWindow", "Color", 0, QApplication::UnicodeUTF8));
        menuService->setTitle(QApplication::translate("MainWindow", "Service", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIAGCOMPONENTEDITOR_H
