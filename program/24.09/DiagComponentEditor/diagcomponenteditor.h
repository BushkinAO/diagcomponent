#ifndef DIAGCOMPONENTEDITOR_H
#define DIAGCOMPONENTEDITOR_H

#include <QtGui/QMainWindow>

#include "ui_diagcomponenteditor.h"
#include "additemdialog.h"

class DiagComponentEditor : public QMainWindow
{
	Q_OBJECT

public:
	DiagComponentEditor(QWidget *parent = 0, Qt::WFlags flags = 0);
	~DiagComponentEditor();

private:
	Ui::MainWindow ui;

private slots:
	void ShowItemDialog();//show dialog to create item on work field
};

#endif // DIAGCOMPONENTEDITOR_H
