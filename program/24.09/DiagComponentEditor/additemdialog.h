#ifndef ADDDIALOG_H
#define ADDDIALOG_H

#include <QDialog>
#include <QRect>
#include <QApplication>
#include <QDesktopWidget>
#include <QPushButton>
#include <QRadioButton>
#include <QGroupBox>

#include <QGraphicsView>
#include <QGraphicsScene>

#include <QGraphicsItem>
#include <QGraphicsRectItem>
#include <QGraphicsEllipseItem>
#include <QGraphicsLineItem>
#include <QGraphicsTextItem>
#include <QGraphicsPolygonItem>

#include <QVector>
#include <QPointF>

class AddDialogItem : public QDialog
{
	Q_OBJECT

	public:
		AddDialogItem(int widthScreen = 450,int heightScreen = 350);
		~AddDialogItem();

	private:
		QGraphicsScene * Sc1;
		QGraphicsScene * Sc2;

	private slots:
		void AcceptItem();
		void ChangeImageOfType();
};

#endif //ADDDIALOG_H