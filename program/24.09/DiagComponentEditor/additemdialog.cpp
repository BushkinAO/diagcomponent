#include "additemdialog.h"

AddDialogItem::AddDialogItem(int widthScreen, int heightScreen) 
	: QDialog()
{
	//set size of dialog
	QRect ScreenSize = QApplication::desktop()->screenGeometry();

	this->setGeometry((ScreenSize.width() - widthScreen)/2,(ScreenSize.height() - heightScreen)/2,widthScreen,heightScreen);
	this->setMaximumSize(this->width(),this->height());
	this->setMinimumSize(this->width(),this->height());

	//group of radio
	QPushButton * OkButton = new QPushButton("Accept",this);
	OkButton->setGeometry(this->width()*4/5,this->height()*8/9,this->width()/5,this->height()/9);

	QGroupBox * TypeItem = new QGroupBox("What's creting?",this);
	TypeItem->setGeometry(10,10,this->width()/5,this->height()/5);

	QRadioButton * ComponentItem = new QRadioButton("Component",TypeItem);
	ComponentItem->setGeometry(10,10,TypeItem->width(),TypeItem->height()/2);
	ComponentItem->setObjectName("ComponentItem");
	ComponentItem->setChecked(true);

	QRadioButton * CommentItem = new QRadioButton("Comment",TypeItem);
	CommentItem->setGeometry(10,TypeItem->height()/2,TypeItem->width(),TypeItem->height()/2);
	CommentItem->setObjectName("CommentItem");


	//graphic view
	QGraphicsView * ShowFutureItemView = new QGraphicsView(this);
	ShowFutureItemView->setObjectName("GrView");
	ShowFutureItemView->setGeometry(TypeItem->width()+10,10,this->width()-TypeItem->width()-15,this->height()-OkButton->height()-15);

	//Scenes
	Sc1 = new QGraphicsScene();
	QGraphicsRectItem * RectComponent_1 = new QGraphicsRectItem(QRect(0,0,100,50),NULL,Sc1);
	QGraphicsRectItem * RectComponent_2_1 = new QGraphicsRectItem(QRect(-15,5,30,10),NULL,Sc1);
	QGraphicsRectItem * RectComponent_2_2 = new QGraphicsRectItem(QRect(-15,35,30,10),NULL,Sc1);
	QGraphicsTextItem * TextComponent = new QGraphicsTextItem("Text here",NULL,Sc1);

	Sc2 = new QGraphicsScene();
	QVector<QPointF> polg;
	polg.append(QPointF(0,0));
	polg.append(QPointF(90,0));
	polg.append(QPointF(100,10));
	polg.append(QPointF(100,50));
	polg.append(QPointF(0,50));

	QGraphicsPolygonItem * PolygonComment = new QGraphicsPolygonItem(QPolygonF(polg),NULL,Sc2);
	QGraphicsTextItem * TextComment = new QGraphicsTextItem("Text here",NULL,Sc2);

	//connects
	connect(OkButton,SIGNAL(clicked()),this,SLOT(AcceptItem()));
	connect(ComponentItem,SIGNAL(toggled(bool)),this,SLOT(ChangeImageOfType()));
	connect(CommentItem,SIGNAL(toggled(bool)),this,SLOT(ChangeImageOfType()));
}

AddDialogItem::~AddDialogItem()
{

}

void AddDialogItem::AcceptItem()
{
	accept();
}

void AddDialogItem::ChangeImageOfType()
{
	QGraphicsView * GrView = this->findChild<QGraphicsView *>("GrView");

	if(this->findChild<QRadioButton *>("ComponentItem")->isChecked())
	{
		GrView->setScene(Sc1);
	}
	else
	{
		GrView->setScene(Sc2);
	}
	
	GrView->show();
}