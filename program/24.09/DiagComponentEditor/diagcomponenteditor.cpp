#include "diagcomponenteditor.h"

DiagComponentEditor::DiagComponentEditor(QWidget *parent, Qt::WFlags flags)
	: QMainWindow(parent, flags)
{
	ui.setupUi(this);

	//connects
	connect(ui.AddItemButton,SIGNAL(clicked()),this,SLOT(ShowItemDialog()));
}

DiagComponentEditor::~DiagComponentEditor()
{

}

void DiagComponentEditor::ShowItemDialog()
{
	AddDialogItem CreateItem = AddDialogItem();
	CreateItem.exec();
}
