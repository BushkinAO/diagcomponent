#ifndef COMPONENTCLASS_H
#define COMPONENTCLASS_H

#include <QGraphicsItem>
#include <QGraphicsRectItem>
#include <QGraphicsTextItem>
#include <QString>

#include <QFont>
#include <QBrush>
#include <QColor>
#include <QPen>
#include <QVariant>

class ComponentEl
{
public:
	ComponentEl();

	//functions
	QGraphicsItem * getThisElement();
	void ColorSelecting(QColor color, QPen pen);

private:
	//variables
	QGraphicsRectItem * MainBlock;
	QGraphicsTextItem * CompName;
	QGraphicsRectItem * SelectRect;
	
	QGraphicsRectItem * IconMain;
	QGraphicsRectItem * IconUp;
	QGraphicsRectItem * IconDown;
};

#endif