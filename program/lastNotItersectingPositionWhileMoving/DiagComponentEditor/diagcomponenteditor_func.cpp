//methods(not slots) here

#include "diagcomponenteditor.h"

//status Item - to find full component by item
//status Diagram - find main parent of this item in all diagram
//status All - find maximal parent, without conditions
QGraphicsItem * DiagComponentEditor::FindMainParent(QGraphicsItem *item, QString status)
{
	QGraphicsItem * currentParent = item->parentItem();

	if(status == QString("Item"))
	{
		if(item->data(2) == QVariant("Main"))
		{
			return item;
		}
		else if(currentParent != 0)
		{
			return FindMainParent(currentParent,status);
		}
		else
		{
			return 0;
		}
	}
	else if(status == QString("Diagram"))
	{
		if(item->data(2) == QVariant("Main") && currentParent->data(2) != QVariant("Main"))
		{
			return item;
		}
		else if(currentParent != 0)
		{
			return FindMainParent(currentParent,status);
		}
		else
		{
			return 0;
		}
	}
	else if(status == QString("All"))
	{
		if(currentParent == 0)
		{
			return item;
		}
		else
		{
			return FindMainParent(currentParent,status);
		}
	}
	else
	{
		return 0;
	}
}

void DiagComponentEditor::TurnOnPlaceholder(QGraphicsItem * item, bool onoff)
{
	//clear placeholder
	TurnOffPlaceholder();

	//show placeholder
	item->setParentItem(Placeholder);
	Placeholder->setVisible(onoff);

	//form placeholder size
	QGraphicsItem * selecting = GetSelectionItem(item);
	if(selecting != 0)
	{
		Placeholder->setRect(selecting->boundingRect());
	}

	ui.WorkPlace->setMouseTracking(true);
}

void DiagComponentEditor::TurnOffPlaceholder()
{
	//hide placeholder
	Placeholder->setVisible(false);
	ui.WorkPlace->setMouseTracking(false);

	//clear after ports
	ItemForAddPorts = NULL;//clear item to work with - it will be again finded when click will been
	Placeholder->setRotation(0);

	//clear placeholder
	QList<QGraphicsItem *> tmp = Placeholder->childItems();
	if(!tmp.isEmpty())
	{
		MainScene->removeItem(tmp.first());
	}
}

// return objects, itersects with items, if it's components or comments without parents
QList<QGraphicsItem *> DiagComponentEditor::ItersectSomething(QGraphicsItem *item)
{
	QList<QGraphicsItem *> PlshlCollidedWith  = item->collidingItems();
	QList<QGraphicsItem *> children = item->childItems();
	QList<QGraphicsItem *>::iterator i;

	for(i=PlshlCollidedWith.begin(); i<PlshlCollidedWith.end(); i++)
	{
		QGraphicsItem * tmp = FindMainParent((*i),"Diagram");

		if(tmp != 0 && PlshlCollidedWith.indexOf(tmp) != -1 && (*i) != tmp && (tmp->data(1) == QVariant("Component") || tmp->data(1) == QVariant("Comment")))//founded parent already exist in array AND not itself (*i)
		{
			PlshlCollidedWith.removeOne((*i));
		}
	}

	//clear placeholder parts
	for(i=children.begin(); i<children.end(); i++)
	{
		PlshlCollidedWith.removeOne(*i);
	}

	PlshlCollidedWith.removeOne(item);//remove item itself

	return PlshlCollidedWith;//after filter
}

void DiagComponentEditor::SelectIt(QGraphicsItem * item, bool onoff)
{
	QGraphicsItem * selecting = GetSelectionItem(item);
	
	if(selecting != 0)//item have selecting child
	{
			selecting->setVisible(onoff);
			selecting->setOpacity(0.35);
			item->setData(3, QVariant((onoff)?"Selected":"NotSelected"));

			return;
	}
}

QGraphicsItem * DiagComponentEditor::GetSelectionItem(QGraphicsItem *item)
{
	QList<QGraphicsItem *> children = item->childItems();

	for(int i=0; i<children.size(); i++)
	{
		if(children[i]->data(1) == QVariant("SelectingRect"))
		{
			return children[i];//selection item found and returned
		}
	}

	return 0;//nothing found - strange, but everything can be
}

int DiagComponentEditor::countTaggedElements(QList<QGraphicsItem*> list, QVariant tag)
{
	int count = 0;

	for(QList<QGraphicsItem *>::iterator i = list.begin(); i != list.end(); i++)
	{
		if((*i)->data(1) == tag)
		{
			count++;
		}
	}

	return count;
}

void DiagComponentEditor::SelectOneItem(QGraphicsItem *clicked)
{
	if(SelectedItems.isEmpty() || !SelectedItems.isEmpty() && clicked->parentItem() == SelectedItems.first()->parentItem())//something already selected, and new have the same parent?
	{
		//mark selected
		SelectIt(clicked,true);
		clicked->setZValue(20);
		SelectedItems.append(clicked);
	}
}