#include "componentitemclass.h"

ComponentEl::ComponentEl()
{
	//create
	MainBlock = new QGraphicsRectItem(0,0,150,75);
	IconMain = new QGraphicsRectItem(0,0,15,12);
	IconUp = new QGraphicsRectItem(10,2,10,3);
	IconDown = new QGraphicsRectItem(10,7,10,3);
	SelectRect = new QGraphicsRectItem(MainBlock->boundingRect(),MainBlock);

	//color
	QBrush whitBr = QBrush(QColor(255,255,255));

	MainBlock->setBrush(whitBr);
	IconMain->setBrush(whitBr);
	IconUp->setBrush(whitBr);
	IconDown->setBrush(whitBr);
	
	//form icon
	IconUp->setParentItem(IconMain);
	IconDown->setParentItem(IconMain);

	IconMain->setPos(120,10);

	//form text
	CompName = new QGraphicsTextItem();
	CompName->setPlainText(QString::fromLocal8Bit("Название"));
	CompName->setPos(10,25);
	CompName->setTextWidth(140);
	CompName->setFont(QFont("Arial",-1,QFont::DemiBold));

	//form all object
	IconMain->setParentItem(MainBlock);
	CompName->setParentItem(MainBlock);

	//set properties for selcting
	SelectRect->setZValue(100);
	SelectRect->setVisible(false);
	SelectRect->setData(1,QVariant("SelectingRect"));

	//set data
	MainBlock->setData(1,QVariant("Component"));//type
	MainBlock->setData(2,QVariant("Main"));//is main part of block
	MainBlock->setData(3,QVariant("NotSelected"));//state
}

QGraphicsItem * ComponentEl::getThisElement()
{
	return MainBlock;
}

void ComponentEl::ColorSelecting(QColor color, QPen pen)
{
	QBrush brush = QBrush(color);

	SelectRect->setBrush(brush);
	SelectRect->setPen(pen);
}