//work with files here

#include "diagcomponenteditor.h"

//slots
void DiagComponentEditor::SaveJPG()
{
	QString filename = QFileDialog::getSaveFileName(this,QString::fromLocal8Bit("������� � �����������"),QDir::currentPath(),QString("Images (*.jpg)"));
	
	//hide all system components
	QList<QGraphicsItem *> items = ui.WorkPlace->items();

	for(QList<QGraphicsItem *>::iterator i = items.begin(); i != items.end(); i++)
	{
		if((*i)->data(1) == QVariant("SelectingRect") && (*i)->isVisible())
		{
			(*i)->setVisible(false);
		}
	}

	Placeholder->setVisible(false);
	
	//save
	SaveAsImage(filename);

	//and show again
	for(QList<QGraphicsItem *>::iterator i = items.begin(); i != items.end(); i++)
	{
		if((*i)->data(1) == QVariant("SelectingRect") && (*i)->isVisible())
		{
			(*i)->setVisible(true);
		}
	}

	Placeholder->setVisible(true);
}



//functions
void DiagComponentEditor::SaveAsImage(QString fileName)
{
	QPixmap pixMap = QPixmap::grabWidget(ui.WorkPlace);
	pixMap.save(fileName);
}