#include "workscene.h"

void WorkScene::mousePressEvent(QGraphicsSceneMouseEvent *e)
{
	emit mousePressed(e);
}

void WorkScene::mouseMoveEvent(QGraphicsSceneMouseEvent *e)
{
	emit mouseMove(e);
}

void WorkScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *e)
{
	emit mouseReleased(e);
}