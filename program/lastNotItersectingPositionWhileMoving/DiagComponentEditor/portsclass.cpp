#include "portsclass.h"

PortClass::PortClass(bool isInput)
{
	//create
	QVector<QPointF> vec;
	vec.append(QPointF(12,6));
	vec.append(QPointF(12,0));
	vec.append(QPointF(0,0));
	vec.append(QPointF(0,12));
	vec.append(QPointF(12,12));
	vec.append(QPointF(12,6));
	vec.append(QPointF(20,6));
	MainBlock = new QGraphicsPolygonItem(vec);
	
	vec.clear();
	vec.append(QPointF(20,6));
	vec.append(QPointF(28,6));
	OutPartLine = new QGraphicsPolygonItem(vec);

	//if input connector - form half-circle polygon
	if(isInput)
	{
		//hohoho
		vec.append(QPointF(28,6));
		vec.append(QPointF(28,9));
		vec.append(QPointF(29,11));
		vec.append(QPointF(30,12));
		vec.append(QPointF(32,13));
		vec.append(QPointF(35,13));

		vec.append(QPointF(32,13));
		vec.append(QPointF(30,12));
		vec.append(QPointF(29,11));
		vec.append(QPointF(28,9));
		vec.append(QPointF(28,6));
		vec.append(QPointF(28,3));
		vec.append(QPointF(29,1));
		vec.append(QPointF(30,0));
		vec.append(QPointF(32,-1));
		vec.append(QPointF(35,-1));

		vec.append(QPointF(32,-1));
		vec.append(QPointF(30,0));
		vec.append(QPointF(29,1));
		vec.append(QPointF(28,3));
		vec.append(QPointF(28,6));
		Connector = new QGraphicsPolygonItem(vec);	
	}
	else
	{
		Connector = new QGraphicsEllipseItem(28,isInput?0:1,isInput?12:10,isInput?12:10);
	}

		//color
	QBrush brush = QBrush(QColor(255,255,255));
	MainBlock->setBrush(brush);

	//form connector
	Connector->setZValue(-10);
	Connector->setParentItem(OutPartLine);

	//form selection
	SelectRect = new QGraphicsRectItem(MainBlock->boundingRect(),MainBlock);

	//form all object
	OutPartLine->setParentItem(MainBlock);
	SelectRect->setParentItem(MainBlock);

	//set properties for selcting
	SelectRect->setZValue(100);
	SelectRect->setVisible(false);
	SelectRect->setData(1,QVariant("SelectingRect"));

	//change center of rotation to more comfortable for me
	MainBlock->setTransformOriginPoint(0,6);

	//set data
	MainBlock->setData(1,QVariant(isInput?"InputPort":"OutputPort"));//type
	MainBlock->setData(2,QVariant("Main"));//is main part of block
	MainBlock->setData(3,QVariant("NotSelected"));//state
}

QGraphicsItem * PortClass::getThisElement()
{
	return MainBlock;
}

void PortClass::ColorSelecting(QColor color, QPen pen)
{
	QBrush brush = QBrush(color);

	SelectRect->setBrush(brush);
	SelectRect->setPen(pen);
}