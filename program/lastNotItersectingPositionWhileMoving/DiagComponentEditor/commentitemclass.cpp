#include "commentitemclass.h"

ElemComment::ElemComment()
{
	//create
	QVector<QPointF> vec;
	vec.append(QPointF(150,10));
	vec.append(QPointF(150,75));
	vec.append(QPointF(0,75));
	vec.append(QPointF(0,0));
	vec.append(QPointF(140,0));
	vec.append(QPointF(150,10));
	vec.append(QPointF(140,10));
	vec.append(QPointF(140,0));
	MainBlock = new QGraphicsPolygonItem(vec);
	SelectRect = new QGraphicsRectItem(MainBlock->boundingRect(),MainBlock);

	//color
	QBrush brush = QBrush(QColor(255,255,255));
	MainBlock->setBrush(brush);

	//form text
	CompName = new QGraphicsTextItem();
	CompName->setPlainText(QString::fromLocal8Bit("��� �����������"));
	CompName->setPos(10,10);
	CompName->setTextWidth(140);
	CompName->setFont(QFont("Arial"));

	//form all object
	CompName->setParentItem(MainBlock);

	//set properties for selcting
	SelectRect->setZValue(100);
	SelectRect->setVisible(false);
	SelectRect->setData(1,QVariant("SelectingRect"));

	//set data
	MainBlock->setData(1,QVariant("Comment"));//type
	MainBlock->setData(2,QVariant("Main"));//is main part of block
	MainBlock->setData(3,QVariant("NotSelected"));//state
}

QGraphicsItem * ElemComment::getThisElement()
{
	return MainBlock;
}

void ElemComment::ColorSelecting(QColor color, QPen pen)
{
	QBrush brush = QBrush(color);

	SelectRect->setBrush(brush);
	SelectRect->setPen(pen);
}