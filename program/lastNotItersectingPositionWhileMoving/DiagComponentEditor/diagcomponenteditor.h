#ifndef DIAGCOMPONENTEDITOR_H
#define DIAGCOMPONENTEDITOR_H

#include <QtGui/QMainWindow>

#include "ui_diagcomponenteditor.h"
#include "componentitemclass.h"
#include "commentitemclass.h"
#include "portsclass.h"
#include "workscene.h"

#include <QString>
#include <QGraphicsScene>
#include <QList>
#include <QAction>
#include <QMenu>
#include <QRect>
#include <QPointF>

#include <QPixmap>
#include <QFileDialog>
#include <QDir>

#include <QMessageBox>
#include <QDebug>

class DiagComponentEditor : public QMainWindow
{
	Q_OBJECT

public:
	DiagComponentEditor(QWidget *parent = 0, Qt::WFlags flags = 0);
	~DiagComponentEditor();

	enum WhatsStateActive {mSELECT = 10, mCOMPONENT, mCOMMENT, mRecPORT, mPrdPORT, mPortsLINK, sITEMS};

private:
	Ui::MainWindow ui;

	//variables
	WhatsStateActive MainState;//states for left menu buttons
	WorkScene * MainScene;//work scene
	
	QColor ColorForSelect;
	QColor ColorError;
	QColor ColorMain;

	QList<QGraphicsItem *> SelectedItems;//list of selected items
	QGraphicsRectItem * Placeholder;//placeholder
	QGraphicsItem * ItemForAddPorts;//item, which chosen to add port

	QPointF LastAllowedPosition;//last NOT itersect positon when move selected
	bool ItersectWhileMoving;//is seldcted items intersect something?
	
	//functions
	//in file diagcomponenteditor_fun.cpp
	QGraphicsItem * FindMainParent(QGraphicsItem * item, QString status);//find parent of this item, that must associate with full component
	QList<QGraphicsItem *> ItersectSomething(QGraphicsItem * item);//is item intersect comment or component on MainScene

	void TurnOnPlaceholder(QGraphicsItem * item, bool onoff);//truen on placeholder, add to it item and set it's visibility on onoff
	void TurnOffPlaceholder();//turn off placeholder

	void SelectIt(QGraphicsItem * item, bool onoff);//turn select on/off
	void SelectOneItem(QGraphicsItem * clicked);//select clicked or not, add to selected - fully complicated selection

	QGraphicsItem * GetSelectionItem(QGraphicsItem * item);//get selection item of input item
	int countTaggedElements(QList<QGraphicsItem *> list, QVariant tag);


	//files functions
	//in file diagcomponenteditor_files.cpp
	void SaveAsImage(QString fileName);//save work scene in file with name fileName

private slots://in file diagcomponenteditor.cpp
	void SwitchSelectMode();//turn on select mode
	void SwitchComponentAddMode();//turn on component additing mode
	void SwitchCommentAddMode();//turn on comment additing mode
	void SwitchReqPortAddMode();//turn on required port additing mode
	void SwitchPrdPortAddMode();//turn on required port additing mode
	void SwitchCreateLinkMode();//turn on create link mode

	void FixMouseClick(QGraphicsSceneMouseEvent *e);//fix mouse click on main scene
	void FixMouseMove(QGraphicsSceneMouseEvent *e);//fix mouse move on  main scene
	void FixMouseRelease(QGraphicsSceneMouseEvent *e);//fix mouse move on  main scene

	void ShowContextMenu(QPoint pos);//show context menu

	//in file diagcomponenteditor_files.cpp
	void SaveJPG();//save work place as jpg image
};

#endif // DIAGCOMPONENTEDITOR_H
