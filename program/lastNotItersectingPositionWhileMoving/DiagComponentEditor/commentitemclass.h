#ifndef COMMENTCLASS_H
#define COMMENTCLASS_H

#include <QGraphicsItem>
#include <QGraphicsRectItem>
#include <QGraphicsTextItem>
#include <QString>

#include <QFont>
#include <QBrush>
#include <QColor>
#include <QPen>
#include <QVariant>

class ElemComment
{
public:
	ElemComment();

	//functions
	QGraphicsItem * getThisElement();
	void ColorSelecting(QColor color, QPen pen);

private:
	//variables
	QGraphicsPolygonItem * MainBlock;
	QGraphicsTextItem * CompName;
	QGraphicsRectItem * SelectRect;
};

#endif