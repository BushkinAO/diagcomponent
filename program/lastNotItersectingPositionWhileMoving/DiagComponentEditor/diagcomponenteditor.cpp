//Constructor and slots here

#include "diagcomponenteditor.h"

DiagComponentEditor::DiagComponentEditor(QWidget *parent, Qt::WFlags flags)
	: QMainWindow(parent, flags)
{
	ui.setupUi(this);//set ui

	MainScene = new WorkScene();//construct main scene
	
	//Colors
	ColorForSelect = QColor(130,200,240);
	ColorError = QColor(210,87,89);
	ColorMain = QColor(255,255,255);

	ui.WorkPlace->setContextMenuPolicy(Qt::CustomContextMenu);//off default context menu

	//set placeholder settings
	Placeholder = new QGraphicsRectItem(0,0,0,0,0,MainScene);
	Placeholder->setBrush(QBrush(ColorForSelect));
	Placeholder->setPen(Qt::NoPen);
	Placeholder->setOpacity(0.33);
	Placeholder->setVisible(false);
	Placeholder->setZValue(1000);

	SwitchSelectMode();//set mode to SELECT
	
	ItersectWhileMoving = false;//at start we have correct diagram or clear workplace
		
	//connects
	//left menu
	connect(ui.SelectModeButton,SIGNAL(clicked()),this,SLOT(SwitchSelectMode()));
	connect(ui.ComponentModeButton,SIGNAL(clicked()),this,SLOT(SwitchComponentAddMode()));
	connect(ui.CommentModeButton,SIGNAL(clicked()),this,SLOT(SwitchCommentAddMode()));
	connect(ui.ProvidedPortButton,SIGNAL(clicked()),this,SLOT(SwitchPrdPortAddMode()));
	connect(ui.RequiredPortButton,SIGNAL(clicked()),this,SLOT(SwitchReqPortAddMode()));
	connect(ui.LinkButton,SIGNAL(clicked()),this,SLOT(SwitchCreateLinkMode()));

	//workscene resignal to this
	connect(MainScene,SIGNAL(mousePressed(QGraphicsSceneMouseEvent *)),this,SLOT(FixMouseClick(QGraphicsSceneMouseEvent *)));
	connect(MainScene,SIGNAL(mouseMove(QGraphicsSceneMouseEvent *)),this,SLOT(FixMouseMove(QGraphicsSceneMouseEvent *)));
	connect(MainScene,SIGNAL(mouseReleased(QGraphicsSceneMouseEvent *)),this,SLOT(FixMouseRelease(QGraphicsSceneMouseEvent *)));

	//context menu
	connect(ui.WorkPlace,SIGNAL(customContextMenuRequested(QPoint)),this,SLOT(ShowContextMenu(QPoint)));

	//files
	connect(ui.actionSave_as,SIGNAL(triggered(bool)),this,SLOT(SaveJPG()));

	ui.WorkPlace->setScene(MainScene);
	ui.WorkPlace->setSceneRect(0,0,ui.WorkPlace->width(),ui.WorkPlace->height());
}

DiagComponentEditor::~DiagComponentEditor()
{

}

//slots

void DiagComponentEditor::SwitchSelectMode()
{
	MainState = mSELECT;

	//off
	TurnOffPlaceholder();
}

void DiagComponentEditor::SwitchComponentAddMode()
{
	MainState = mCOMPONENT;

	//on
	ComponentEl newItemRaw = ComponentEl();
	QGraphicsItem * tmp = newItemRaw.getThisElement();
	
	TurnOnPlaceholder(tmp, true);
}

void DiagComponentEditor::SwitchCommentAddMode()
{
	MainState = mCOMMENT;

	//on
	ElemComment newItemRaw = ElemComment();
	QGraphicsItem * tmp = newItemRaw.getThisElement();

	TurnOnPlaceholder(tmp, true);
}

void DiagComponentEditor::SwitchReqPortAddMode()
{
	MainState = mRecPORT;

	//on
	PortClass newItemRaw = PortClass(true);
	QGraphicsItem * tmp = newItemRaw.getThisElement();
	
	TurnOnPlaceholder(tmp,false);
}

void DiagComponentEditor::SwitchPrdPortAddMode()
{
	MainState = mPrdPORT;

	//on
	PortClass newItemRaw = PortClass(false);
	QGraphicsItem * tmp = newItemRaw.getThisElement();
	
	TurnOnPlaceholder(tmp, false);
}

void DiagComponentEditor::SwitchCreateLinkMode()
{
	MainState = mPortsLINK;

	//off
	TurnOffPlaceholder();
}

void DiagComponentEditor::FixMouseClick(QGraphicsSceneMouseEvent *e)
{
	QPointF pos = e->scenePos();

	QGraphicsItem * pressed = MainScene->itemAt(pos);
	QGraphicsItem * clicked = 0;

	if(pressed != 0)//find something
	{
		clicked = FindMainParent(pressed,"Item");//find whole item - if it's not it

		//remove all linked with placeholder
		QList<QGraphicsItem *> children = Placeholder->childItems();
		int size = children.size();

		for(int i=0; i<size; i++)
		{
			if(clicked == children[i])//it was placeholder, nothing to do here
			{
				clicked = 0;
				break;
			}
		}
	}

	//work with states
	if(e->button() == Qt::LeftButton)
	{
		if(MainState == mCOMPONENT && Placeholder->brush() == QBrush(ColorForSelect))
		{
			ComponentEl newItemRaw = ComponentEl();
			newItemRaw.ColorSelecting(ColorForSelect, QPen(Qt::NoPen));
			QGraphicsItem * newItem = newItemRaw.getThisElement();
			newItem->setPos(pos);

			MainScene->addItem(newItem);
		}
		else if(MainState == mCOMMENT && Placeholder->brush() == QBrush(ColorForSelect))
		{
			ElemComment newItemRaw = ElemComment();
			newItemRaw.ColorSelecting(ColorForSelect, QPen(Qt::NoPen));
			QGraphicsItem * newItem = newItemRaw.getThisElement();
			newItem->setPos(pos);

			MainScene->addItem(newItem);
		}
		else if(MainState == mSELECT)
		{
			//need to add this element to selected?
			if(clicked != 0 && clicked->data(3) == QVariant("NotSelected"))
			{
				SelectOneItem(clicked);
			}
		}
		else if(MainState == mRecPORT || MainState == mPrdPORT)
		{
			//is clicked element can have ports?
			if(clicked != 0 && clicked->data(1) == QVariant("Component") && ItemForAddPorts != clicked)
			{
				//mark selectedclicked item
				ItemForAddPorts = clicked;
			}
			else if(Placeholder->brush() == QBrush(ColorForSelect))//this click seems port placed andnot intersect something forbitten
			{
				QGraphicsItem * chPlaceholder = Placeholder->childItems().first();//here must be only one element in array - tagged mainblock
				PortClass newPort;

				if(MainState == mRecPORT)
				{
					newPort = PortClass(true);
				}
				else//mPrdPORT
				{
					newPort = PortClass(false);
				}

				//create new port object
				newPort.ColorSelecting(ColorForSelect, QPen(Qt::NoPen));
				QGraphicsItem * newItem = newPort.getThisElement();

				//form place gor additing port - because center of this object is not in (0,0)
				QPointF placeRaw = chPlaceholder->scenePos();
				QPointF placeGrilled;
				qreal sizeOffset = newItem->transformOriginPoint().y();
				
				if(Placeholder->rotation() == 90)
				{
					placeGrilled = QPointF(placeRaw.x()-sizeOffset, placeRaw.y()-sizeOffset);
				}
				else if(Placeholder->rotation() == 180)
				{
					placeGrilled = QPointF(placeRaw.x(), placeRaw.y()-sizeOffset*2);
				}
				else if(Placeholder->rotation() == -90)
				{
					placeGrilled = QPointF(placeRaw.x()+sizeOffset, placeRaw.y()-sizeOffset);
				}
				else
				{
					placeGrilled = placeRaw;
				}

				//add new port
				newItem->setRotation(Placeholder->rotation());
				newItem->setParentItem(ItemForAddPorts);
				
				placeGrilled = ItemForAddPorts->mapFromScene(placeGrilled);
				newItem->setPos(placeGrilled);
				
				MainScene->addItem(newItem);//add on scene
			}
		}
		

		//remove selection
		if(clicked == 0)
		{
			int size = SelectedItems.size();

			for(int i=0; i<size; i++)//free linked item from selection
			{
				SelectIt(SelectedItems[i],false);
				SelectedItems[i]->setZValue(1);
				SelectedItems[i]->setData(3,QVariant("NotSelected"));
			}
					
			//clear list
			SelectedItems.clear();
		}
	}

	ui.WorkPlace->update();
}

void DiagComponentEditor::FixMouseMove(QGraphicsSceneMouseEvent *e)
{
	//moving selected element
	if(MainState == mSELECT && e->buttons() & Qt::LeftButton)
	{
		//finding under cursor item with tag SelectingRect
		if(MainScene->itemAt(e->scenePos())->data(1) == QVariant("SelectingRect"))
		{
			int size = SelectedItems.size();
			bool changed = false;
			QList<QGraphicsItem *> children;
			int sizej;

			for(int i=0; i<size; i++)
			{
				children = SelectedItems[i]->childItems();
				children.append(SelectedItems[i]);//items itself MUST also be verified
				sizej = children.size();

				for(int j=0; j<sizej; j++)
				{
					//grab selctingrect of item and verify it on collision. if it have no collicsion - all elemen have not collision
					if(children[j]->data(1) == QVariant("SelectingRect"))
					{
						//find itersection
						QList<QGraphicsItem *> IntersectionCurrent = ItersectSomething(children[j]);
						IntersectionCurrent.removeOne(SelectedItems[i]);//remove selected item itself - it cant collide with itself

						//remove all that not ports or components
						for(QList<QGraphicsItem *>::iterator k = IntersectionCurrent.begin(); k != IntersectionCurrent.end(); k++)
						{
							if((*k)->data(1) != QVariant("InputPort") && (*k)->data(1) != QVariant("OutputPort") && (*k)->data(1) != QVariant("Component"))
							{
								IntersectionCurrent.removeOne((*k));
							}
						}

						if(IntersectionCurrent.size())
						{
							ItersectWhileMoving = true;
							changed = true;
						}

						break;//no need to find else intersection, if selected rect not intersect. it's time economy
					}
				}
			}

			if(!changed && ItersectWhileMoving)//no inersection was found for ALL children
			{
				ItersectWhileMoving = false;
			}

			//save last rigth position
			if(!ItersectWhileMoving)
			{
				LastAllowedPosition = e->lastScenePos();
			}
		
			//move all selected items
			size = SelectedItems.size();
			for(int i=0; i<size; i++)
			{
				SelectedItems[i]->setPos(SelectedItems[i]->scenePos() + (e->scenePos() - e->lastScenePos()));
			}
		}
	}
	

	//placeholder
	if(ItemForAddPorts != 0)//show port if need
	{
		Placeholder->setVisible(true);
	}

	//if we have something to move...
	if(Placeholder->isVisible())
	{		
		//maybe intersect with something?
		QList<QGraphicsItem *> intersection = ItersectSomething(Placeholder);

		//move placeholder - depends on type of additing element
		QGraphicsItem * chPlaceholder = Placeholder->childItems().first();//here must be only one element in array - tagged mainblock

		//choose type item under placeholder
		if(chPlaceholder->data(1) == QVariant("Comment") || chPlaceholder->data(1) == QVariant("Component"))
		{
			//remove rotation of placeholder
			Placeholder->setRotation(0);

			//moving placeholder
			Placeholder->setPos(e->scenePos());
		}
		else if(chPlaceholder->data(1) == QVariant("InputPort") || chPlaceholder->data(1) == QVariant("OutputPort"))
		{
			//moving placeholder
			if(ItemForAddPorts != 0)
			{
				float X = e->scenePos().x();
				float Y = e->scenePos().y();
				QPointF newPos = e->scenePos();

				//remove all that not ports or components
				for(QList<QGraphicsItem *>::iterator k = intersection.begin(); k != intersection.end(); k++)
				{
					if((*k)->data(1) != QVariant("InputPort") && (*k)->data(1) != QVariant("OutputPort") && (*k)->data(1) != QVariant("Component"))
					{
						intersection.removeOne((*k));
					}
				}

				//remove current item and all it's childs from intersection
				QList<QGraphicsItem *> thisItemP = ItemForAddPorts->childItems();
				thisItemP.append(ItemForAddPorts);//add item itself to list

				for(QList<QGraphicsItem *>::iterator j = thisItemP.begin(); j!=thisItemP.end(); j++)
				{
					intersection.removeOne(*j);
				}

				//here accuratly - port have rect with some size - 12px - and MUST NOT bulge
				int rectsize = 12;

				if(X < ItemForAddPorts->x())//left
				{
					//verify positon - it must be into bounding rect of ItemForAddPorts
					if(newPos.y() >= ItemForAddPorts->y()+rectsize && newPos.y() <= ItemForAddPorts->y()+ItemForAddPorts->boundingRect().height())
					{
						Placeholder->setRotation(180);
						Placeholder->setPos(ItemForAddPorts->x()+rectsize/2,newPos.y());
					}
				}
				else if(X >= ItemForAddPorts->x() && X <= ItemForAddPorts->x()+ItemForAddPorts->boundingRect().width())//center
				{
					if(Y <= ItemForAddPorts->y() + ItemForAddPorts->boundingRect().height()/2)//up
					{
						//verify positon - it must be into bounding rect of ItemForAddPorts
						if(newPos.x() >= ItemForAddPorts->x() && newPos.x() <= ItemForAddPorts->x()+ItemForAddPorts->boundingRect().width()-rectsize)
						{
							Placeholder->setRotation(-90);
							Placeholder->setPos(newPos.x(),ItemForAddPorts->y()+rectsize/2);
						}
					}
					else//down
					{
						//verify positon - it must be into bounding rect of ItemForAddPorts
						if(newPos.x() >= ItemForAddPorts->x()+rectsize && newPos.x() <= ItemForAddPorts->x()+ItemForAddPorts->boundingRect().width())
						{
							Placeholder->setRotation(90);
							Placeholder->setPos(newPos.x(),ItemForAddPorts->y()+ItemForAddPorts->boundingRect().height()-rectsize/2);
						}
					}
				}
				else//right
				{
					//verify positon - it must be into bounding rect of ItemForAddPorts
					if(newPos.y() >= ItemForAddPorts->y() && newPos.y() <= ItemForAddPorts->y()+ItemForAddPorts->boundingRect().height()-rectsize)
					{
						Placeholder->setRotation(0);
						Placeholder->setPos(ItemForAddPorts->x()+ItemForAddPorts->boundingRect().width()-rectsize/2,newPos.y());
					}
				}
			}
		}

		//set color for placeholder
		if(!intersection.isEmpty())
		{
			Placeholder->setBrush(ColorError);
		}
		else
		{
			Placeholder->setBrush(ColorForSelect);
		}
	}
}

void DiagComponentEditor::FixMouseRelease(QGraphicsSceneMouseEvent *e)
{
	if(ItersectWhileMoving)
	{
		int size = SelectedItems.size();
		for(int i=0; i<size; i++)
		{
			SelectedItems[i]->setPos(SelectedItems[i]->scenePos() - (e->scenePos() - LastAllowedPosition));
		}
	}
}

void DiagComponentEditor::ShowContextMenu(QPoint pos)
{
	//create
	QMenu menu;
	//QPointF tmpPos = ui.WorkPlace->mapToScene(pos);
	QPoint posScene = pos;//= QPoint(tmpPos.x(),tmpPos.y());
	
	if(MainState == mSELECT)
	{
		QAction *action1 = new QAction(QString::fromLocal8Bit("��������"), ui.WorkPlace);
		menu.addAction(action1);

		QAction *action2 = new QAction(QString::fromLocal8Bit("����������"), ui.WorkPlace);
		menu.addAction(action2);

		QAction *action3 = new QAction(QString::fromLocal8Bit("��������"), ui.WorkPlace);
		menu.addAction(action3);

		menu.addSeparator();

		QAction *action4 = new QAction(QString::fromLocal8Bit("�������� �����"), ui.WorkPlace);
		menu.addAction(action4);

		QAction *action5 = new QAction(QString::fromLocal8Bit("������� ����������"), ui.WorkPlace);
		menu.addAction(action5);
	}

	//call
	QAction* selectedItem = menu.exec(posScene);
	if (selectedItem)
	{
		QMessageBox::warning(this, "", QString::fromLocal8Bit("������ %1").arg(selectedItem->text()));
	}
	else
	{
		QMessageBox::warning(this, "", QString::fromLocal8Bit("������ �� �������"));
	}
}