//work with files here

#include "diagcomponenteditor.h"

//slots
void DiagComponentEditor::SaveJPG()
{
	QString filename = QFileDialog::getSaveFileName(this,QString::fromLocal8Bit("������� � �����������"),QDir::currentPath(),QString("Images (*.jpg)"));
	
	QList<QGraphicsItem *> wasSelected;
	bool wasPlaceholder = Placeholder->isVisible();

	//hide all system components
	QList<QGraphicsItem *> items = ui.WorkPlace->items();

	for(QList<QGraphicsItem *>::iterator i = items.begin(); i != items.end(); i++)
	{
		if((*i)->data(1) == QVariant("SelectingRect"))
		{
			QGraphicsRectItem * rect = (QGraphicsRectItem *)*i;

			if(rect->brush() == QBrush(ColorForSelect))//was selected
			{
				wasSelected.append(*i);//save it state
				rect->setBrush(Qt::NoBrush);
			}
		}
	}

	Placeholder->setVisible(false);
	
	//save
	if(!filename.size() || !SaveAsImage(filename))
	{
		QMessageBox::warning(this,QString::fromLocal8Bit("������"),QString::fromLocal8Bit("�� ������� ��������� ��������� ��� �����������! ���������� ������� ������ ���� ��� �����������"));
	}

	//and show again
	for(QList<QGraphicsItem *>::iterator i = wasSelected.begin(); i != wasSelected.end(); i++)
	{
		QGraphicsRectItem * rect = (QGraphicsRectItem *)*i;
		rect->setBrush(QBrush(ColorForSelect));
	}

	Placeholder->setVisible(wasPlaceholder);
}



//functions
bool DiagComponentEditor::SaveAsImage(QString fileName)
{
	QPixmap pixMap = QPixmap::grabWidget(ui.WorkPlace);
	return pixMap.save(fileName);
}