//Constructor and slots here

#include "diagcomponenteditor.h"

DiagComponentEditor::DiagComponentEditor(QWidget *parent, Qt::WFlags flags)
	: QMainWindow(parent, flags)
{
	ui.setupUi(this);//set ui

	CreateNewDocument();

	//connects
	//left menu
	connect(ui.SelectModeButton,SIGNAL(clicked()),this,SLOT(SwitchSelectMode()));
	connect(ui.ComponentModeButton,SIGNAL(clicked()),this,SLOT(SwitchComponentAddMode()));
	connect(ui.CommentModeButton,SIGNAL(clicked()),this,SLOT(SwitchCommentAddMode()));
	connect(ui.ProvidedPortButton,SIGNAL(clicked()),this,SLOT(SwitchPrdPortAddMode()));
	connect(ui.RequiredPortButton,SIGNAL(clicked()),this,SLOT(SwitchReqPortAddMode()));
	connect(ui.LinkButton,SIGNAL(clicked()),this,SLOT(SwitchCreateLinkMode()));

	//workscene resignal to this
	connect(MainScene,SIGNAL(mousePressed(QGraphicsSceneMouseEvent *)),this,SLOT(FixMouseClick(QGraphicsSceneMouseEvent *)));
	connect(MainScene,SIGNAL(mouseMove(QGraphicsSceneMouseEvent *)),this,SLOT(FixMouseMove(QGraphicsSceneMouseEvent *)));
	connect(MainScene,SIGNAL(mouseReleased(QGraphicsSceneMouseEvent *)),this,SLOT(FixMouseRelease(QGraphicsSceneMouseEvent *)));

	//context menu
	connect(ui.WorkPlace,SIGNAL(customContextMenuRequested(QPoint)),this,SLOT(ShowContextMenu(QPoint)));

	//main menu
	connect(ui.actionNew_file,SIGNAL(triggered(bool)),this,SLOT(CreateNewDocument()));
	connect(ui.actionSave_as,SIGNAL(triggered(bool)),this,SLOT(SaveJPG()));

	//text editing buttons
	connect(ui.AgreeButton,SIGNAL(clicked()),this,SLOT(ConfirmEditing()));
	connect(ui.DisagreeButton,SIGNAL(clicked()),this,SLOT(CancelEditing()));

	connect(TextEditText,SIGNAL(textChanged()),this,SLOT(VerifyMaxLength()));
}

DiagComponentEditor::~DiagComponentEditor()
{

}

//slots

void DiagComponentEditor::SwitchSelectMode()
{
	MainState = mSELECT;

	//off
	TurnOffPlaceholder();
}

void DiagComponentEditor::SwitchComponentAddMode()
{
	MainState = mCOMPONENT;

	//on
	ComponentEl newItemRaw = ComponentEl();
	QGraphicsItem * tmp = newItemRaw.getThisElement();
	
	TurnOnPlaceholder(tmp, true);
}

void DiagComponentEditor::SwitchCommentAddMode()
{
	MainState = mCOMMENT;

	//on
	ElemComment newItemRaw = ElemComment();
	QGraphicsItem * tmp = newItemRaw.getThisElement();

	TurnOnPlaceholder(tmp, true);
}

void DiagComponentEditor::SwitchReqPortAddMode()
{
	MainState = mRecPORT;

	//on
	QGraphicsItem * tmp = new QGraphicsTextItem(QString::fromLocal8Bit("�������� �� ��������� ��� ���������� �����"),0,MainScene);
	tmp->setData(1,QVariant("Tooltip"));
	
	TurnOnPlaceholder(tmp,true);
}

void DiagComponentEditor::SwitchPrdPortAddMode()
{
	MainState = mPrdPORT;

	//on
	QGraphicsItem * tmp = new QGraphicsTextItem(QString::fromLocal8Bit("�������� �� ��������� ��� ���������� �����"),0,MainScene);
	tmp->setData(1,QVariant("Tooltip"));

	TurnOnPlaceholder(tmp,true);
}

void DiagComponentEditor::SwitchCreateLinkMode()
{
	MainState = mPortsLINK;

	//off
	TurnOffPlaceholder();
}

void DiagComponentEditor::FixMouseClick(QGraphicsSceneMouseEvent *e)
{
	QPointF pos = e->scenePos();

	QGraphicsItem * pressed = MainScene->itemAt(pos);
	QGraphicsItem * clicked = 0;

	if(pressed != 0)//find something
	{
		clicked = FindMainParent(pressed,"Item");//find whole item - if it's not it

		//remove all linked with placeholder
		QList<QGraphicsItem *> children = Placeholder->childItems();
		int size = children.size();

		for(int i=0; i<size; i++)
		{
			if(clicked == children[i])//it was placeholder, nothing to do here
			{
				clicked = 0;
				break;
			}
		}
	}

	//work with states
	if(e->button() == Qt::LeftButton)
	{
		if(MainState == mCOMPONENT && Placeholder->brush() == QBrush(ColorForSelect))
		{
			ComponentEl newItemRaw = ComponentEl();
			newItemRaw.ColorSelecting(QBrush(Qt::NoBrush), QPen(Qt::NoPen));
			QGraphicsItem * newItem = newItemRaw.getThisElement();
			newItem->setPos(pos);

			MainScene->addItem(newItem);
		}
		else if(MainState == mCOMMENT && Placeholder->brush() == QBrush(ColorForSelect))
		{
			ElemComment newItemRaw = ElemComment();
			newItemRaw.ColorSelecting(QBrush(Qt::NoBrush), QPen(Qt::NoPen));
			QGraphicsItem * newItem = newItemRaw.getThisElement();
			newItem->setPos(pos);

			MainScene->addItem(newItem);
		}
		else if(MainState == mSELECT)
		{
			//need to add this element to selected?
			if(clicked != 0 && clicked->data(3) == QVariant("NotSelected"))
			{
				SelectOneItem(clicked);
			}
			else if(SelectedItems.size())
			{
				LastAllowedPosition = e->scenePos();
			}
		}
		else if(MainState == mRecPORT || MainState == mPrdPORT)
		{
			//is clicked element can have ports?
			if(clicked != 0 && clicked->data(1) == QVariant("Component") && ItemForAddPorts != clicked)
			{
				//set placeholder
				PortClass newItemRaw;

				if(MainState == mRecPORT)
				{
					newItemRaw = PortClass(true);
				}
				else
				{
					newItemRaw = PortClass(false);
				}

				//turn it on
				QGraphicsItem * tmp = newItemRaw.getThisElement();
				TurnOnPlaceholder(tmp, true);

				//mark selectedclicked item
				ItemForAddPorts = clicked;
			}
			else if(ItemForAddPorts != 0 && Placeholder->brush() == QBrush(ColorForSelect))//this click seems port placed andnot intersect something forbitten
			{
				QGraphicsItem * chPlaceholder = Placeholder->childItems().first();//here must be only one element in array - tagged mainblock
				PortClass newPort;

				if(MainState == mRecPORT)
				{
					newPort = PortClass(true);
				}
				else//mPrdPORT
				{
					newPort = PortClass(false);
				}

				//create new port object
				newPort.ColorSelecting(QBrush(Qt::NoBrush), QPen(Qt::NoPen));
				QGraphicsItem * newItem = newPort.getThisElement();

				//form place gor additing port - because center of this object is not in (0,0)
				QPointF placeRaw = chPlaceholder->scenePos();
				QPointF placeGrilled;
				qreal sizeOffset = newItem->transformOriginPoint().y();
				
				if(Placeholder->rotation() == 90)
				{
					placeGrilled = QPointF(placeRaw.x()-sizeOffset, placeRaw.y()-sizeOffset);
				}
				else if(Placeholder->rotation() == 180)
				{
					placeGrilled = QPointF(placeRaw.x(), placeRaw.y()-sizeOffset*2);
				}
				else if(Placeholder->rotation() == -90)
				{
					placeGrilled = QPointF(placeRaw.x()+sizeOffset, placeRaw.y()-sizeOffset);
				}
				else
				{
					placeGrilled = placeRaw;
				}

				//add new port
				newItem->setRotation(Placeholder->rotation());
				newItem->setParentItem(ItemForAddPorts);
				
				placeGrilled = ItemForAddPorts->mapFromScene(placeGrilled);
				newItem->setPos(placeGrilled);
				
				MainScene->addItem(newItem);//add on scene
			}
		}
		

		//remove selection
		if(clicked == 0)
		{
			int size = SelectedItems.size();

			for(int i=0; i<size; i++)//free linked item from selection
			{
				SelectIt(SelectedItems[i],false);
				SelectedItems[i]->setZValue(1);
				SelectedItems[i]->setData(3,QVariant("NotSelected"));
			}
					
			//clear list
			SelectedItems.clear();
		}
	}
	
	ui.WorkPlace->update();
}

void DiagComponentEditor::FixMouseMove(QGraphicsSceneMouseEvent *e)
{
	//moving selected element
	if(MainState == mSELECT && e->buttons() & Qt::LeftButton)
	{
		QGraphicsItem * clicked = MainScene->itemAt(e->scenePos());

		//finding under cursor item with tag SelectingRect - for drag it can be only comment or component as parent
		if(clicked != 0 && clicked->parentItem() != 0 && clicked->data(1) == QVariant("SelectingRect") && (clicked->parentItem()->data(1) == QVariant("Component") || clicked->parentItem()->data(1) == QVariant("Comment")))
		{
			//move all selected items
			int size = SelectedItems.size();
			for(int i=0; i<size; i++)
			{
				SelectedItems[i]->setPos(SelectedItems[i]->scenePos() + (e->scenePos() - e->lastScenePos()));
			}
		}
	}
	

	//placeholder
	//if we have something to move...
	if(Placeholder->isVisible())
	{
		QList<QGraphicsItem *> intersection;
		QGraphicsItem * chPlaceholder = Placeholder->childItems().first();//here must be only one element in array - tagged mainblock

		if(chPlaceholder->data(1) == QVariant("Tooltip"))
		{
			//remove rotation of placeholder
			Placeholder->setRotation(0);

			//moving placeholder
			Placeholder->setPos(e->scenePos() + QPointF(-10,-25));
			Placeholder->setBrush(ColorMain);
		}
		else
		{
			//maybe intersect with something?
			intersection = ItersectSomething(Placeholder);

			//move placeholder - depends on type of additing element
			//choose type item under placeholder
			if(chPlaceholder->data(1) == QVariant("Comment") || chPlaceholder->data(1) == QVariant("Component"))
			{
				//remove rotation of placeholder
				Placeholder->setRotation(0);

				//moving placeholder
				Placeholder->setPos(e->scenePos());
			}
			else if(chPlaceholder->data(1) == QVariant("InputPort") || chPlaceholder->data(1) == QVariant("OutputPort"))
			{
				//moving placeholder
				if(ItemForAddPorts != 0)
				{
					float X = e->scenePos().x();
					float Y = e->scenePos().y();
					QPointF newPos = e->scenePos();

					//remove all that not ports or components
					/*for(QList<QGraphicsItem *>::iterator k = intersection.begin(); k != intersection.end(); k++)
					{
						QGraphicsItem * g = *k;
						QVariant f= (*k)->data(1);
						if((*k)->data(1) != QVariant("InputPort") && (*k)->data(1) != QVariant("OutputPort") && (*k)->data(1) != QVariant("Component"))
						{
							intersection.removeOne((*k));
						}
					}*/
					intersection.clear();//its tmp!

					//remove current item and all it's childs from intersection
					QList<QGraphicsItem *> thisItemP = ItemForAddPorts->childItems();
					intersection.removeOne(ItemForAddPorts);//remove item itself

					for(QList<QGraphicsItem *>::iterator j = thisItemP.begin(); j!=thisItemP.end(); j++)
					{
						if((*j)->data(1) != QVariant("InputPort") && (*j)->data(1) != QVariant("OutputPort") && (*j)->data(1) != QVariant("Component"))//child is not port
						{
							intersection.removeOne(*j);
						}
					}

					//here accuratly - port have rect with some size - 12px - and MUST NOT bulge
					int rectsize = 12;

					if(X < ItemForAddPorts->x())//left
					{
						//verify positon - it must be into bounding rect of ItemForAddPorts
						if(newPos.y() >= ItemForAddPorts->y()+rectsize && newPos.y() <= ItemForAddPorts->y()+ItemForAddPorts->boundingRect().height())
						{
							Placeholder->setRotation(180);
							Placeholder->setPos(ItemForAddPorts->x()+rectsize/2,newPos.y());
						}
					}
					else if(X >= ItemForAddPorts->x() && X <= ItemForAddPorts->x()+ItemForAddPorts->boundingRect().width())//center
					{
						if(Y <= ItemForAddPorts->y() + ItemForAddPorts->boundingRect().height()/2)//up
						{
							//verify positon - it must be into bounding rect of ItemForAddPorts
							if(newPos.x() >= ItemForAddPorts->x() && newPos.x() <= ItemForAddPorts->x()+ItemForAddPorts->boundingRect().width()-rectsize)
							{
								Placeholder->setRotation(-90);
								Placeholder->setPos(newPos.x(),ItemForAddPorts->y()+rectsize/2);
							}
						}
						else//down
						{
							//verify positon - it must be into bounding rect of ItemForAddPorts
							if(newPos.x() >= ItemForAddPorts->x()+rectsize && newPos.x() <= ItemForAddPorts->x()+ItemForAddPorts->boundingRect().width())
							{
								Placeholder->setRotation(90);
								Placeholder->setPos(newPos.x(),ItemForAddPorts->y()+ItemForAddPorts->boundingRect().height()-rectsize/2);
							}
						}
					}
					else//right
					{
						//verify positon - it must be into bounding rect of ItemForAddPorts
						if(newPos.y() >= ItemForAddPorts->y() && newPos.y() <= ItemForAddPorts->y()+ItemForAddPorts->boundingRect().height()-rectsize)
						{
							Placeholder->setRotation(0);
							Placeholder->setPos(ItemForAddPorts->x()+ItemForAddPorts->boundingRect().width()-rectsize/2,newPos.y());
						}
					}
				}
			}
		
			//set color for placeholder
			if(!intersection.isEmpty())
			{
				Placeholder->setBrush(ColorError);
			}
			else
			{
				Placeholder->setBrush(ColorForSelect);
			}
		}
	}
}

void DiagComponentEditor::FixMouseRelease(QGraphicsSceneMouseEvent *e)
{
	int size = SelectedItems.size();
	bool intersected = false;

	if(MainState == mSELECT && e->button() == Qt::LeftButton)
	{
		QList<QGraphicsItem *> children;
		int sizej;

		for(int i=0; i<size; i++)
		{
			if(SelectedItems[i]->data(1) == QVariant("Component"))//selected component
			{
				children = SelectedItems[i]->childItems();
				children.append(SelectedItems[i]);//items itself MUST also be verified
				sizej = children.size();

				for(int j=0; j<sizej; j++)
				{
					//grab selctingrect of item and verify it on collision. if it have no collicsion - all elemen have not collision
					if(children[j]->data(1) == QVariant("SelectingRect"))
					{
						//find itersection
						QList<QGraphicsItem *> IntersectionCurrent = ItersectSomething(children[j]);
						IntersectionCurrent.removeOne(SelectedItems[i]);//remove selected item itself - it cant collide with itself

						//remove all that cant intersect with selected item
						for(QList<QGraphicsItem *>::iterator k = IntersectionCurrent.begin(); k != IntersectionCurrent.end(); k++)
						{
							if(((*k)->data(1) != QVariant("Component") && (*k)->data(1) != QVariant("InputPort") && (*k)->data(1) != QVariant("OutputPort")))
							{
								QVariant f = (*k)->data(1);
								IntersectionCurrent.removeOne((*k));
							}
						}

						if(IntersectionCurrent.size())
						{
							for(int i=0; i<size; i++)
							{
								SelectedItems[i]->setPos(SelectedItems[i]->scenePos() - (e->scenePos() - LastAllowedPosition));
							}

							intersected = true;
							break;//time economy
						}
					}
				}
				
				if(intersected)
				{
					break;//time economy
				}
			}
		}
	}
}







//ContextMenu starts here
void DiagComponentEditor::ShowContextMenu(QPoint pos)
{
	//create
	QMenu menu;
	
	//clicked on selected item
	if(MainState == mSELECT && SelectedItems.size())
	{
		//QAction *action1 = new QAction(QString::fromLocal8Bit("��������"), ui.WorkPlace);
		//menu.addAction(action1);

		//QAction *action2 = new QAction(QString::fromLocal8Bit("����������"), ui.WorkPlace);
		//menu.addAction(action2);

		//QAction *action3 = new QAction(QString::fromLocal8Bit("��������"), ui.WorkPlace);
		//menu.addAction(action3);

		//menu.addSeparator();

		if(SelectedItems.size() == 1 && (SelectedItems.first()->data(1) == QVariant("Component") || SelectedItems.first()->data(1) == QVariant("Comment")) && TextThatEditing == 0)
		{
			QAction *action4 = new QAction(QString::fromLocal8Bit("�������� �����"), ui.WorkPlace);
			menu.addAction(action4);
		}

		QAction *action5 = new QAction(QString::fromLocal8Bit("������� ����������"), ui.WorkPlace);
		menu.addAction(action5);

		//call
		QAction* selectedItem = menu.exec(ui.WorkPlace->mapToGlobal(pos));
		if (selectedItem)
		{
			if(selectedItem->text() == QString::fromLocal8Bit("������� ����������"))
			{
				DeleteSelectedItems();
			}
			else if(selectedItem->text() == QString::fromLocal8Bit("�������� �����"))
			{
				ChangeText(SelectedItems.first());
			}
		}
	}
}

void DiagComponentEditor::ConfirmEditing()
{
	//confirm
	if(LineEditText->isVisible())//for component
	{
		TextThatEditing->setPlainText(LineEditText->text());
	}
	else if(TextEditText->isVisible())//for comment
	{
		TextThatEditing->setPlainText(TextEditText->toPlainText());
	}

	//hide
	ui.AgreeButton->setVisible(false);
	ui.DisagreeButton->setVisible(false);
	TextThatEditing->setVisible(true);
	TextEditText->setVisible(false);
	LineEditText->setVisible(false);

	TextThatEditing = 0;
}

void DiagComponentEditor::CancelEditing()
{
	//hide
	ui.AgreeButton->setVisible(false);
	ui.DisagreeButton->setVisible(false);
	TextThatEditing->setVisible(true);
	TextEditText->setVisible(false);
	LineEditText->setVisible(false);

	TextThatEditing = 0;
}

void DiagComponentEditor::VerifyMaxLength()
{
	//int countLines = TextEditText->document()->blockCount();
	int maxNormalCount = 3;
	int maxLineSize = 11;

	if(TextEditText->isVisible())
	{
		if(TextEditText->toPlainText().size() > maxNormalCount*maxLineSize)
		{
			TextEditText->textCursor().deletePreviousChar();
		}
	}
}





//up menu
void DiagComponentEditor::CreateNewDocument()
{
	MainScene = new WorkScene();//construct main scene

	//clear
	SelectedItems.clear();

	ItemForAddPorts = 0;

	//Colors
	ColorForSelect = QColor(130,200,240);
	ColorError = QColor(210,87,89);
	ColorMain = QColor(255,255,255);

	ui.WorkPlace->setContextMenuPolicy(Qt::CustomContextMenu);//off default context menu

	//set placeholder settings
	Placeholder = new QGraphicsRectItem(0,0,0,0,0,MainScene);
	Placeholder->setBrush(QBrush(ColorForSelect));
	Placeholder->setPen(Qt::NoPen);
	Placeholder->setOpacity(0.33);
	Placeholder->setVisible(false);
	Placeholder->setZValue(1000);

	SwitchSelectMode();//set mode to SELECT

	//for editiong text component
	LineEditText = new QLineEdit(ui.centralWidget);
	LineEditText->setVisible(false);

	TextEditText = new QTextEdit(ui.centralWidget);
	TextEditText->setVisible(false);
	TextEditText->setWordWrapMode(QTextOption::WordWrap);

	ui.AgreeButton->setVisible(false);
	ui.DisagreeButton->setVisible(false);

	TextThatEditing = NULL;

	ui.WorkPlace->setScene(MainScene);
	ui.WorkPlace->setSceneRect(0,0,ui.WorkPlace->width(),ui.WorkPlace->height());
}