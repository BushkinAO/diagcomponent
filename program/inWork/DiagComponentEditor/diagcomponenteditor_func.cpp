//methods(not slots) here

#include "diagcomponenteditor.h"

//status Item - to find full component by item
//status Diagram - find main parent of this item in all diagram
//status All - find maximal parent, without conditions
QGraphicsItem * DiagComponentEditor::FindMainParent(QGraphicsItem *item, QString status)
{
	QGraphicsItem * currentParent = item->parentItem();

	if(status == QString("Item"))
	{
		if(item->data(2) == QVariant("Main"))
		{
			return item;
		}
		else if(currentParent != 0)
		{
			return FindMainParent(currentParent,status);
		}
		else
		{
			return 0;
		}
	}
	else if(status == QString("Diagram"))
	{
		if(item->data(2) == QVariant("Main") && currentParent->data(2) != QVariant("Main"))
		{
			return item;
		}
		else if(currentParent != 0)
		{
			return FindMainParent(currentParent,status);
		}
		else
		{
			return 0;
		}
	}
	else if(status == QString("All"))
	{
		if(currentParent == 0)
		{
			return item;
		}
		else
		{
			return FindMainParent(currentParent,status);
		}
	}
	else
	{
		return 0;
	}
}

void DiagComponentEditor::TurnOnPlaceholder(QGraphicsItem * item, bool onoff)
{
	//clear placeholder
	TurnOffPlaceholder();

	//show placeholder
	Placeholder->setVisible(onoff);

	if(item != 0)
	{
		item->setParentItem(Placeholder);

		//form placeholder size
		QGraphicsItem * selecting = GetSelectionItem(item);
		if(selecting != 0)
		{
			Placeholder->setRect(selecting->boundingRect());
		}
		else
		{
			Placeholder->setRect(item->boundingRect());
		}
	}

	ui.WorkPlace->setMouseTracking(true);
}

void DiagComponentEditor::TurnOffPlaceholder()
{
	//hide placeholder
	Placeholder->setVisible(false);
	ui.WorkPlace->setMouseTracking(false);

	//clear after ports
	ItemForAddPorts = NULL;//clear item to work with - it will be again finded when click will been
	Placeholder->setRotation(0);

	//clear placeholder
	QList<QGraphicsItem *> tmp = Placeholder->childItems();
	if(!tmp.isEmpty())
	{
		MainScene->removeItem(tmp.first());
	}
}

// return objects, itersects with items, if it's components or comments without parents
QList<QGraphicsItem *> DiagComponentEditor::ItersectSomething(QGraphicsItem *item)
{
	QList<QGraphicsItem *> PlshlCollidedWith  = item->collidingItems();
	QList<QGraphicsItem *> children = item->childItems();
	QList<QGraphicsItem *>::iterator i;

	for(i=PlshlCollidedWith.begin(); i<PlshlCollidedWith.end(); i++)
	{
		QGraphicsItem * tmp = FindMainParent((*i),"Diagram");

		if(tmp != 0 && PlshlCollidedWith.indexOf(tmp) != -1 && (*i) != tmp && (tmp->data(1) == QVariant("Component") || tmp->data(1) == QVariant("Comment")))//founded parent already exist in array AND not itself (*i)
		{
			PlshlCollidedWith.removeOne((*i));
		}
	}

	//clear placeholder parts
	for(i=children.begin(); i<children.end(); i++)
	{
		PlshlCollidedWith.removeOne(*i);
	}

	PlshlCollidedWith.removeOne(item);//remove item itself

	return PlshlCollidedWith;//after filter
}

void DiagComponentEditor::SelectIt(QGraphicsItem * item, bool onoff)
{
	QGraphicsRectItem * selecting = (QGraphicsRectItem *)GetSelectionItem(item);
	
	if(selecting != 0)//item have selecting child
	{
		if(onoff)
		{
			selecting->setBrush(QBrush(ColorForSelect));
		}
		else
		{
			selecting->setBrush(Qt::NoBrush);
		}

		selecting->setPen(Qt::NoPen);
		selecting->setOpacity(0.35);
		item->setData(3, QVariant((onoff)?"Selected":"NotSelected"));
	}
}

QGraphicsItem * DiagComponentEditor::GetSelectionItem(QGraphicsItem *item)
{
	if(item != 0)
	{
		QList<QGraphicsItem *> children = item->childItems();
		children.append(item);//item itself can be that we want

		for(int i=0; i<children.size(); i++)
		{
			if(children[i]->data(1) == QVariant("SelectingRect"))
			{
				return children[i];//selection item found and returned
			}
		}
	}

	return 0;//nothing found - strange, but everything can be
}

int DiagComponentEditor::countTaggedElements(QList<QGraphicsItem*> list, QVariant tag)
{
	int count = 0;

	for(QList<QGraphicsItem *>::iterator i = list.begin(); i != list.end(); i++)
	{
		if((*i)->data(1) == tag)
		{
			count++;
		}
	}

	return count;
}

void DiagComponentEditor::SelectOneItem(QGraphicsItem *clicked)
{
	if(SelectedItems.isEmpty() || !SelectedItems.isEmpty() && clicked->parentItem() == SelectedItems.first()->parentItem())//something already selected, and new have the same parent?
	{
		//mark selected
		SelectIt(clicked,true);
		clicked->setZValue(20);
		SelectedItems.append(clicked);
	}
}




//context menu
void DiagComponentEditor::DeleteSelectedItems()
{
	QList<QGraphicsItem *>::iterator i;

	int state = QMessageBox::information(this,QString::fromLocal8Bit("����� �������?"),QString::fromLocal8Bit("�� ������ ����� ������ ������� ���������� %1 �� ���������?").arg(QString::number(SelectedItems.size())),QString::fromLocal8Bit("��"),QString::fromLocal8Bit("���"));

	if(state == 0)
	{
		for(i = SelectedItems.begin(); i != SelectedItems.end(); i++)
		{
			MainScene->removeItem(*i);
		}

		SelectedItems.clear();
	}
}

void DiagComponentEditor::ChangeText(QGraphicsItem * item)
{
	if(TextThatEditing)
	{
		TextThatEditing->setVisible(true);//hide
	}

	//find text
	QList<QGraphicsItem *> children = item->childItems();
	QGraphicsTextItem * textItem = 0;

	QPointF pos;

	for(QList<QGraphicsItem *>::iterator i=children.begin(); i!=children.end(); i++)
	{
		if((*i)->data(1) == QVariant("TextContent"))
		{
			textItem = (QGraphicsTextItem *)(*i);
			break;
		}
	}

	//create edit form
	//start editing
	ui.AgreeButton->setVisible(true);
	ui.DisagreeButton->setVisible(true);
	textItem->setVisible(false);//hide

	TextThatEditing = textItem;//save for confirm/cancel

	if(textItem)
	{
		if(item->data(1) == QVariant("Component"))
		{
			pos = textItem->pos();
			pos = item->mapToScene(pos.toPoint());
			pos = ui.WorkPlace->mapFromScene(pos.toPoint()) + ui.WorkPlace->pos()+QPointF(0,5);
			
			LineEditText->setVisible(true);
			LineEditText->setGeometry(pos.x(),pos.y(),item->boundingRect().width()-20,18);
			LineEditText->setMaxLength(12*item->boundingRect().width()/150);
			LineEditText->setText(textItem->toPlainText());
		}
		else if(item->data(1) == QVariant("Comment"))//comment
		{
			pos = textItem->pos();
			pos = item->mapToScene(pos.toPoint());
			pos = ui.WorkPlace->mapFromScene(pos.toPoint()) + ui.WorkPlace->pos()+QPointF(0,5);
			
			TextEditText->setVisible(true);
			TextEditText->setGeometry(pos.x(),pos.y(),item->boundingRect().width()-20,item->boundingRect().height()-20);
			//TextEditText->setTextWidth(12*item->boundingRect().width()/150);
			TextEditText->setText(textItem->toPlainText());
		}
	}
}