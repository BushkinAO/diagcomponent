/********************************************************************************
** Form generated from reading UI file 'diagcomponenteditor.ui'
**
** Created: Mon 4. Nov 23:32:49 2013
**      by: Qt User Interface Compiler version 4.7.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIAGCOMPONENTEDITOR_H
#define UI_DIAGCOMPONENTEDITOR_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGraphicsView>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QSlider>
#include <QtGui/QSpacerItem>
#include <QtGui/QSpinBox>
#include <QtGui/QStatusBar>
#include <QtGui/QToolBar>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionNew_file;
    QAction *actionOpen_file;
    QAction *actionSave;
    QAction *actionSave_as;
    QAction *actionExit;
    QAction *actionTracing;
    QAction *actionKegl;
    QAction *actionCopy;
    QAction *actionPaste;
    QAction *actionDelete;
    QAction *actionColor_of_line;
    QAction *actionColor_of_area;
    QAction *actionHelp_about_program;
    QAction *actionHelp_about_functions;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout;
    QPushButton *SelectModeButton;
    QPushButton *SelectRectButton;
    QPushButton *ComponentModeButton;
    QPushButton *CommentModeButton;
    QPushButton *ProvidedPortButton;
    QPushButton *RequiredPortButton;
    QPushButton *LinkButton;
    QSpacerItem *verticalSpacer_2;
    QPushButton *AgreeButton;
    QPushButton *DisagreeButton;
    QSpacerItem *verticalSpacer;
    QGraphicsView *WorkPlace;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer;
    QLabel *label_2;
    QSlider *horizontalSlider;
    QSpinBox *ScaleSpin;
    QMenuBar *menuBar;
    QMenu *menuUMLComponentMaker;
    QMenu *menuService;
    QStatusBar *statusBar;
    QToolBar *mainToolBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(850, 571);
        actionNew_file = new QAction(MainWindow);
        actionNew_file->setObjectName(QString::fromUtf8("actionNew_file"));
        actionOpen_file = new QAction(MainWindow);
        actionOpen_file->setObjectName(QString::fromUtf8("actionOpen_file"));
        actionSave = new QAction(MainWindow);
        actionSave->setObjectName(QString::fromUtf8("actionSave"));
        actionSave_as = new QAction(MainWindow);
        actionSave_as->setObjectName(QString::fromUtf8("actionSave_as"));
        actionExit = new QAction(MainWindow);
        actionExit->setObjectName(QString::fromUtf8("actionExit"));
        actionTracing = new QAction(MainWindow);
        actionTracing->setObjectName(QString::fromUtf8("actionTracing"));
        actionKegl = new QAction(MainWindow);
        actionKegl->setObjectName(QString::fromUtf8("actionKegl"));
        actionCopy = new QAction(MainWindow);
        actionCopy->setObjectName(QString::fromUtf8("actionCopy"));
        actionPaste = new QAction(MainWindow);
        actionPaste->setObjectName(QString::fromUtf8("actionPaste"));
        actionDelete = new QAction(MainWindow);
        actionDelete->setObjectName(QString::fromUtf8("actionDelete"));
        actionColor_of_line = new QAction(MainWindow);
        actionColor_of_line->setObjectName(QString::fromUtf8("actionColor_of_line"));
        actionColor_of_area = new QAction(MainWindow);
        actionColor_of_area->setObjectName(QString::fromUtf8("actionColor_of_area"));
        actionHelp_about_program = new QAction(MainWindow);
        actionHelp_about_program->setObjectName(QString::fromUtf8("actionHelp_about_program"));
        actionHelp_about_functions = new QAction(MainWindow);
        actionHelp_about_functions->setObjectName(QString::fromUtf8("actionHelp_about_functions"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        verticalLayout_2 = new QVBoxLayout(centralWidget);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        SelectModeButton = new QPushButton(centralWidget);
        SelectModeButton->setObjectName(QString::fromUtf8("SelectModeButton"));
        QIcon icon;
        icon.addFile(QString::fromUtf8("Resources/select_mode.png"), QSize(), QIcon::Normal, QIcon::Off);
        SelectModeButton->setIcon(icon);
        SelectModeButton->setIconSize(QSize(24, 24));

        verticalLayout->addWidget(SelectModeButton);

        SelectRectButton = new QPushButton(centralWidget);
        SelectRectButton->setObjectName(QString::fromUtf8("SelectRectButton"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8("Resources/select_tool.png"), QSize(), QIcon::Normal, QIcon::Off);
        SelectRectButton->setIcon(icon1);
        SelectRectButton->setIconSize(QSize(24, 24));

        verticalLayout->addWidget(SelectRectButton);

        ComponentModeButton = new QPushButton(centralWidget);
        ComponentModeButton->setObjectName(QString::fromUtf8("ComponentModeButton"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8("Resources/component_add_mode.png"), QSize(), QIcon::Normal, QIcon::Off);
        ComponentModeButton->setIcon(icon2);
        ComponentModeButton->setIconSize(QSize(24, 24));

        verticalLayout->addWidget(ComponentModeButton);

        CommentModeButton = new QPushButton(centralWidget);
        CommentModeButton->setObjectName(QString::fromUtf8("CommentModeButton"));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8("Resources/comment_add_mode.png"), QSize(), QIcon::Normal, QIcon::Off);
        CommentModeButton->setIcon(icon3);
        CommentModeButton->setIconSize(QSize(24, 24));

        verticalLayout->addWidget(CommentModeButton);

        ProvidedPortButton = new QPushButton(centralWidget);
        ProvidedPortButton->setObjectName(QString::fromUtf8("ProvidedPortButton"));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8("Resources/provided_port.png"), QSize(), QIcon::Normal, QIcon::Off);
        ProvidedPortButton->setIcon(icon4);
        ProvidedPortButton->setIconSize(QSize(24, 24));

        verticalLayout->addWidget(ProvidedPortButton);

        RequiredPortButton = new QPushButton(centralWidget);
        RequiredPortButton->setObjectName(QString::fromUtf8("RequiredPortButton"));
        QIcon icon5;
        icon5.addFile(QString::fromUtf8("Resources/required_port.png"), QSize(), QIcon::Normal, QIcon::Off);
        RequiredPortButton->setIcon(icon5);
        RequiredPortButton->setIconSize(QSize(24, 24));

        verticalLayout->addWidget(RequiredPortButton);

        LinkButton = new QPushButton(centralWidget);
        LinkButton->setObjectName(QString::fromUtf8("LinkButton"));
        QIcon icon6;
        icon6.addFile(QString::fromUtf8("Resources/link_mode.png"), QSize(), QIcon::Normal, QIcon::Off);
        LinkButton->setIcon(icon6);
        LinkButton->setIconSize(QSize(24, 24));

        verticalLayout->addWidget(LinkButton);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout->addItem(verticalSpacer_2);

        AgreeButton = new QPushButton(centralWidget);
        AgreeButton->setObjectName(QString::fromUtf8("AgreeButton"));
        QIcon icon7;
        icon7.addFile(QString::fromUtf8("Resources/yes.png"), QSize(), QIcon::Normal, QIcon::Off);
        AgreeButton->setIcon(icon7);
        AgreeButton->setIconSize(QSize(24, 24));

        verticalLayout->addWidget(AgreeButton);

        DisagreeButton = new QPushButton(centralWidget);
        DisagreeButton->setObjectName(QString::fromUtf8("DisagreeButton"));
        QIcon icon8;
        icon8.addFile(QString::fromUtf8("Resources/no.png"), QSize(), QIcon::Normal, QIcon::Off);
        DisagreeButton->setIcon(icon8);
        DisagreeButton->setIconSize(QSize(24, 24));

        verticalLayout->addWidget(DisagreeButton);

        verticalSpacer = new QSpacerItem(20, 213, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        horizontalLayout_2->addLayout(verticalLayout);

        WorkPlace = new QGraphicsView(centralWidget);
        WorkPlace->setObjectName(QString::fromUtf8("WorkPlace"));

        horizontalLayout_2->addWidget(WorkPlace);


        verticalLayout_2->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer);

        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_3->addWidget(label_2);

        horizontalSlider = new QSlider(centralWidget);
        horizontalSlider->setObjectName(QString::fromUtf8("horizontalSlider"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(horizontalSlider->sizePolicy().hasHeightForWidth());
        horizontalSlider->setSizePolicy(sizePolicy);
        horizontalSlider->setMinimum(10);
        horizontalSlider->setMaximum(300);
        horizontalSlider->setSingleStep(25);
        horizontalSlider->setPageStep(50);
        horizontalSlider->setSliderPosition(100);
        horizontalSlider->setOrientation(Qt::Horizontal);

        horizontalLayout_3->addWidget(horizontalSlider);

        ScaleSpin = new QSpinBox(centralWidget);
        ScaleSpin->setObjectName(QString::fromUtf8("ScaleSpin"));
        ScaleSpin->setMinimum(10);
        ScaleSpin->setMaximum(300);
        ScaleSpin->setSingleStep(25);
        ScaleSpin->setValue(100);

        horizontalLayout_3->addWidget(ScaleSpin);


        verticalLayout_2->addLayout(horizontalLayout_3);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 850, 21));
        menuUMLComponentMaker = new QMenu(menuBar);
        menuUMLComponentMaker->setObjectName(QString::fromUtf8("menuUMLComponentMaker"));
        menuService = new QMenu(menuBar);
        menuService->setObjectName(QString::fromUtf8("menuService"));
        MainWindow->setMenuBar(menuBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);

        menuBar->addAction(menuUMLComponentMaker->menuAction());
        menuBar->addAction(menuService->menuAction());
        menuUMLComponentMaker->addAction(actionNew_file);
        menuUMLComponentMaker->addAction(actionOpen_file);
        menuUMLComponentMaker->addAction(actionSave);
        menuUMLComponentMaker->addAction(actionSave_as);
        menuService->addAction(actionHelp_about_program);
        menuService->addAction(actionHelp_about_functions);

        retranslateUi(MainWindow);
        QObject::connect(horizontalSlider, SIGNAL(valueChanged(int)), ScaleSpin, SLOT(setValue(int)));
        QObject::connect(ScaleSpin, SIGNAL(valueChanged(int)), horizontalSlider, SLOT(setValue(int)));

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "UMLComponentMaker", 0, QApplication::UnicodeUTF8));
        actionNew_file->setText(QApplication::translate("MainWindow", "\320\235\320\276\320\262\321\213\320\271", 0, QApplication::UnicodeUTF8));
        actionOpen_file->setText(QApplication::translate("MainWindow", "\320\236\321\202\320\272\321\200\321\213\321\202\321\214...", 0, QApplication::UnicodeUTF8));
        actionSave->setText(QApplication::translate("MainWindow", "\320\241\320\276\321\205\321\200\320\260\320\275\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
        actionSave_as->setText(QApplication::translate("MainWindow", "\320\255\320\272\320\277\320\276\321\200\321\202 \320\262 jpg", 0, QApplication::UnicodeUTF8));
        actionExit->setText(QApplication::translate("MainWindow", "\320\222\321\213\321\205\320\276\320\264", 0, QApplication::UnicodeUTF8));
        actionTracing->setText(QApplication::translate("MainWindow", "\320\235\320\260\321\207\320\265\321\200\321\202\320\260\320\275\320\270\320\265", 0, QApplication::UnicodeUTF8));
        actionKegl->setText(QApplication::translate("MainWindow", "\320\232\320\265\320\263\320\273\321\214", 0, QApplication::UnicodeUTF8));
        actionCopy->setText(QApplication::translate("MainWindow", "\320\232\320\276\320\277\320\270\321\200\320\276\320\262\320\260\321\202\321\214", 0, QApplication::UnicodeUTF8));
        actionPaste->setText(QApplication::translate("MainWindow", "\320\222\321\201\321\202\320\260\320\262\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
        actionDelete->setText(QApplication::translate("MainWindow", "\320\243\320\264\320\260\320\273\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
        actionColor_of_line->setText(QApplication::translate("MainWindow", "Color of line", 0, QApplication::UnicodeUTF8));
        actionColor_of_area->setText(QApplication::translate("MainWindow", "Color of area", 0, QApplication::UnicodeUTF8));
        actionHelp_about_program->setText(QApplication::translate("MainWindow", "\320\241\320\277\321\200\320\260\320\262\320\272\320\260 \320\276 \320\277\321\200\320\276\320\263\321\200\320\260\320\274\320\274\320\265", 0, QApplication::UnicodeUTF8));
        actionHelp_about_functions->setText(QApplication::translate("MainWindow", "\320\241\320\277\321\200\320\260\320\262\320\272\320\260 \320\276 \321\204\321\203\320\275\320\272\321\206\320\270\321\217\321\205 \320\277\321\200\320\276\320\263\321\200\320\260\320\274\320\274\321\213", 0, QApplication::UnicodeUTF8));
        SelectModeButton->setText(QString());
        SelectRectButton->setText(QString());
        ComponentModeButton->setText(QString());
        CommentModeButton->setText(QString());
        ProvidedPortButton->setText(QString());
        RequiredPortButton->setText(QString());
        LinkButton->setText(QString());
        AgreeButton->setText(QString());
        DisagreeButton->setText(QString());
        label_2->setText(QApplication::translate("MainWindow", "\320\234\320\260\321\201\321\210\321\202\320\260\320\261", 0, QApplication::UnicodeUTF8));
        ScaleSpin->setSuffix(QApplication::translate("MainWindow", "%", 0, QApplication::UnicodeUTF8));
        menuUMLComponentMaker->setTitle(QApplication::translate("MainWindow", "\320\244\320\260\320\271\320\273", 0, QApplication::UnicodeUTF8));
        menuService->setTitle(QApplication::translate("MainWindow", "\320\241\320\277\321\200\320\260\320\262\320\272\320\260", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIAGCOMPONENTEDITOR_H
