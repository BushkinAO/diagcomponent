//work with files here

#include "diagcomponenteditor.h"

//slots
void DiagComponentEditor::SaveJPG()
{
	QString filename = QFileDialog::getSaveFileName(this,QString::fromLocal8Bit("������� � �����������"),QDir::currentPath(),QString("Images (*.jpg)"));
	
	QList<QGraphicsItem *> wasSelected;
	bool wasPlaceholder = Placeholder->isVisible();

	//hide all system components
	QList<QGraphicsItem *> items = ui.WorkPlace->items();

	for(QList<QGraphicsItem *>::iterator i = items.begin(); i != items.end(); i++)
	{
		if((*i)->data(1) == QVariant("SelectingRect"))
		{
			QGraphicsRectItem * rect = (QGraphicsRectItem *)*i;

			if(rect->brush() == QBrush(ColorForSelect))//was selected
			{
				wasSelected.append(*i);//save it state
				rect->setBrush(Qt::NoBrush);
			}
		}
	}

	Placeholder->setVisible(false);
	
	//save
	filename = filename.replace(QString("/"),QString("\\"));
	if(!filename.size() || !SaveAsImage(filename))
	{
		QMessageBox::warning(this,QString::fromLocal8Bit("������"),QString::fromLocal8Bit("�� ������� ��������� ��������� ��� �����������! ���������� ������� ������ ���� ��� ����������� %1").arg(filename));
	}

	//and show again
	for(QList<QGraphicsItem *>::iterator i = wasSelected.begin(); i != wasSelected.end(); i++)
	{
		QGraphicsRectItem * rect = (QGraphicsRectItem *)*i;
		rect->setBrush(QBrush(ColorForSelect));
	}

	Placeholder->setVisible(wasPlaceholder);
}

void DiagComponentEditor::PrintResult()
{
	//hide system data
	QList<QGraphicsItem *> wasSelected;
	bool wasPlaceholder = Placeholder->isVisible();

	//hide all system components
	QList<QGraphicsItem *> items = ui.WorkPlace->items();

	for(QList<QGraphicsItem *>::iterator i = items.begin(); i != items.end(); i++)
	{
		if((*i)->data(1) == QVariant("SelectingRect"))
		{
			QGraphicsRectItem * rect = (QGraphicsRectItem *)*i;

			if(rect->brush() == QBrush(ColorForSelect))//was selected
			{
				wasSelected.append(*i);//save it state
				rect->setBrush(Qt::NoBrush);
			}
		}
	}

	Placeholder->setVisible(false);


	//print
	QPrinter printer(QPrinter::HighResolution);
	QPixmap pixMap = QPixmap::grabWidget(ui.WorkPlace);
	
	QPrintDialog dialog(&printer, ui.WorkPlace);
	dialog.setOption(QAbstractPrintDialog::PrintToFile,false);
	dialog.setOption(QAbstractPrintDialog::PrintSelection,false);
	dialog.setOption(QAbstractPrintDialog::PrintPageRange,false);
	dialog.setOption(QAbstractPrintDialog::PrintShowPageSize);
	dialog.setOption(QAbstractPrintDialog::PrintCurrentPage,false);

	//form image
	QImage imageBackground(MainScene->width(), MainScene->height(), QImage::Format_RGB32);
	imageBackground.fill(QColor(Qt::white).rgb());

	QPainter painter(&imageBackground);
	MainScene->render(&painter);

	if(dialog.exec() == QDialog::Accepted)
	{
		pixMap = QPixmap::fromImage(imageBackground);
		pixMap = pixMap.scaled(printer.pageRect().width(), printer.pageRect().height(), Qt::KeepAspectRatio);

		QPainter painter1;
		painter1.begin(&printer);
		painter1.drawPixmap (0, 0, pixMap);
		painter1.end();
	}


	//restore system data
	for(QList<QGraphicsItem *>::iterator i = wasSelected.begin(); i != wasSelected.end(); i++)
	{
		QGraphicsRectItem * rect = (QGraphicsRectItem *)*i;
		rect->setBrush(QBrush(ColorForSelect));
	}

	Placeholder->setVisible(wasPlaceholder);
}

void DiagComponentEditor::SaveDCE()
{
	//get filename
	QString filename = QFileDialog::getSaveFileName(this,QString::fromLocal8Bit("����������"),QDir::currentPath(),QString("DiagramComponentEditor files (*.dce)"));

	//verify result
	if(!filename.isEmpty())
	{
		if(!SaveDocument(filename))
		{
			QMessageBox::warning(this,QString::fromLocal8Bit("������!"),QString::fromLocal8Bit("�� ���������� ��������� ���������!"));
		}
		else
		{
			QMessageBox::information(this,QString::fromLocal8Bit("�����"),QString::fromLocal8Bit("��������� �� ������:\n%1").arg(filename));
		}
	}
}

void DiagComponentEditor::LoadDCE()
{
	//get filename
	QString filename = QFileDialog::getOpenFileName(this,QString::fromLocal8Bit("����������"),QDir::currentPath(),QString("DiagramComponentEditor files (*.dce)"));

	//verify result
	if(!filename.isEmpty())
	{
		if(!LoadDocument(filename))
		{
			QMessageBox::warning(this,QString::fromLocal8Bit("������!"),QString::fromLocal8Bit("�� ���������� ��������� ���������!"));
		}
		else
		{
			QMessageBox::information(this,QString::fromLocal8Bit("�����"),QString::fromLocal8Bit("���������!"));
		}
	}
}





//functions
bool DiagComponentEditor::SaveAsImage(QString fileName)
{
	QImage imageBackground(MainScene->width(), MainScene->height(), QImage::Format_RGB32);
	imageBackground.fill(QColor(Qt::white).rgb());

	QPainter painter(&imageBackground);
	MainScene->render(&painter);

	return imageBackground.save(fileName);
	//QPixmap pixMap = QPixmap::grabWidget(ui.WorkPlace);
	//return pixMap.save(fileName);
}

bool DiagComponentEditor::SaveDocument(QString filename)
{
	QList<QGraphicsItem *> allItems = MainScene->items();
	int size = allItems.size();

	ChildsItems alreadySavedPorts;

	QFile file(filename);
	file.open(QIODevice::WriteOnly);

	QXmlStreamWriter xmlWriter(&file);
	xmlWriter.setAutoFormatting(true);
	xmlWriter.writeStartDocument();

	xmlWriter.writeStartElement("Start");
	xmlWriter.writeTextElement("GlobalID",QString::number(GlobalID));
	xmlWriter.writeTextElement("Scale",QString::number(ui.ScaleSpin->value()));

	for(int i=0; i<size; i++)
	{
		if(allItems[i]->parentItem() == 0)
		{
			if(allItems[i]->data(1) == QVariant("Component"))
			{
				if(!WriteComponent(&xmlWriter,allItems[i],&alreadySavedPorts))
				{
					file.close();
					return false;
				}
			}
			else if(allItems[i]->data(1) == QVariant("Comment"))
			{
				if(!WriteComment(&xmlWriter,allItems[i]))
				{
					file.close();
					return false;
				}
			}
			else if(allItems[i]->data(1) == QVariant("OutputPort") || allItems[i]->data(1) == QVariant("InputPort"))
			{
				if(!WritePort(&xmlWriter,allItems[i],&alreadySavedPorts))
				{
					file.close();
					return false;
				}
			}
		}
	}

	xmlWriter.writeEndElement();
	xmlWriter.writeEndDocument();
	file.close();

	return true;
}

bool DiagComponentEditor::LoadDocument(QString filename)
{
	CreateNewDocument();

	QFile file(filename);
	
	if (!file.open(QFile::ReadOnly | QFile::Text))
	{
		QMessageBox::warning(this,QString::fromLocal8Bit("������!"),QString::fromLocal8Bit("�� ������� ��������� ����!"));
		return false;
	}

	QXmlStreamReader Rxml(&file);
	Rxml.readNextStartElement();

	while(!Rxml.atEnd())
	{
		if(Rxml.isStartElement())
		{
			if(Rxml.name() == "Start")
			{
				Rxml.readNext();
				continue;
			}
			else if(Rxml.name() == "GlobalID")
			{
				Rxml.readNext();
				GlobalID = Rxml.text().toString().toUInt();
			}
			else if(Rxml.name() == "Scale")
			{
				Rxml.readNext();
				ui.ScaleSpin->setValue(Rxml.text().toString().toInt());
			}
			else if(Rxml.isStartElement())
			{
				if(Rxml.name() == "Comment")
				{
					if(!LoadComment(&Rxml))
					{
						QMessageBox::warning(this,QString::fromLocal8Bit("������!"),QString::fromLocal8Bit("�� ���� �������� �����������!"));
						return false;
					}
				}
				else if(Rxml.name() == "Component")
				{
					Rxml.readNext();

					if(!LoadComponent(&Rxml,0,0))
					{
						QMessageBox::warning(this,QString::fromLocal8Bit("������!"),QString::fromLocal8Bit("�� ���� �������� ���������!"));
						return false;
					}
				}
				else if(Rxml.name() == "InputPort" || Rxml.name() == "OutputPort")
				{
					/*if(!LoadPort(&Rxml,0))
					{
						QMessageBox::warning(this,QString::fromLocal8Bit("������!"),QString::fromLocal8Bit("�� ���� �������� ����!"));
						return false;
					}*/
					QMessageBox::warning(this,QString::fromLocal8Bit("������!"),QString::fromLocal8Bit("�� ��������� ����!"));
					return false;
				}
			}
			else if(!Rxml.isStartElement() && !Rxml.isEndElement() && !Rxml.isStartDocument() && !Rxml.isEndDocument())
			{
				Rxml.raiseError(QString::fromLocal8Bit("�������������� �������"));
			}
		}
		
		Rxml.readNext();
	}

	file.close();

	if (Rxml.hasError())
	{
		qWarning()<<Rxml.errorString()<<" "<<Rxml.error();
        QMessageBox::warning(this,QString::fromLocal8Bit("������!"),QString::fromLocal8Bit("�� ������� ���������� ����! ��������, ���� ���������."));
		return false;
	}
	else if (file.error() != QFile::NoError)
	{
		QMessageBox::warning(this,QString::fromLocal8Bit("������!"),QString::fromLocal8Bit("�� ������� ��������� ����!"));
		return false;
	}

	return true;
}



//write components

bool DiagComponentEditor::WriteComment(QXmlStreamWriter *fileForWrite, QGraphicsItem *item)
{
	//get text
	QString text = QString();

	QList<QGraphicsItem *> chlds = item->childItems();
	int size = chlds.size();
	bool found = false;

	for(int i=0; i<size; i++)
	{
		if(chlds[i]->data(1) == QVariant("TextContent"))
		{
			QGraphicsTextItem * tmp = (QGraphicsTextItem *)chlds[i];
			text = tmp->toPlainText();
			found = true;
			break;
		}
	}

	if(!found)
	{
		QMessageBox::warning(this,QString::fromLocal8Bit("������!"),QString::fromLocal8Bit("��������� ����������� �������(%1). ���������� � ����� ���� ����������").arg(QString::fromLocal8Bit("�����������")));
		return false;
	}

	fileForWrite->writeStartElement("Comment");
	fileForWrite->writeTextElement("ID",item->data(0).toString());
	fileForWrite->writeTextElement("Text",text);
	fileForWrite->writeTextElement("X",QString::number(item->x()));
	fileForWrite->writeTextElement("Y",QString::number(item->y()));
	fileForWrite->writeEndElement();

	return true;
}

bool DiagComponentEditor::WriteComponent(QXmlStreamWriter * fileForWrite, QGraphicsItem * item, ChildsItems * alreadySavedPorts)
{
	//get text
	QString text = QString();

	QList<QGraphicsItem *> chlds = item->childItems();
	int size = chlds.size();
	bool found = false;

	for(int i=0; i<size; i++)
	{
		if(chlds[i]->data(1) == QVariant("TextContent"))
		{
			QGraphicsTextItem * tmp = (QGraphicsTextItem *)chlds[i];
			text = tmp->toPlainText();
			found = true;
			break;
		}
	}

	if(!found)
	{
		QMessageBox::warning(this,QString::fromLocal8Bit("������!"),QString::fromLocal8Bit("��������� ����������� �������(%1). ���������� � ����� ���� ����������").arg(QString::fromLocal8Bit("���������")));
		return false;
	}

	fileForWrite->writeStartElement("Component");
	fileForWrite->writeTextElement("ID",item->data(0).toString());
	fileForWrite->writeTextElement("Text",text);
	fileForWrite->writeTextElement("X",QString::number(item->x()));
	fileForWrite->writeTextElement("Y",QString::number(item->y()));

	//get linked comment - data 7
	if(item->data(7) != QVariant(0))
	{
		fileForWrite->writeTextElement("ConnectedComment",item->data(7).value<QGraphicsItem *>()->data(7).value<LinkPoints>().input->data(0).toString());
	}

	//get children components - data 5
	if(item->data(5) != QVariant(0))
	{
		fileForWrite->writeStartElement("Children_components");

		QList<ChildsItems> CIlist = item->data(5).value<QList<ChildsItems>>();
		QList<QGraphicsItem *> CurList;

		int sizek;
		size = CIlist.size();

		for(int i=0; i<size; i++)
		{
			CurList = CIlist[i].line;

			sizek = CurList.size();
			for(int k=0; k<sizek; k++)
			{
				if(!WriteComponent(fileForWrite,CurList[k],alreadySavedPorts))
				{
					QMessageBox::warning(this,QString::fromLocal8Bit("������!"),QString::fromLocal8Bit("��������� ����������� �������(%1). ���������� �� ����� ���� ����������").arg(QString::fromLocal8Bit("���������")));
					return false;
				}
			}
		}
		
		fileForWrite->writeEndElement();
	}

	//get other children - ports
	for(int i=0; i<size; i++)
	{
		if(chlds[i]->data(1) == QVariant("InputPort") || chlds[i]->data(1) == QVariant("OutputPort"))
		{
			if(!WritePort(fileForWrite,chlds[i],alreadySavedPorts))
			{
				QMessageBox::warning(this,QString::fromLocal8Bit("������!"),QString::fromLocal8Bit("��������� ����������� �������(%1). ���������� �� ����� ���� ����������").arg(QString::fromLocal8Bit("����")));
				return false;
			}
		}
	}


	fileForWrite->writeEndElement();

	return true;
}

bool DiagComponentEditor::WritePort(QXmlStreamWriter *fileForWrite, QGraphicsItem *item, ChildsItems * alreadySavedPorts)
{
	QString PortType = item->data(1).toString();

	fileForWrite->writeStartElement(PortType);
	fileForWrite->writeTextElement("ID",item->data(0).toString());
	fileForWrite->writeTextElement("X",QString::number(item->x()));
	fileForWrite->writeTextElement("Y",QString::number(item->y()));
	fileForWrite->writeTextElement("Rotation",QString::number(item->rotation()));

	//get connected ports - data 4
	if(item->data(4) != QVariant(0))
	{
		fileForWrite->writeStartElement("Connected_with_this");

		QList<LinkPoints> CIlist = item->data(4).value<QList<LinkPoints>>();
		QGraphicsItem * NotThisItem;

		int sizek;
		int size = CIlist.size();

		for(int i=0; i<size; i++)
		{
			if(!alreadySavedPorts->line.contains(CIlist[i].connectionLine))//not saved yet
			{
				NotThisItem = item==CIlist[i].input?CIlist[i].input:CIlist[i].output;
				
				fileForWrite->writeStartElement("Port");
				fileForWrite->writeTextElement("TypePort",NotThisItem->data(1).toString());
				fileForWrite->writeTextElement("ID",NotThisItem->data(0).toString());
				fileForWrite->writeEndElement();

				alreadySavedPorts->line.append(CIlist[i].connectionLine);
			}
		}

		fileForWrite->writeEndElement();
	}

	fileForWrite->writeEndElement();

	return true;
}


//read components

bool DiagComponentEditor::LoadComment(QXmlStreamReader *fileForRead)
{
	ElemComment comment;
	QGraphicsItem * result;

	while(!(fileForRead->name() == "Comment" && fileForRead->isEndElement()))
	{
		if(fileForRead->isStartElement())
		{
			if(fileForRead->name() == "ID")
			{
				fileForRead->readNext();

				comment = ElemComment(fileForRead->text().toString().toUInt());
				comment.ColorSelecting(QBrush(Qt::NoBrush), QPen(Qt::NoPen));
				result = comment.getThisElement();

				MainScene->addItem(result);
			}
			else if(fileForRead->name() == "Text")
			{
				fileForRead->readNext();

				QList<QGraphicsItem *> chlds = result->childItems();
				int size = chlds.size();

				for(int i=0; i<size; i++)
				{
					if(chlds[i]->data(1) == QVariant("TextContent"))
					{
						QGraphicsTextItem * tmp = (QGraphicsTextItem *)chlds[i];
						tmp->setPlainText(fileForRead->text().toString());
						break;
					}
				}
			}
			else if(fileForRead->name() == "X")
			{
				fileForRead->readNext();

				result->setX(fileForRead->text().toString().toFloat());
			}
			else if(fileForRead->name() == "Y")
			{
				fileForRead->readNext();

				result->setY(fileForRead->text().toString().toFloat());
			}
			else if(!fileForRead->isStartElement() && !fileForRead->isEndElement() && !fileForRead->isStartDocument() && !fileForRead->isEndDocument())
			{
				fileForRead->raiseError(QString::fromLocal8Bit("�������������� �������"));
				return false;
			}
		}

		fileForRead->readNext();
	}

	return true;
}

bool DiagComponentEditor::LoadComponent(QXmlStreamReader *fileForRead, QGraphicsItem * parent = 0, QGraphicsItem * resultAnsw = 0)
{
	ComponentEl component;
	QGraphicsItem * result;
	QGraphicsItem * answer;

	QList<QGraphicsItem *> chldsComps;

	while(!(fileForRead->name() == "Component" && fileForRead->isEndElement()))
	{
		qDebug()<<fileForRead->name()<<"   "<<fileForRead->text();
		if(fileForRead->isStartElement())
		{
			if(fileForRead->name() == "ID")
			{
				fileForRead->readNext();

				component = ComponentEl(fileForRead->text().toString().toUInt());
				component.ColorSelecting(QBrush(Qt::NoBrush), QPen(Qt::NoPen));
				result = component.getThisElement();

				MainScene->addItem(result);
			}
			else if(fileForRead->name() == "Text")
			{
				fileForRead->readNext();

				QList<QGraphicsItem *> chlds = result->childItems();
				int size = chlds.size();

				for(int i=0; i<size; i++)
				{
					if(chlds[i]->data(1) == QVariant("TextContent"))
					{
						QGraphicsTextItem * tmp = (QGraphicsTextItem *)chlds[i];
						tmp->setPlainText(fileForRead->text().toString());
						break;
					}
				}
			}
			else if(fileForRead->name() == "X")
			{
				fileForRead->readNext();

				result->setX(fileForRead->text().toString().toFloat());
			}
			else if(fileForRead->name() == "Y")
			{
				fileForRead->readNext();

				result->setY(fileForRead->text().toString().toFloat());
			}
			else if(fileForRead->name() == "InputPort" || fileForRead->name() == "OutputPort")
			{
				if(!LoadPort(fileForRead,result))
				{
					return false;
				}
			}
			else if(fileForRead->name() == "ConnectedComment")
			{
				QList<QGraphicsItem *> items = MainScene->items();
				int size = items.size();
				bool founded = false;

				fileForRead->readNext();

				for(int i=0; i<size && !founded; i++)
				{
					if(items[i]->data(0) == QVariant(fileForRead->text().toString().toUInt()))
					{
						LinkCommentWithComponent(items[i],result);
						founded = true;
					}
				}

				if(!founded)
				{
					QMessageBox::warning(this,QString::fromLocal8Bit("������!"),QString::fromLocal8Bit("�������� ��������� ���������! ����������� ������ ����������� �� �����������!"));
					return false;
				}

			}
			else if(fileForRead->name() == "Children_components")
			{
				fileForRead->readNext();
				continue;
			}
			else if(fileForRead->name() == "Component")
			{
				fileForRead->readNext();

				if(!LoadComponent(fileForRead,0,answer))
				{
					QMessageBox::warning(this,QString::fromLocal8Bit("������!"),QString::fromLocal8Bit("�� ���� �������� �������� ���������!"));
					return false;
				}

				chldsComps.append(answer);
			}
			else if(!fileForRead->isStartDocument() && !fileForRead->isEndDocument())
			{
				fileForRead->raiseError(QString::fromLocal8Bit("�������������� �������"));
				return false;
			}
		}

		fileForRead->readNext();
	}

	if(fileForRead->hasError())
	{
		qWarning()<<fileForRead->error()<<"   "<<fileForRead->errorString();
	}

	//set parent if needed
	if(!fileForRead->hasError() && chldsComps.size())
	{
		chldsComps.append(result);
		SetParentForComponents(chldsComps);
	}

	resultAnsw = result;//return

	return true;
}

bool DiagComponentEditor::LoadPort(QXmlStreamReader *fileForRead, QGraphicsItem * parent = 0)
{
	PortClass port;
	QGraphicsItem * result;
	QGraphicsItem * answer;
	bool PortType = true;

	QList<QGraphicsItem *> chldsComps;

	while(!(fileForRead->name() == "Component" && fileForRead->isEndElement()))
	{
		if(fileForRead->isStartElement())
		{
			if(fileForRead->name() == "InputPort")
			{
				PortType = true;
			}
			else if(fileForRead->name() == "OutputPort")
			{
				PortType = false;
			}
			else if(fileForRead->name() == "ID")
			{
				fileForRead->readNext();

				port = PortClass(PortType,fileForRead->text().toString().toUInt());
				port.ColorSelecting(QBrush(Qt::NoBrush), QPen(Qt::NoPen));
				result = port.getThisElement();

				MainScene->addItem(result);
			}
			else if(fileForRead->name() == "X")
			{
				fileForRead->readNext();

				result->setX(fileForRead->text().toString().toFloat());
			}
			else if(fileForRead->name() == "Y")
			{
				fileForRead->readNext();

				result->setY(fileForRead->text().toString().toFloat());
			}
			else if(fileForRead->name() == "Rotation")
			{
				fileForRead->readNext();

				result->setRotation(fileForRead->text().toString().toInt());
			}
			else if(fileForRead->name() == "Connected_with_this")
			{
				fileForRead->readNext();
				continue;
			}
			else if(!fileForRead->isStartDocument() && !fileForRead->isEndDocument())
			{
				fileForRead->raiseError(QString::fromLocal8Bit("�������������� �������"));
				return false;
			}
		}

		fileForRead->readNext();
	}

	return true;
}