/****************************************************************************
** Meta object code from reading C++ file 'diagcomponenteditor.h'
**
** Created: Thu 19. Dec 23:11:59 2013
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../diagcomponenteditor.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'diagcomponenteditor.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_DiagComponentEditor[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
      19,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      21,   20,   20,   20, 0x08,
      40,   20,   20,   20, 0x08,
      65,   20,   20,   20, 0x08,
      88,   20,   20,   20, 0x08,
     111,   20,   20,   20, 0x08,
     134,   20,   20,   20, 0x08,
     159,  157,   20,   20, 0x08,
     200,  157,   20,   20, 0x08,
     240,  157,   20,   20, 0x08,
     287,  283,   20,   20, 0x08,
     311,   20,   20,   20, 0x08,
     328,   20,   20,   20, 0x08,
     344,   20,   20,   20, 0x08,
     366,  362,   20,   20, 0x08,
     382,   20,   20,   20, 0x08,
     402,   20,   20,   20, 0x08,
     412,   20,   20,   20, 0x08,
     426,   20,   20,   20, 0x08,
     436,   20,   20,   20, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_DiagComponentEditor[] = {
    "DiagComponentEditor\0\0SwitchSelectMode()\0"
    "SwitchComponentAddMode()\0"
    "SwitchCommentAddMode()\0SwitchReqPortAddMode()\0"
    "SwitchPrdPortAddMode()\0SwitchCreateLinkMode()\0"
    "e\0FixMouseClick(QGraphicsSceneMouseEvent*)\0"
    "FixMouseMove(QGraphicsSceneMouseEvent*)\0"
    "FixMouseRelease(QGraphicsSceneMouseEvent*)\0"
    "pos\0ShowContextMenu(QPoint)\0"
    "ConfirmEditing()\0CancelEditing()\0"
    "VerifyMaxLength()\0val\0ScaleScene(int)\0"
    "CreateNewDocument()\0SaveJPG()\0"
    "PrintResult()\0SaveDCE()\0LoadDCE()\0"
};

const QMetaObject DiagComponentEditor::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_DiagComponentEditor,
      qt_meta_data_DiagComponentEditor, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &DiagComponentEditor::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *DiagComponentEditor::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *DiagComponentEditor::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_DiagComponentEditor))
        return static_cast<void*>(const_cast< DiagComponentEditor*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int DiagComponentEditor::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: SwitchSelectMode(); break;
        case 1: SwitchComponentAddMode(); break;
        case 2: SwitchCommentAddMode(); break;
        case 3: SwitchReqPortAddMode(); break;
        case 4: SwitchPrdPortAddMode(); break;
        case 5: SwitchCreateLinkMode(); break;
        case 6: FixMouseClick((*reinterpret_cast< QGraphicsSceneMouseEvent*(*)>(_a[1]))); break;
        case 7: FixMouseMove((*reinterpret_cast< QGraphicsSceneMouseEvent*(*)>(_a[1]))); break;
        case 8: FixMouseRelease((*reinterpret_cast< QGraphicsSceneMouseEvent*(*)>(_a[1]))); break;
        case 9: ShowContextMenu((*reinterpret_cast< QPoint(*)>(_a[1]))); break;
        case 10: ConfirmEditing(); break;
        case 11: CancelEditing(); break;
        case 12: VerifyMaxLength(); break;
        case 13: ScaleScene((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 14: CreateNewDocument(); break;
        case 15: SaveJPG(); break;
        case 16: PrintResult(); break;
        case 17: SaveDCE(); break;
        case 18: LoadDCE(); break;
        default: ;
        }
        _id -= 19;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
