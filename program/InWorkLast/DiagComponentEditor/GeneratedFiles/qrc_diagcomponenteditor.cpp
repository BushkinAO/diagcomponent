/****************************************************************************
** Resource object code
**
** Created: Thu 19. Dec 14:41:17 2013
**      by: The Resource Compiler for Qt version 4.7.3
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <QtCore/qglobal.h>

QT_BEGIN_NAMESPACE

QT_END_NAMESPACE


int QT_MANGLE_NAMESPACE(qInitResources_diagcomponenteditor)()
{
    return 1;
}

Q_CONSTRUCTOR_FUNCTION(QT_MANGLE_NAMESPACE(qInitResources_diagcomponenteditor))

int QT_MANGLE_NAMESPACE(qCleanupResources_diagcomponenteditor)()
{
    return 1;
}

Q_DESTRUCTOR_FUNCTION(QT_MANGLE_NAMESPACE(qCleanupResources_diagcomponenteditor))

