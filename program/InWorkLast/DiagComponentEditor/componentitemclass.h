#ifndef COMPONENTCLASS_H
#define COMPONENTCLASS_H

#include <QGraphicsItem>
#include <QGraphicsRectItem>
#include <QGraphicsTextItem>
#include <QString>

#include <QFont>
#include <QBrush>
#include <QColor>
#include <QPen>
#include <QVariant>

class ComponentEl
{
public:
	ComponentEl();
	ComponentEl(uint ID);

	//functions
	QGraphicsItem * getThisElement();
	void ColorSelecting(QBrush brush, QPen pen);

private:
	//variables
	QGraphicsRectItem * MainBlock;
	QGraphicsTextItem * CompName;
	QGraphicsRectItem * SelectRect;
	
	QGraphicsRectItem * IconMain;
	QGraphicsRectItem * IconUp;
	QGraphicsRectItem * IconDown;
};

#endif