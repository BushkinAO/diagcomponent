#ifndef PORTCLASS_H
#define PORTCLASS_H

#include <QGraphicsItem>
#include <QGraphicsRectItem>
#include <QGraphicsTextItem>
#include <QGraphicsEllipseItem>
#include <QString>

#include <QFont>
#include <QBrush>
#include <QColor>
#include <QPen>
#include <QVariant>

class PortClass
{
public:
	PortClass(bool isInput = false, uint ID = 0);

	//functions
	QGraphicsItem * getThisElement();
	void ColorSelecting(QBrush brush, QPen pen);

private:
	//variables
	QGraphicsPolygonItem * MainBlock;
	QGraphicsPolygonItem * OutPartLine;
	QGraphicsItem * Connector;
	QGraphicsRectItem * SelectRect;
};

#endif