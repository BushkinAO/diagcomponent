#ifndef DIAGCOMPONENTEDITOR_H
#define DIAGCOMPONENTEDITOR_H

#include <QtGui/QMainWindow>

#include "ui_diagcomponenteditor.h"
#include "componentitemclass.h"
#include "commentitemclass.h"
#include "portsclass.h"
#include "workscene.h"

#include <QString>
#include <QGraphicsScene>
#include <QList>
#include <QMap>
#include <QAction>
#include <QMenu>
#include <QRect>
#include <QPointF>
#include <QPushButton>

#include <QLineEdit>
#include <QTextEdit>
#include <QFontMetrics>

#include <QPixmap>
#include <QFileDialog>
#include <QDir>
#include <QPrintDialog>
#include <QPrinter>

#include <QMetaType>

#include <QXmlStreamWriter>
#include <QXmlStreamReader>

#include <QMessageBox>
#include <QDebug>

struct LinkPoints
{
	QGraphicsItem * input;
	QGraphicsItem * output;
	QGraphicsItem * connectionLine;

	bool operator==(const LinkPoints otherstruc)
	{
		if(this->input == otherstruc.input && this->output == otherstruc.output)
		{
			return true;
		}
		
		return false;
	}
};
Q_DECLARE_METATYPE(LinkPoints)
Q_DECLARE_METATYPE(QList<LinkPoints>)

struct ChildsItems
{
	QList<QGraphicsItem *> line;
};
Q_DECLARE_METATYPE(QList<ChildsItems>)

Q_DECLARE_METATYPE(QPolygonF)

class DiagComponentEditor : public QMainWindow
{
	Q_OBJECT

public:
	DiagComponentEditor(QWidget *parent = 0, Qt::WFlags flags = 0);
	~DiagComponentEditor();

	enum WhatsStateActive {mSELECT = 10, mCOMPONENT, mCOMMENT, mRecPORT, mPrdPORT, mPortsLINK, mEditTEXT};

private:
	Ui::MainWindow ui;

	//variables
	WhatsStateActive MainState;//states for left menu buttons
	WorkScene * MainScene;//work scene

	uint GlobalID;

	QLineEdit * LineEditText;//for editing text component
	QTextEdit * TextEditText;//for editing text comment
	
	QColor ColorForSelect;
	QColor ColorError;
	QColor ColorMain;

	QList<QGraphicsItem *> SelectedItems;//list of selected items
	QGraphicsRectItem * Placeholder;//placeholder
	QGraphicsItem * ItemForAddPorts;//item, which chosen to add port
	QGraphicsItem * RecPortItemForLink;
	QGraphicsItem * PrdPortItemForLink;
		
	QPointF LastAllowedPosition;//last NOT itersect positon when move selected
	
	QGraphicsTextItem * TextThatEditing;//here lay textitem, that editing


	//functions
	//in file diagcomponenteditor_fun.cpp
	QGraphicsItem * FindMainParent(QGraphicsItem * item, QString status);//find parent of this item, that must associate with full component
	//QList<QGraphicsItem *> ItersectSomething(QGraphicsItem * item);//is item intersect comment or component on MainScene

	void TurnOnPlaceholder(QGraphicsItem * item, bool onoff);//truen on placeholder, add to it item and set it's visibility on onoff
	void TurnOffPlaceholder();//turn off placeholder
	void ColorModeButton(QPushButton * button);//for color-mark active button

	void SelectIt(QGraphicsItem * item, bool onoff);//turn select on/off
	void SelectOneItem(QGraphicsItem * clicked);//select clicked or not, add to selected - fully complicated selection

	QGraphicsItem * GetSelectionItem(QGraphicsItem * item);//get selection item of input item
	int countTaggedElements(QList<QGraphicsItem *> list, QVariant tag);
	void ClearSelection();

	//text
	void CalculateTextLength(QGraphicsItem * textitem);

	//work with parents
	void SetParentForComponents(QList<QGraphicsItem *> items);
	void RemoveParentForComponent(QGraphicsItem * item);
	void ResizeParentAfterExitingElement(QGraphicsItem * parentt);//resize parent when some of it's child gone

	void RemoveParentsForSelected(QList<QGraphicsItem *> items);//for context menu

	QGraphicsItem * ResizeElement(QGraphicsItem * item, int width, int height);

	//make new form of connection line
	void MakeNewFormOfConnection(QGraphicsItem * PrdPort, QGraphicsItem * RecPort);
	void LinkCommentWithComponent(QGraphicsItem * comment, QGraphicsItem * component);
	void MakeConnectionLines();
	QPointF NextPointOfSelectionConnection(int state, QPointF cur, QPointF las, QPointF nex);

	//from context menu
	void DeleteSelectedItems();//delete selected items, it's obvious
	void ChangeText(QGraphicsItem * item);//chage text on element

	//removes
	void RemovePort(QGraphicsItem * port);
	void RemoveItem(QGraphicsItem * item);
	void RemoveCommCompLink(QList<QGraphicsItem *> comments);

	//files functions
	//in file diagcomponenteditor_files.cpp
	bool SaveAsImage(QString fileName);//save work scene in file with name fileName
	bool SaveDocument(QString filename);//save current document
	bool LoadDocument(QString filename);//load document

	//save items
	bool WriteComponent(QXmlStreamWriter * fileForWrite, QGraphicsItem * item, ChildsItems * alreadySavedPorts);
	bool WriteComment(QXmlStreamWriter * fileForWrite, QGraphicsItem * item);
	bool WritePort(QXmlStreamWriter * fileForWrite, QGraphicsItem * item, ChildsItems * alreadySavedPorts);

	//load items
	bool LoadComponent(QXmlStreamReader * fileForRead, QGraphicsItem * parent, QGraphicsItem * resultAnsw);
	bool LoadComment(QXmlStreamReader * fileForRead);
	bool LoadPort(QXmlStreamReader * fileForRead, QGraphicsItem * parent);

private slots://in file diagcomponenteditor.cpp
	void SwitchSelectMode();//turn on select mode
	void SwitchComponentAddMode();//turn on component additing mode
	void SwitchCommentAddMode();//turn on comment additing mode
	void SwitchReqPortAddMode();//turn on required port additing mode
	void SwitchPrdPortAddMode();//turn on required port additing mode
	void SwitchCreateLinkMode();//turn on create link mode

	void FixMouseClick(QGraphicsSceneMouseEvent *e);//fix mouse click on main scene
	void FixMouseMove(QGraphicsSceneMouseEvent *e);//fix mouse move on  main scene
	void FixMouseRelease(QGraphicsSceneMouseEvent *e);//fix mouse move on  main scene

	void ShowContextMenu(QPoint pos);//show context menu

	//editing text buttons
	void ConfirmEditing();
	void CancelEditing();
	
	void VerifyMaxLength();//verified max length of TextEditText

	void ScaleScene(int val);

	//menu
	void CreateNewDocument();

	//in file diagcomponenteditor_files.cpp
	void SaveJPG();//save work place as jpg image
	void PrintResult();//printing
	void SaveDCE();//save diagram
	void LoadDCE();//save diagram
};

#endif // DIAGCOMPONENTEDITOR_H
