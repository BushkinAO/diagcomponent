//Constructor and slots here

#include "diagcomponenteditor.h"

DiagComponentEditor::DiagComponentEditor(QWidget *parent, Qt::WFlags flags)
	: QMainWindow(parent, flags)
{
	ui.setupUi(this);//set ui

	CreateNewDocument();

	//connects
	//left menu
	connect(ui.SelectModeButton,SIGNAL(clicked()),this,SLOT(SwitchSelectMode()));
	connect(ui.ComponentModeButton,SIGNAL(clicked()),this,SLOT(SwitchComponentAddMode()));
	connect(ui.CommentModeButton,SIGNAL(clicked()),this,SLOT(SwitchCommentAddMode()));
	connect(ui.ProvidedPortButton,SIGNAL(clicked()),this,SLOT(SwitchPrdPortAddMode()));
	connect(ui.RequiredPortButton,SIGNAL(clicked()),this,SLOT(SwitchReqPortAddMode()));
	connect(ui.LinkButton,SIGNAL(clicked()),this,SLOT(SwitchCreateLinkMode()));

	//context menu
	connect(ui.WorkPlace,SIGNAL(customContextMenuRequested(QPoint)),this,SLOT(ShowContextMenu(QPoint)));

	//scale
	connect(ui.ScaleSpin,SIGNAL(valueChanged(int)),this,SLOT(ScaleScene(int)));

	//main menu
	connect(ui.actionNew_file,SIGNAL(triggered(bool)),this,SLOT(CreateNewDocument()));
	connect(ui.actionSave_as,SIGNAL(triggered(bool)),this,SLOT(SaveJPG()));
	connect(ui.actionPrint,SIGNAL(triggered(bool)),this,SLOT(PrintResult()));
	connect(ui.actionSave,SIGNAL(triggered(bool)),this,SLOT(SaveDCE()));
	connect(ui.actionOpen_file,SIGNAL(triggered(bool)),this,SLOT(LoadDCE()));

	//text editing buttons
	connect(ui.AgreeButton,SIGNAL(clicked()),this,SLOT(ConfirmEditing()));
	connect(ui.DisagreeButton,SIGNAL(clicked()),this,SLOT(CancelEditing()));
}

DiagComponentEditor::~DiagComponentEditor()
{

}

//slots

void DiagComponentEditor::SwitchSelectMode()
{
	MainState = mSELECT;

	ClearSelection();
	ColorModeButton(ui.SelectModeButton);

	//off
	TurnOffPlaceholder();
}

void DiagComponentEditor::SwitchComponentAddMode()
{
	MainState = mCOMPONENT;

	ClearSelection();
	ColorModeButton(ui.ComponentModeButton);

	//on
	ComponentEl newItemRaw = ComponentEl(GlobalID);
	GlobalID++;
	QGraphicsItem * tmp = newItemRaw.getThisElement();
	
	TurnOnPlaceholder(tmp, true);
}

void DiagComponentEditor::SwitchCommentAddMode()
{
	MainState = mCOMMENT;

	ClearSelection();
	ColorModeButton(ui.CommentModeButton);

	//on
	ElemComment newItemRaw = ElemComment(GlobalID);
	GlobalID++;
	QGraphicsItem * tmp = newItemRaw.getThisElement();

	TurnOnPlaceholder(tmp, true);
}

void DiagComponentEditor::SwitchReqPortAddMode()
{
	MainState = mRecPORT;

	ClearSelection();
	ColorModeButton(ui.RequiredPortButton);

	//on
	QGraphicsItem * tmp = new QGraphicsTextItem(QString::fromLocal8Bit("�������� �� ��������� ��� ���������� �����"),0,MainScene);
	tmp->setData(1,QVariant("Tooltip"));
	
	TurnOnPlaceholder(tmp,true);
}

void DiagComponentEditor::SwitchPrdPortAddMode()
{
	MainState = mPrdPORT;

	ClearSelection();
	ColorModeButton(ui.ProvidedPortButton);

	//on
	QGraphicsItem * tmp = new QGraphicsTextItem(QString::fromLocal8Bit("�������� �� ��������� ��� ���������� �����"),0,MainScene);
	tmp->setData(1,QVariant("Tooltip"));

	TurnOnPlaceholder(tmp,true);
}

void DiagComponentEditor::SwitchCreateLinkMode()
{
	MainState = mPortsLINK;

	ClearSelection();
	ColorModeButton(ui.LinkButton);

	QGraphicsItem * tmp = new QGraphicsTextItem(QString::fromLocal8Bit("�������� �� �������� ����(����)"),0,MainScene);
	tmp->setData(1,QVariant("Tooltip"));

	PrdPortItemForLink = 0;
	RecPortItemForLink = 0;

	//off
	TurnOnPlaceholder(tmp,true);
}

void DiagComponentEditor::FixMouseClick(QGraphicsSceneMouseEvent *e)
{
	QPointF pos = e->scenePos();

	QGraphicsItem * pressed = MainScene->itemAt(pos);
	QGraphicsItem * clicked = 0;

	if(pressed != 0)//find something
	{
		clicked = FindMainParent(pressed,"Item");//find whole item - if it's not it

		//remove all linked with placeholder
		QList<QGraphicsItem *> children = Placeholder->childItems();
		int size = children.size();

		for(int i=0; i<size; i++)
		{
			if(clicked == children[i])//it was placeholder, nothing to do here
			{
				clicked = 0;
				break;
			}
		}
	}

	//work with states
	if(e->button() == Qt::LeftButton)
	{
		if(MainState == mCOMPONENT && Placeholder->brush() == QBrush(ColorForSelect))
		{
			ComponentEl newItemRaw = ComponentEl(GlobalID);
			GlobalID++;
			newItemRaw.ColorSelecting(QBrush(Qt::NoBrush), QPen(Qt::NoPen));
			QGraphicsItem * newItem = newItemRaw.getThisElement();
			newItem->setPos(pos);

			MainScene->addItem(newItem);
		}
		else if(MainState == mCOMMENT && Placeholder->brush() == QBrush(ColorForSelect))
		{
			ElemComment newItemRaw = ElemComment(GlobalID);
			GlobalID++;
			newItemRaw.ColorSelecting(QBrush(Qt::NoBrush), QPen(Qt::NoPen));
			QGraphicsItem * newItem = newItemRaw.getThisElement();
			newItem->setPos(pos);

			MainScene->addItem(newItem);
		}
		else if(MainState == mSELECT)
		{
			//remove selection
			if(clicked == 0)
			{
				ClearSelection();
			}

			//need to add this element to selected?
			if(clicked != 0 && clicked->data(3) == QVariant("NotSelected"))
			{
				SelectOneItem(clicked);
			}
			else if(SelectedItems.size())
			{
				LastAllowedPosition = e->scenePos();
			}
		}
		else if(MainState == mRecPORT || MainState == mPrdPORT)
		{
			//is clicked element can have ports?
			if(clicked != 0 && clicked->data(1) == QVariant("Component") && ItemForAddPorts != clicked)//1 stage
			{
				//set placeholder
				PortClass newItemRaw;

				if(MainState == mRecPORT)
				{
					newItemRaw = PortClass(true,GlobalID);
					GlobalID++;
				}
				else
				{
					newItemRaw = PortClass(false,GlobalID);
					GlobalID++;
				}

				//turn it on
				QGraphicsItem * tmp = newItemRaw.getThisElement();
				TurnOnPlaceholder(tmp, true);

				//mark selectedclicked item
				ItemForAddPorts = clicked;
			}
			else if(ItemForAddPorts != 0 && Placeholder->brush() == QBrush(ColorForSelect))//2 stage
			{
				QGraphicsItem * chPlaceholder = Placeholder->childItems().first();//here must be only one element in array - tagged mainblock
				PortClass newPort;

				if(MainState == mRecPORT)
				{
					newPort = PortClass(true,GlobalID);
					GlobalID++;
				}
				else//mPrdPORT
				{
					newPort = PortClass(false,GlobalID);
					GlobalID++;
				}

				//create new port object
				newPort.ColorSelecting(QBrush(Qt::NoBrush), QPen(Qt::NoPen));
				QGraphicsItem * newItem = newPort.getThisElement();

				//form place gor additing port - because center of this object is not in (0,0)
				QPointF placeRaw = chPlaceholder->scenePos();
				QPointF placeGrilled;
				qreal sizeOffset = newItem->transformOriginPoint().y();
				
				if(Placeholder->rotation() == 90)
				{
					placeGrilled = QPointF(placeRaw.x()-sizeOffset, placeRaw.y()-sizeOffset);
				}
				else if(Placeholder->rotation() == 180)
				{
					placeGrilled = QPointF(placeRaw.x(), placeRaw.y()-sizeOffset*2);
				}
				else if(Placeholder->rotation() == -90)
				{
					placeGrilled = QPointF(placeRaw.x()+sizeOffset, placeRaw.y()-sizeOffset);
				}
				else
				{
					placeGrilled = placeRaw;
				}

				//add new port
				newItem->setRotation(Placeholder->rotation());
				newItem->setParentItem(ItemForAddPorts);
				
				placeGrilled = ItemForAddPorts->mapFromScene(placeGrilled);
				newItem->setPos(placeGrilled);
				
				MainScene->addItem(newItem);//add on scene
			}
		}
		else if(MainState == mPortsLINK)
		{
			if(RecPortItemForLink == 0 && clicked->data(1) == QVariant("InputPort"))//1 stage
			{
				RecPortItemForLink = clicked;
				SelectOneItem(clicked);

				QGraphicsItem * tmp = new QGraphicsTextItem(QString::fromLocal8Bit("�������� �� ������� ����(����)"),0,MainScene);
				tmp->setData(1,QVariant("Tooltip"));

				TurnOnPlaceholder(tmp,true);
			}
			else if(PrdPortItemForLink == 0 && RecPortItemForLink != 0 && clicked->data(1) == QVariant("OutputPort"))//2 stage
			{
				PrdPortItemForLink = clicked;
				SelectOneItem(clicked);

				if(RecPortItemForLink->parentItem() == PrdPortItemForLink->parentItem())//one parent for both ports is not allowed
				{
					QMessageBox::warning(this,QString::fromLocal8Bit("������"),QString::fromLocal8Bit("���������� ������� �����!\n��� ����� ����������� ������ � ���� �� ����������!"));
				}
				else
				{
					MakeNewFormOfConnection(PrdPortItemForLink,RecPortItemForLink);
				}

				SwitchCreateLinkMode();
			}
		}
	}
	
	ui.WorkPlace->update();
}

void DiagComponentEditor::FixMouseMove(QGraphicsSceneMouseEvent *e)
{
	//moving selected element
	if(MainState == mSELECT && e->buttons() & Qt::LeftButton)
	{
		QGraphicsItem * clicked = MainScene->itemAt(e->scenePos());

		//finding under cursor item with tag SelectingRect - for drag it can be only comment or component as parent
		if(clicked != 0 && clicked->parentItem() != 0 && clicked->data(1) == QVariant("SelectingRect") && (clicked->parentItem()->data(1) == QVariant("Component") || clicked->parentItem()->data(1) == QVariant("Comment")))
		{
			//move all selected items
			int size = SelectedItems.size();
			for(int i=0; i<size; i++)
			{
				if(SelectedItems[i]->parentItem()->data(1) != QVariant("Component"))
				{
					SelectedItems[i]->setPos(SelectedItems[i]->scenePos() + (e->scenePos() - e->lastScenePos()));
				}
			}
		}
	}
	

	//placeholder
	//if we have something to move...
	if(Placeholder->isVisible())
	{
		QList<QGraphicsItem *> intersection;
		QGraphicsItem * chPlaceholder = Placeholder->childItems().first();//here must be only one element in array - tagged mainblock

		if(chPlaceholder->data(1) == QVariant("Tooltip"))
		{
			//remove rotation of placeholder
			Placeholder->setRotation(0);

			//moving placeholder
			Placeholder->setPos(e->scenePos() + QPointF(-10,-25));
			Placeholder->setBrush(ColorMain);
		}
		else
		{
			//maybe intersect with something?
			//intersection = ItersectSomething(Placeholder);

			//move placeholder - depends on type of additing element
			//choose type item under placeholder
			if(chPlaceholder->data(1) == QVariant("Comment") || chPlaceholder->data(1) == QVariant("Component"))
			{
				//remove rotation of placeholder
				Placeholder->setRotation(0);

				//moving placeholder
				Placeholder->setPos(e->scenePos());
			}
			else if(chPlaceholder->data(1) == QVariant("InputPort") || chPlaceholder->data(1) == QVariant("OutputPort"))
			{
				//moving placeholder
				if(ItemForAddPorts != 0)
				{
					float X = e->scenePos().x();
					float Y = e->scenePos().y();
					QPointF newPos = e->scenePos();

					/*
					//remove all that not ports or components
					for(QList<QGraphicsItem *>::iterator k = intersection.begin(); k != intersection.end(); k++)
					{
						QGraphicsItem * g = *k;
						QVariant f= (*k)->data(1);
						if((*k)->data(1) != QVariant("InputPort") && (*k)->data(1) != QVariant("OutputPort") && (*k)->data(1) != QVariant("Component"))
						{
							intersection.removeOne((*k));
						}
					}
					intersection.clear();//its tmp!

					//remove current item and all it's childs from intersection
					QList<QGraphicsItem *> thisItemP = ItemForAddPorts->childItems();
					intersection.removeOne(ItemForAddPorts);//remove item itself

					for(QList<QGraphicsItem *>::iterator j = thisItemP.begin(); j!=thisItemP.end(); j++)
					{
						if((*j)->data(1) != QVariant("InputPort") && (*j)->data(1) != QVariant("OutputPort") && (*j)->data(1) != QVariant("Component"))//child is not port
						{
							intersection.removeOne(*j);
						}
					}*/

					//here accuratly - port have rect with some size - 12px - and MUST NOT bulge
					int rectsize = 12;

					QPointF PosIFAP = ItemForAddPorts->parentItem()!=0?ItemForAddPorts->parentItem()->mapToScene(ItemForAddPorts->pos()):ItemForAddPorts->pos();//map to scene for ItemForAddPorts, if it's needed

					if(X < PosIFAP.x())//left
					{
						//verify positon - it must be into bounding rect of ItemForAddPorts
						if(newPos.y() >= PosIFAP.y()+rectsize && newPos.y() <= PosIFAP.y()+ItemForAddPorts->boundingRect().height())
						{
							Placeholder->setRotation(180);
							Placeholder->setPos(PosIFAP.x()+rectsize/2,newPos.y());
						}
					}
					else if(X >= PosIFAP.x() && X <= PosIFAP.x()+ItemForAddPorts->boundingRect().width())//center
					{
						if(Y <= PosIFAP.y() + ItemForAddPorts->boundingRect().height()/2)//up
						{
							//verify positon - it must be into bounding rect of ItemForAddPorts
							if(newPos.x() >= PosIFAP.x() && newPos.x() <= ItemForAddPorts->x()+ItemForAddPorts->boundingRect().width()-rectsize)
							{
								Placeholder->setRotation(-90);
								Placeholder->setPos(newPos.x(),PosIFAP.y()+rectsize/2);
							}
						}
						else//down
						{
							//verify positon - it must be into bounding rect of ItemForAddPorts
							if(newPos.x() >= PosIFAP.x()+rectsize && newPos.x() <= PosIFAP.x()+ItemForAddPorts->boundingRect().width())
							{
								Placeholder->setRotation(90);
								Placeholder->setPos(newPos.x(),PosIFAP.y()+ItemForAddPorts->boundingRect().height()-rectsize/2);
							}
						}
					}
					else//right
					{
						//verify positon - it must be into bounding rect of ItemForAddPorts
						if(newPos.y() >= PosIFAP.y() && newPos.y() <= PosIFAP.y()+ItemForAddPorts->boundingRect().height()-rectsize)
						{
							Placeholder->setRotation(0);
							Placeholder->setPos(PosIFAP.x()+ItemForAddPorts->boundingRect().width()-rectsize/2,newPos.y());
						}
					}
				}
			}
		
			//set color for placeholder
			if(!intersection.isEmpty())
			{
				Placeholder->setBrush(ColorError);
			}
			else
			{
				Placeholder->setBrush(ColorForSelect);
			}
		}
	}
}

void DiagComponentEditor::FixMouseRelease(QGraphicsSceneMouseEvent *e)
{
	if(MainState == mSELECT && e->button() == Qt::LeftButton)
	{
		/*
		//out of scene
		QList<QGraphicsItem *> allItems = MainScene->items();
		QList<QGraphicsItem *> returnPosItems;

		QGraphicsItem * tmp;
		QGraphicsPolygonItem * sceneR = new QGraphicsPolygonItem(ui.WorkPlace->property("ViewRect").value<QPolygonF>(),0,MainScene);

		int sizek;
		int size = allItems.size();

		for(int i=0; i<size; i++)
		{
			if(!sceneR->collidesWithItem(allItems[i]))
			{
				sizek = SelectedItems.size();

				for(int k=0; k<sizek; k++)
				{
					tmp = FindMainParent(SelectedItems[k],"Diagram");

					if(returnPosItems.indexOf(tmp) == -1)//not verified yet
					{
						tmp->setPos(SelectedItems[k]->scenePos() - (e->scenePos() - LastAllowedPosition));
						returnPosItems.append(tmp);
					}
				}

				QMessageBox::warning(this,QString::fromLocal8Bit("������!"),QString::fromLocal8Bit("��� ������� ���� �� ��������� ����� �� ������� ������� �������! ��������� ����������� ����� ��������!\n\n�����: ���������� ��������� �������, ���� ������ ��������� ������� �������."));
				break;
			}
		}

		MainScene->removeItem(sceneR);
*/


		//make connection line
		MakeConnectionLines();
	}



	/*
	int size = SelectedItems.size();
	bool intersected = false;

	if(MainState == mSELECT && e->button() == Qt::LeftButton)
	{
		QList<QGraphicsItem *> children;
		int sizej;

		for(int i=0; i<size; i++)
		{
			if(SelectedItems[i]->data(1) == QVariant("Component"))//selected component
			{
				children = SelectedItems[i]->childItems();
				children.append(SelectedItems[i]);//items itself MUST also be verified
				sizej = children.size();

				for(int j=0; j<sizej; j++)
				{
					//grab selctingrect of item and verify it on collision. if it have no collicsion - all elemen have not collision
					if(children[j]->data(1) == QVariant("SelectingRect"))
					{
						//find itersection
						QList<QGraphicsItem *> IntersectionCurrent = ItersectSomething(children[j]);
						IntersectionCurrent.removeOne(SelectedItems[i]);//remove selected item itself - it cant collide with itself

						//remove all that cant intersect with selected item
						for(QList<QGraphicsItem *>::iterator k = IntersectionCurrent.begin(); k != IntersectionCurrent.end(); k++)
						{
							if(((*k)->data(1) != QVariant("Component") && (*k)->data(1) != QVariant("InputPort") && (*k)->data(1) != QVariant("OutputPort")))
							{
								QVariant f = (*k)->data(1);
								IntersectionCurrent.removeOne((*k));
							}
						}

						if(IntersectionCurrent.size())
						{
							for(int i=0; i<size; i++)
							{
								SelectedItems[i]->setPos(SelectedItems[i]->scenePos() - (e->scenePos() - LastAllowedPosition));
							}

							intersected = true;
							break;//time economy
						}
					}
				}
				
				if(intersected)
				{
					break;//time economy
				}
			}
		}
	}*/
}







//ContextMenu starts here
void DiagComponentEditor::ShowContextMenu(QPoint pos)
{
	//create
	QMenu menu;
	bool allComponents;

	QGraphicsItem * comm;
	QGraphicsItem * comp;

	QList<QGraphicsItem *> comments;
	
	//clicked on selected item
	if(MainState == mSELECT && SelectedItems.size())
	{
		//QAction *action1 = new QAction(QString::fromLocal8Bit("��������"), ui.WorkPlace);
		//menu.addAction(action1);

		//QAction *action2 = new QAction(QString::fromLocal8Bit("����������"), ui.WorkPlace);
		//menu.addAction(action2);

		//QAction *action3 = new QAction(QString::fromLocal8Bit("��������"), ui.WorkPlace);
		//menu.addAction(action3);

		//menu.addSeparator();

		//4
		if(SelectedItems.size() == 1 && (SelectedItems.first()->data(1) == QVariant("Component") || SelectedItems.first()->data(1) == QVariant("Comment")) && TextThatEditing == 0)
		{
			QAction *action4 = new QAction(QString::fromLocal8Bit("�������� �����"), ui.WorkPlace);
			menu.addAction(action4);
		}

		//5
		QAction *action5 = new QAction(QString::fromLocal8Bit("������� ����������"), ui.WorkPlace);
		menu.addAction(action5);

		//6
		if(SelectedItems.size() >= 2)
		{
			allComponents = true;

			for(QList<QGraphicsItem *>::iterator it = SelectedItems.begin(); it != SelectedItems.end() && allComponents; it++)
			{
				if((*it)->data(1) != QVariant("Component"))
				{
					allComponents = false;
				}
			}
		
			if(allComponents)//all selected elements are components
			{
				QAction *action6 = new QAction(QString::fromLocal8Bit("���������� ��������"), ui.WorkPlace);
				menu.addAction(action6);
			}
			
		}

		//7
		allComponents = true;

		for(QList<QGraphicsItem *>::iterator it = SelectedItems.begin(); it != SelectedItems.end() && allComponents; it++)
		{
			if((*it)->parentItem() == 0 || (*it)->data(1) != QVariant("Component"))
			{
				allComponents = false;
			}
		}
		
		if(allComponents)//all selected elements are components
		{
			QAction *action7 = new QAction(QString::fromLocal8Bit("�������� ����� � ���������"), ui.WorkPlace);
			menu.addAction(action7);
		}

		//8
		if(SelectedItems.size() == 2)
		{
			if(SelectedItems[0]->data(1) == QVariant("Comment") && SelectedItems[1]->data(1) == QVariant("Component"))
			{
				comm = SelectedItems[0];
				comp = SelectedItems[1];

				QAction *action8 = new QAction(QString::fromLocal8Bit("��������� �����������"), ui.WorkPlace);
				menu.addAction(action8);
			}
			else if(SelectedItems[0]->data(1) == QVariant("Component") && SelectedItems[1]->data(1) == QVariant("Comment"))
			{
				comm = SelectedItems[1];
				comp = SelectedItems[0];

				QAction *action8 = new QAction(QString::fromLocal8Bit("��������� �����������"), ui.WorkPlace);
				menu.addAction(action8);
			}
		}

		//9
		if(SelectedItems.size())
		{
			for(QList<QGraphicsItem *>::iterator it = SelectedItems.begin(); it != SelectedItems.end(); it++)
			{
				if((*it)->data(1) == QVariant("Comment"))
				{
					comments.append(*it);
				}
			}
		
			if(comments.size())//we have comments
			{
				QAction *action9 = new QAction(QString::fromLocal8Bit("�������� �����������"), ui.WorkPlace);
				menu.addAction(action9);
			}
			
		}


		//call
		QAction* selectedItem = menu.exec(ui.WorkPlace->mapToGlobal(pos));
		if (selectedItem)
		{
			if(selectedItem->text() == QString::fromLocal8Bit("������� ����������"))
			{
				DeleteSelectedItems();
			}
			else if(selectedItem->text() == QString::fromLocal8Bit("�������� �����"))
			{
				MainState = mEditTEXT;
				ChangeText(SelectedItems.first());
			}
			else if(selectedItem->text() == QString::fromLocal8Bit("���������� ��������"))
			{
				SetParentForComponents(SelectedItems);
			}
			else if(selectedItem->text() == QString::fromLocal8Bit("�������� ����� � ���������"))
			{
				RemoveParentsForSelected(SelectedItems);
			}
			else if(selectedItem->text() == QString::fromLocal8Bit("��������� �����������"))
			{
				LinkCommentWithComponent(comm,comp);
			}
			else if(selectedItem->text() == QString::fromLocal8Bit("�������� �����������"))
			{
				RemoveCommCompLink(comments);
			}
		}
	}
}

void DiagComponentEditor::ConfirmEditing()
{
	//confirm
	if(LineEditText->isVisible())//for component
	{
		TextThatEditing->setPlainText(LineEditText->text());

		//verify text length
		CalculateTextLength(TextThatEditing);
	}
	else if(TextEditText->isVisible())//for comment
	{
		TextThatEditing->setPlainText(TextEditText->toPlainText());
	}

	//hide
	ui.AgreeButton->setVisible(false);
	ui.DisagreeButton->setVisible(false);
	TextThatEditing->setVisible(true);
	TextEditText->setVisible(false);
	LineEditText->setVisible(false);

	TextThatEditing = 0;
	MainState = mSELECT;
}

void DiagComponentEditor::CancelEditing()
{
	//hide
	ui.AgreeButton->setVisible(false);
	ui.DisagreeButton->setVisible(false);
	TextThatEditing->setVisible(true);
	TextEditText->setVisible(false);
	LineEditText->setVisible(false);

	TextThatEditing = 0;
	MainState = mSELECT;
}

void DiagComponentEditor::VerifyMaxLength()
{
	//int countLines = TextEditText->document()->blockCount();
	int maxNormalCount = 3;
	int maxLineSize = 11;

	if(TextEditText->isVisible())
	{
		if(TextEditText->toPlainText().size() > maxNormalCount*maxLineSize)
		{
			TextEditText->textCursor().deletePreviousChar();
		}
	}
}

void DiagComponentEditor::ScaleScene(int val)
{
	QRectF scRect = ui.WorkPlace->sceneRect();

	float f = ui.WorkPlace->width()*((float)val/100);
	float g = scRect.width();

	scRect.setRect(scRect.x(),scRect.y(),ui.WorkPlace->width()/((float)val/100),ui.WorkPlace->height()/((float)val/100));
	
	ui.WorkPlace->fitInView(scRect);
	ui.WorkPlace->setProperty("ViewRect",QVariant::fromValue<QPolygonF>(ui.WorkPlace->mapToScene(scRect.toRect())));
}





//up menu
void DiagComponentEditor::CreateNewDocument()
{
	MainScene = new WorkScene();//construct main scene

	//clear
	SelectedItems.clear();

	ItemForAddPorts = 0;

	//Colors
	ColorForSelect = QColor(130,200,240);
	ColorError = QColor(210,87,89);
	ColorMain = QColor(255,255,255);

	ui.WorkPlace->setContextMenuPolicy(Qt::CustomContextMenu);//off default context menu

	//set placeholder settings
	Placeholder = new QGraphicsRectItem(0,0,0,0,0,MainScene);
	Placeholder->setBrush(QBrush(ColorForSelect));
	Placeholder->setPen(Qt::NoPen);
	Placeholder->setOpacity(0.33);
	Placeholder->setVisible(false);
	Placeholder->setZValue(1000);

	SwitchSelectMode();//set mode to SELECT

	//for editiong text component
	LineEditText = new QLineEdit(ui.centralWidget);
	LineEditText->setVisible(false);

	TextEditText = new QTextEdit(ui.centralWidget);
	TextEditText->setVisible(false);
	TextEditText->setWordWrapMode(QTextOption::WordWrap);

	ui.AgreeButton->setVisible(false);
	ui.DisagreeButton->setVisible(false);

	TextThatEditing = NULL;

	GlobalID = 0;

	ui.WorkPlace->setScene(MainScene);
	ui.WorkPlace->setSceneRect(0,0,ui.WorkPlace->width(),ui.WorkPlace->height());

	//set standart scale
	ScaleScene(100);
	ui.ScaleSpin->setValue(100);

	//workscene resignal to this
	connect(MainScene,SIGNAL(mousePressed(QGraphicsSceneMouseEvent *)),this,SLOT(FixMouseClick(QGraphicsSceneMouseEvent *)));
	connect(MainScene,SIGNAL(mouseMove(QGraphicsSceneMouseEvent *)),this,SLOT(FixMouseMove(QGraphicsSceneMouseEvent *)));
	connect(MainScene,SIGNAL(mouseReleased(QGraphicsSceneMouseEvent *)),this,SLOT(FixMouseRelease(QGraphicsSceneMouseEvent *)));

	//when text is editing
	connect(TextEditText,SIGNAL(textChanged()),this,SLOT(VerifyMaxLength()));
}