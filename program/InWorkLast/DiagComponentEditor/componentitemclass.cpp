#include "componentitemclass.h"

ComponentEl::ComponentEl()
{
}

ComponentEl::ComponentEl(uint ID)
{
	//create
	MainBlock = new QGraphicsRectItem(0,0,150,75);
	IconMain = new QGraphicsRectItem(0,0,15,12);
	IconUp = new QGraphicsRectItem(10,2,10,3);
	IconDown = new QGraphicsRectItem(10,7,10,3);
	SelectRect = new QGraphicsRectItem(MainBlock->boundingRect(),MainBlock);

	//color
	QBrush whitBr = QBrush(QColor(255,255,255));

	MainBlock->setBrush(whitBr);
	IconMain->setBrush(whitBr);
	IconUp->setBrush(whitBr);
	IconDown->setBrush(whitBr);
	
	//form icon
	IconUp->setParentItem(IconMain);
	IconDown->setParentItem(IconMain);

	IconMain->setPos(MainBlock->boundingRect().width()-30,10);
	IconMain->setZValue(8);
	IconMain->setData(1,QVariant("IconComponent"));

	//form text
	CompName = new QGraphicsTextItem();
	CompName->setPlainText(QString::fromLocal8Bit("Название"));
	CompName->setPos(10,25);
	CompName->setTextWidth(MainBlock->boundingRect().width()-20);
	CompName->setFont(QFont("Arial",-1,QFont::DemiBold));
	CompName->setData(1,QVariant("TextContent"));

	//form all object
	IconMain->setParentItem(MainBlock);
	CompName->setParentItem(MainBlock);

	//set properties for selcting
	SelectRect->setZValue(800);
	SelectRect->setBrush(Qt::NoBrush);
	SelectRect->setPen(Qt::NoPen);
	SelectRect->setData(1,QVariant("SelectingRect"));

	//set data
	MainBlock->setData(0,QVariant(ID));//id
	MainBlock->setData(1,QVariant("Component"));//type
	MainBlock->setData(2,QVariant("Main"));//is main part of block
	MainBlock->setData(3,QVariant("NotSelected"));//state
	MainBlock->setData(5,QVariant(0));//children
	MainBlock->setData(6,QVariant(0));//level of nesting
	MainBlock->setData(7,QVariant(0));//line, that connect's with comment
}

QGraphicsItem * ComponentEl::getThisElement()
{
	return MainBlock;
}

void ComponentEl::ColorSelecting(QBrush brush, QPen pen)
{
	SelectRect->setBrush(brush);
	SelectRect->setPen(pen);
}